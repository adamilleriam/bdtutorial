/**
 * Created by sha1 on 3/28/17.
 */
$(function(){
    $('#perPage').change(function () {
        var url=$(this).attr('url')+'/'+$(this).val();
        console.log(url);
        $.ajax({
            url:url,
            method: 'GET',
            success: function (data) {

                location.reload();
            },
            error:function (error) {
                alert('Sorry, Something went wrong ! Please try again after sometime.');
            }
        })
        console.log(url);
    });
});

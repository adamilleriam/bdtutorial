<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class SubDistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subdistricts')->insert([
            [ 'subdist_id'=>1, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Adabor-আদাবর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>2, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Badda-বাড্ডা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>3, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Bangsal-বংশাল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>4, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Bimanbandar-বিমানবন্দর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>5, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Cantonment-কেন্টনমেন্ট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>6, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Chak Bazar-চকবাজার ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>7, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Dakshinkhan-দক্ষিনখান ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>8, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Darus Salam-দারুস সালাম ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>9, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Demra-ডেমরা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>10, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Dhamrai-ধামরাই ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>11, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Dhanmondi-ধানমন্ডি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>12, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Dohar-দোহার ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>13, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Gendaria-গেন্ডারিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>14, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Gulshan-গুলশান ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>15, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Hazaribagh-হাজারীবাগ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>16, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Jatrabari-যাত্রাবাড়ী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>17, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Kadamtali-কদমতলী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>18, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Kafrul-কুরিল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>19, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Kalabagan-কলাবাগান ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>20, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Kamrangirchar-কামরাঙ্গীরচর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>21, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Keraniganj-কেরানীগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>22, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Khilgaon-খীলগাঁ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>23, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' khilkhet-খীলখেত ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>24, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Kotwali-কতোয়ালী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>25, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Lalbagh-লালবাগ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>26, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Mirpur-মিরপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>27, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Mohammadpur-মোহাম্মদপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>28, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Motijheel-মতিঝিল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>29, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Nawabganj-ন্ওয়াবগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>30, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Newmarket-নিউমারকেট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>31, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Pallabi-পল্লবী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>32, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Paltan-পাবনা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>33, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Ramna-রমনা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>34, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Rampura-রামপুরা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>35, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Rupnagar-রুপনগর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>36, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Sabujbagh-সবুজবাগ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>37, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Savar-সাভার ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>38, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Shah Ali-সাহআলী', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>39, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Shahbag-সাহবাগ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>40, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Sher-e-Bangla Nagar-শেরইবাংলা নগর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>41, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Shyampur-সেমপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>42, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Sutrapur-সুত্রাপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>43, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Tejgaon-তেজগাঁ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>44, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Mohakhali-মোহাখালী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>45, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Tejgaon Industrial Area-তেজগাঁ ইন্ডাষ্ট্রিয়াল এরিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>46, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Turag-তুরাগ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>47, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Uttara-উত্তরা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>48, 'division_id'=> '1', 'district_id'=> '1', 'subdistrictName'=> ' Uttar Khan-উত্তর খান ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>49, 'division_id'=> '1', 'district_id'=> '2', 'subdistrictName'=> ' Alfadanga-আফতাবনগর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>50, 'division_id'=> '1', 'district_id'=> '2', 'subdistrictName'=> ' Bhanga-বাঙ্গা', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>51, 'division_id'=> '1', 'district_id'=> '2', 'subdistrictName'=> ' Boalmari-বোয়ালমারী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>52, 'division_id'=> '1', 'district_id'=> '2', 'subdistrictName'=> ' Charbhadrasan-চরভদ্রাসন ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>53, 'division_id'=> '1', 'district_id'=> '2', 'subdistrictName'=> ' Faridpur Sadar-ফরিদপুর সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>54, 'division_id'=> '1', 'district_id'=> '2', 'subdistrictName'=> ' Madhukhali-মধুখালী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>55, 'division_id'=> '1', 'district_id'=> '2', 'subdistrictName'=> ' Nagarkanda-নগরকান্দা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>56, 'division_id'=> '1', 'district_id'=> '2', 'subdistrictName'=> ' Sadarpur-সদরপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>57, 'division_id'=> '1', 'district_id'=> '2', 'subdistrictName'=> ' Saltha-সালতা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>58, 'division_id'=> '1', 'district_id'=> '3', 'subdistrictName'=> ' Gazipur Sadar-গাজীপুর সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>59, 'division_id'=> '1', 'district_id'=> '3', 'subdistrictName'=> ' Kaliakair-কালীয়াকৈর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>60, 'division_id'=> '1', 'district_id'=> '3', 'subdistrictName'=> ' Kaliganj-কালীগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>61, 'division_id'=> '1', 'district_id'=> '3', 'subdistrictName'=> ' Kapasia-কাপাসিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>62, 'division_id'=> '1', 'district_id'=> '3', 'subdistrictName'=> ' Sreepur-শ্রীপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>63, 'division_id'=> '1', 'district_id'=> '4', 'subdistrictName'=> ' Gopalganj Sadar-গোপালগঞ্জ সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>64, 'division_id'=> '1', 'district_id'=> '4', 'subdistrictName'=> ' Kashiani-কাশিয়ানী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>65, 'division_id'=> '1', 'district_id'=> '4', 'subdistrictName'=> ' Kotalipara-কোসালীপাড়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>66, 'division_id'=> '1', 'district_id'=> '4', 'subdistrictName'=> ' Muksudpur-মকসুদপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>67, 'division_id'=> '1', 'district_id'=> '4', 'subdistrictName'=> ' Tungipara-টুঙ্গিপাড়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>68, 'division_id'=> '1', 'district_id'=> '5', 'subdistrictName'=> ' Baksiganj-বকসীগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>69, 'division_id'=> '1', 'district_id'=> '5', 'subdistrictName'=> ' Dewanganj-দেওয়ানগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>70, 'division_id'=> '1', 'district_id'=> '5', 'subdistrictName'=> ' Islampur-ইসলামপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>71, 'division_id'=> '1', 'district_id'=> '5', 'subdistrictName'=> ' Jamalpur Sadar-জামালপুর সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>72, 'division_id'=> '1', 'district_id'=> '5', 'subdistrictName'=> ' Madarganj-মাদারগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>73, 'division_id'=> '1', 'district_id'=> '5', 'subdistrictName'=> ' Melandaha-মেলান্দহ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>74, 'division_id'=> '1', 'district_id'=> '5', 'subdistrictName'=> ' Sarishabari-শরিষাবাড়ী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>75, 'division_id'=> '1', 'district_id'=> '6', 'subdistrictName'=> ' Astagram-আষ্টগ্রাম ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>76, 'division_id'=> '1', 'district_id'=> '6', 'subdistrictName'=> ' Bajitpur-বাজিতপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>77, 'division_id'=> '1', 'district_id'=> '6', 'subdistrictName'=> ' Bhairab-ভৈরব ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>78, 'division_id'=> '1', 'district_id'=> '6', 'subdistrictName'=> ' Hossainpur-হোসেনপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>79, 'division_id'=> '1', 'district_id'=> '6', 'subdistrictName'=> ' Itna-ইতনা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>80, 'division_id'=> '1', 'district_id'=> '6', 'subdistrictName'=> ' Karimganj-করিমগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>81, 'division_id'=> '1', 'district_id'=> '6', 'subdistrictName'=> ' Katiadi-কাতিয়াদি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>82, 'division_id'=> '1', 'district_id'=> '6', 'subdistrictName'=> ' Kishoreganj Sadar-কিশোরগঞ্জ সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>83, 'division_id'=> '1', 'district_id'=> '6', 'subdistrictName'=> ' Kuliarchar-কুলিয়াছারা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>84, 'division_id'=> '1', 'district_id'=> '6', 'subdistrictName'=> ' Mithamain-মিথামিইন ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>85, 'division_id'=> '1', 'district_id'=> '6', 'subdistrictName'=> ' Nikli-নিখিলদি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>86, 'division_id'=> '1', 'district_id'=> '6', 'subdistrictName'=> ' Pakundia-পাকুন্দিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>87, 'division_id'=> '1', 'district_id'=> '6', 'subdistrictName'=> ' Tarail-তারাইল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>88, 'division_id'=> '1', 'district_id'=> '7', 'subdistrictName'=> ' Rajoir-রাজইর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>89, 'division_id'=> '1', 'district_id'=> '7', 'subdistrictName'=> ' Madaripur Sadar-মাদারীপুর সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>90, 'division_id'=> '1', 'district_id'=> '7', 'subdistrictName'=> ' Kalkini-কালকিনি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>91, 'division_id'=> '1', 'district_id'=> '7', 'subdistrictName'=> ' Shibchar-শিবচর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>92, 'division_id'=> '1', 'district_id'=> '8', 'subdistrictName'=> ' Daulatpur-দৌলতপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>93, 'division_id'=> '1', 'district_id'=> '8', 'subdistrictName'=> ' Ghior-ঘিওর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>94, 'division_id'=> '1', 'district_id'=> '8', 'subdistrictName'=> ' Harirampur-হরিরামপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>95, 'division_id'=> '1', 'district_id'=> '8', 'subdistrictName'=> ' Manikgonj Sadar-মানিকগঞ্জ সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>96, 'division_id'=> '1', 'district_id'=> '8', 'subdistrictName'=> ' Saturia-সাতুরিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>97, 'division_id'=> '1', 'district_id'=> '8', 'subdistrictName'=> ' Shivalaya-শিবালয় ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>98, 'division_id'=> '1', 'district_id'=> '8', 'subdistrictName'=> ' Singair-সিংগাইর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>99, 'division_id'=> '1', 'district_id'=> '9', 'subdistrictName'=> ' Gazaria-গজারিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>100, 'division_id'=> '1', 'district_id'=> '9', 'subdistrictName'=> ' Lohajang-লৌহজং ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>101, 'division_id'=> '1', 'district_id'=> '9', 'subdistrictName'=> ' Munshiganj Sadar-মুন্সীগঞ্জ সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>102, 'division_id'=> '1', 'district_id'=> '9', 'subdistrictName'=> ' Sirajdikhan-সিরাজদিখান ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>103, 'division_id'=> '1', 'district_id'=> '9', 'subdistrictName'=> ' Sreenagar-শ্রীনগর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>104, 'division_id'=> '1', 'district_id'=> '9', 'subdistrictName'=> ' Tongibari-টঙ্গীবাড়ী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>105, 'division_id'=> '1', 'district_id'=> '10', 'subdistrictName'=> ' Bhaluka-ভালুকা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>106, 'division_id'=> '1', 'district_id'=> '10', 'subdistrictName'=> ' Dhobaura-ডুবুরিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>107, 'division_id'=> '1', 'district_id'=> '10', 'subdistrictName'=> ' Fulbaria-ফুলবাড়ীয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>108, 'division_id'=> '1', 'district_id'=> '10', 'subdistrictName'=> ' Gaffargaon-গফরগাঁ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>109, 'division_id'=> '1', 'district_id'=> '10', 'subdistrictName'=> ' Gauripur-গৌরীপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>110, 'division_id'=> '1', 'district_id'=> '10', 'subdistrictName'=> ' Haluaghat-হালুয়াঘাট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>111, 'division_id'=> '1', 'district_id'=> '10', 'subdistrictName'=> ' Ishwarganj-ঈশবরগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>112, 'division_id'=> '1', 'district_id'=> '10', 'subdistrictName'=> ' Mymensingh Sadar-ময়মনসিংহ সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>113, 'division_id'=> '1', 'district_id'=> '10', 'subdistrictName'=> ' Muktagachha-মুক্তাগাছা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>114, 'division_id'=> '1', 'district_id'=> '10', 'subdistrictName'=> ' Nandail-নন্দাইল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>115, 'division_id'=> '1', 'district_id'=> '10', 'subdistrictName'=> ' Phulpur-পুলপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>116, 'division_id'=> '1', 'district_id'=> '10', 'subdistrictName'=> ' Trishal-ত্রিশাইল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>117, 'division_id'=> '1', 'district_id'=> '10', 'subdistrictName'=> ' Tara Khanda-তারা কান্দা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>118, 'division_id'=> '1', 'district_id'=> '11', 'subdistrictName'=> ' Araihazar-আরাইহাজার ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>119, 'division_id'=> '1', 'district_id'=> '11', 'subdistrictName'=> ' Bandar-বন্দর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>120, 'division_id'=> '1', 'district_id'=> '11', 'subdistrictName'=> ' Narayanganj Sadar-নারায়নগঞ্জ সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>121, 'division_id'=> '1', 'district_id'=> '11', 'subdistrictName'=> ' Rupganj-রুপগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>122, 'division_id'=> '1', 'district_id'=> '11', 'subdistrictName'=> ' Sonargaon-সোনারগাঁ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>123, 'division_id'=> '1', 'district_id'=> '12', 'subdistrictName'=> ' Narsingdi Sadar-নরসিংদি সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>124, 'division_id'=> '1', 'district_id'=> '12', 'subdistrictName'=> ' Belabo-বেলাবো ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>125, 'division_id'=> '1', 'district_id'=> '12', 'subdistrictName'=> ' Monohardi-মনোহরদি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>126, 'division_id'=> '1', 'district_id'=> '12', 'subdistrictName'=> ' Palash-পলাশ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>127, 'division_id'=> '1', 'district_id'=> '12', 'subdistrictName'=> ' Raipura-রাইপুরা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>128, 'division_id'=> '1', 'district_id'=> '12', 'subdistrictName'=> ' Shibpur-শিবপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>129, 'division_id'=> '1', 'district_id'=> '13', 'subdistrictName'=> ' Atpara-আতপাড়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>130, 'division_id'=> '1', 'district_id'=> '13', 'subdistrictName'=> ' Barhatta-বারহাট্টা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>131, 'division_id'=> '1', 'district_id'=> '13', 'subdistrictName'=> ' Durgapur-দুর্গাপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>132, 'division_id'=> '1', 'district_id'=> '13', 'subdistrictName'=> ' Khaliajuri-কালিজুরি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>133, 'division_id'=> '1', 'district_id'=> '13', 'subdistrictName'=> ' Kalmakanda-কলমাকান্দা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>134, 'division_id'=> '1', 'district_id'=> '13', 'subdistrictName'=> ' Kendua-কেন্দুয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>135, 'division_id'=> '1', 'district_id'=> '13', 'subdistrictName'=> ' Madan-মদন ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>136, 'division_id'=> '1', 'district_id'=> '13', 'subdistrictName'=> ' Mohanganj-মোহনগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>137, 'division_id'=> '1', 'district_id'=> '13', 'subdistrictName'=> ' Netrokona Sadar-নেত্রোকোনা সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>138, 'division_id'=> '1', 'district_id'=> '13', 'subdistrictName'=> ' Purbadhala-পূর্বধলা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>139, 'division_id'=> '1', 'district_id'=> '14', 'subdistrictName'=> ' Baliakandi-কালিয়াকান্দি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>140, 'division_id'=> '1', 'district_id'=> '14', 'subdistrictName'=> ' Goalandaghat-গোয়ালন্দ ঘাট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>141, 'division_id'=> '1', 'district_id'=> '14', 'subdistrictName'=> ' Pangsha-পাংশা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>142, 'division_id'=> '1', 'district_id'=> '14', 'subdistrictName'=> ' Rajbari Sadar-রাজবাড়ী সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>143, 'division_id'=> '1', 'district_id'=> '14', 'subdistrictName'=> ' Kalukhali-কালুখালি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>144, 'division_id'=> '1', 'district_id'=> '15', 'subdistrictName'=> ' Bhedarganj-বদরগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>145, 'division_id'=> '1', 'district_id'=> '15', 'subdistrictName'=> ' Damudya-ডামুড্যা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>146, 'division_id'=> '1', 'district_id'=> '15', 'subdistrictName'=> ' Gosairhat-গোসাইরহাট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>147, 'division_id'=> '1', 'district_id'=> '15', 'subdistrictName'=> ' Naria-নাড়িয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>148, 'division_id'=> '1', 'district_id'=> '15', 'subdistrictName'=> ' Shariatpur Sadar-শরীয়তপুর সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>149, 'division_id'=> '1', 'district_id'=> '15', 'subdistrictName'=> ' Zanjira-জাজিরায় ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>150, 'division_id'=> '1', 'district_id'=> '15', 'subdistrictName'=> ' Shakhipur-সখীপুরে ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>151, 'division_id'=> '1', 'district_id'=> '16', 'subdistrictName'=> ' Jhenaigati-ঝিনাইগাতী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>152, 'division_id'=> '1', 'district_id'=> '16', 'subdistrictName'=> ' Nakla-নকলা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>153, 'division_id'=> '1', 'district_id'=> '16', 'subdistrictName'=> ' Nalitabari-নালিতাবাড়ী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>154, 'division_id'=> '1', 'district_id'=> '16', 'subdistrictName'=> ' Sherpur Sadar-শেরপুর সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>155, 'division_id'=> '1', 'district_id'=> '16', 'subdistrictName'=> ' Sreebardi-শ্রীবারদী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>156, 'division_id'=> '1', 'district_id'=> '17', 'subdistrictName'=> ' Gopalpur-গোপালপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>157, 'division_id'=> '1', 'district_id'=> '17', 'subdistrictName'=> ' Basail-বাসিল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>158, 'division_id'=> '1', 'district_id'=> '17', 'subdistrictName'=> ' Bhuapur-ভুয়াপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>159, 'division_id'=> '1', 'district_id'=> '17', 'subdistrictName'=> ' Delduar-দেলদুয়ার ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>160, 'division_id'=> '1', 'district_id'=> '17', 'subdistrictName'=> ' Ghatail-ঘাটাইল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>161, 'division_id'=> '1', 'district_id'=> '17', 'subdistrictName'=> ' Kalihati-কালিহাটি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>162, 'division_id'=> '1', 'district_id'=> '17', 'subdistrictName'=> ' Madhupur-মধুপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>163, 'division_id'=> '1', 'district_id'=> '17', 'subdistrictName'=> ' Mirzapur-মিরজাপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>164, 'division_id'=> '1', 'district_id'=> '17', 'subdistrictName'=> ' Nagarpur-নগরপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>165, 'division_id'=> '1', 'district_id'=> '17', 'subdistrictName'=> ' Sakhipur-সুখপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>166, 'division_id'=> '1', 'district_id'=> '17', 'subdistrictName'=> ' Dhanbari-ধানবাড়ি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>167, 'division_id'=> '1', 'district_id'=> '17', 'subdistrictName'=> ' Tangail Sadar-টাঙ্গাইল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>168, 'division_id'=> '1', 'district_id'=> '18', 'subdistrictName'=> ' Dhamrai-ধামরাই ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>169, 'division_id'=> '1', 'district_id'=> '19', 'subdistrictName'=> ' Dohar-দোহার ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>170, 'division_id'=> '1', 'district_id'=> '20', 'subdistrictName'=> ' Keraniganj-কেরানীগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>171, 'division_id'=> '1', 'district_id'=> '21', 'subdistrictName'=> ' Nawabganj-নওয়া্বগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>172, 'division_id'=> '1', 'district_id'=> '21', 'subdistrictName'=> ' Savar-সাভার ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>173, 'division_id'=> '2', 'district_id'=> '23', 'subdistrictName'=> ' Agailjhara-আগইলঝারা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>174, 'division_id'=> '2', 'district_id'=> '23', 'subdistrictName'=> ' Babuganj-বাবুগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>175, 'division_id'=> '2', 'district_id'=> '23', 'subdistrictName'=> ' Bakerganj-বাকেরগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>176, 'division_id'=> '2', 'district_id'=> '23', 'subdistrictName'=> ' Banaripara-বানরীপাড়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>177, 'division_id'=> '2', 'district_id'=> '23', 'subdistrictName'=> ' Gaurnadi-গৌরনদী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>178, 'division_id'=> '2', 'district_id'=> '23', 'subdistrictName'=> ' Hizla-হিজলা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>179, 'division_id'=> '2', 'district_id'=> '23', 'subdistrictName'=> ' Barisal Sadar-বরিশাল সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>180, 'division_id'=> '2', 'district_id'=> '23', 'subdistrictName'=> ' Mehendiganj-মেহেদিগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>181, 'division_id'=> '2', 'district_id'=> '23', 'subdistrictName'=> ' Muladi-মুলাদি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>182, 'division_id'=> '2', 'district_id'=> '23', 'subdistrictName'=> ' Wazirpur-ওয়াজিরপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>183, 'division_id'=> '2', 'district_id'=> '24', 'subdistrictName'=> ' Amtali-আমতলী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>184, 'division_id'=> '2', 'district_id'=> '24', 'subdistrictName'=> ' Bamna-বামনা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>185, 'division_id'=> '2', 'district_id'=> '24', 'subdistrictName'=> ' Barguna Sadar-বরগুনা সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>186, 'division_id'=> '2', 'district_id'=> '24', 'subdistrictName'=> ' Betagi-বেতাগী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>187, 'division_id'=> '2', 'district_id'=> '24', 'subdistrictName'=> ' Patharghata-পাথরঘাটা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>188, 'division_id'=> '2', 'district_id'=> '24', 'subdistrictName'=> ' Taltoli-তালতলী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>189, 'division_id'=> '2', 'district_id'=> '25', 'subdistrictName'=> ' Bhola Sadar-ভোলা সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>190, 'division_id'=> '2', 'district_id'=> '25', 'subdistrictName'=> ' Burhanuddin-বোরহানউদ্দিন ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>191, 'division_id'=> '2', 'district_id'=> '25', 'subdistrictName'=> ' Char Fasson-চরফেসন ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>192, 'division_id'=> '2', 'district_id'=> '25', 'subdistrictName'=> ' Daulatkhan-দৌলতখান ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>193, 'division_id'=> '2', 'district_id'=> '25', 'subdistrictName'=> ' Lalmohan-লালমোহন ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>194, 'division_id'=> '2', 'district_id'=> '25', 'subdistrictName'=> ' Manpura-মনপুরা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>195, 'division_id'=> '2', 'district_id'=> '25', 'subdistrictName'=> ' Tazumuddin-তাজুমুদ্দিন ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>196, 'division_id'=> '2', 'district_id'=> '26', 'subdistrictName'=> ' Jhalokati Sadar-ঝালকাঠি সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>197, 'division_id'=> '2', 'district_id'=> '26', 'subdistrictName'=> ' Kathalia-কাঠালিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>198, 'division_id'=> '2', 'district_id'=> '26', 'subdistrictName'=> ' Nalchity-নলসিটি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>199, 'division_id'=> '2', 'district_id'=> '26', 'subdistrictName'=> ' Rajapur-রাজাপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>200, 'division_id'=> '2', 'district_id'=> '27', 'subdistrictName'=> ' Bauphal-বাউফল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>201, 'division_id'=> '2', 'district_id'=> '27', 'subdistrictName'=> ' Dashmina-দাশমিনা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>202, 'division_id'=> '2', 'district_id'=> '27', 'subdistrictName'=> ' Galachipa-গলাচিপা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>203, 'division_id'=> '2', 'district_id'=> '27', 'subdistrictName'=> ' Kalapara-কলাপাড়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>204, 'division_id'=> '2', 'district_id'=> '27', 'subdistrictName'=> ' Mirzaganj-মিরজাগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>205, 'division_id'=> '2', 'district_id'=> '27', 'subdistrictName'=> ' Patuakhali Sadar-পটুয়াখালী সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>206, 'division_id'=> '2', 'district_id'=> '27', 'subdistrictName'=> ' Rangabali-রাঙ্গাবালী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>207, 'division_id'=> '2', 'district_id'=> '27', 'subdistrictName'=> ' Dumki-ধুমকি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>208, 'division_id'=> '2', 'district_id'=> '28', 'subdistrictName'=> ' Bhandaria-ভান্ডারিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>209, 'division_id'=> '2', 'district_id'=> '28', 'subdistrictName'=> ' Kawkhali-কাওখালি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>210, 'division_id'=> '2', 'district_id'=> '28', 'subdistrictName'=> ' Mathbaria-মটবাড়িয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>211, 'division_id'=> '2', 'district_id'=> '28', 'subdistrictName'=> ' Nazirpur-নাজিরপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>212, 'division_id'=> '2', 'district_id'=> '28', 'subdistrictName'=> ' Pirojpur Sadar-পিরোজপুর সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>213, 'division_id'=> '2', 'district_id'=> '28', 'subdistrictName'=> ' Nesarabad (Swarupkati)-নাছিরাবাদ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>214, 'division_id'=> '2', 'district_id'=> '28', 'subdistrictName'=> ' Zianagor-জিয়ানগর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>215, 'division_id'=> '3', 'district_id'=> '29', 'subdistrictName'=> ' Anwara-আনোয়ারা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>216, 'division_id'=> '3', 'district_id'=> '29', 'subdistrictName'=> ' Banshkhali-বাশখালী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>217, 'division_id'=> '3', 'district_id'=> '29', 'subdistrictName'=> ' Boalkhali-বোয়ালখালী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>218, 'division_id'=> '3', 'district_id'=> '29', 'subdistrictName'=> ' Chandanaish-চন্দনাইশ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>219, 'division_id'=> '3', 'district_id'=> '29', 'subdistrictName'=> ' Fatikchhari-ফটিকছরি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>220, 'division_id'=> '3', 'district_id'=> '29', 'subdistrictName'=> ' Hathazari-হাটহাজারী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>221, 'division_id'=> '3', 'district_id'=> '29', 'subdistrictName'=> ' Lohagara-লোহাগড়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>222, 'division_id'=> '3', 'district_id'=> '29', 'subdistrictName'=> ' Mirsharai-মিরশরাই ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>223, 'division_id'=> '3', 'district_id'=> '29', 'subdistrictName'=> ' Patiya-পটিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>224, 'division_id'=> '3', 'district_id'=> '29', 'subdistrictName'=> ' Rangunia-রাঙ্গুনিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>225, 'division_id'=> '3', 'district_id'=> '29', 'subdistrictName'=> ' Raozan-রাওজান ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>226, 'division_id'=> '3', 'district_id'=> '29', 'subdistrictName'=> ' Sandwip-সনদিপ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>227, 'division_id'=> '3', 'district_id'=> '29', 'subdistrictName'=> ' Satkania-সতকানিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>228, 'division_id'=> '3', 'district_id'=> '29', 'subdistrictName'=> ' Sitakunda-শিতাকুন্ডা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>229, 'division_id'=> '3', 'district_id'=> '29', 'subdistrictName'=> ' Bandor (Chittagong Port), নস্দর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>230, 'division_id'=> '3', 'district_id'=> '29', 'subdistrictName'=> ' Chandgaon-চাঁদগা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>231, 'division_id'=> '3', 'district_id'=> '29', 'subdistrictName'=> ' Double Mooring-ডবলমুরিং ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>232, 'division_id'=> '3', 'district_id'=> '29', 'subdistrictName'=> ' Kotwali-কতোয়ালী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>233, 'division_id'=> '3', 'district_id'=> '29', 'subdistrictName'=> ' Pahartali-পাহাড়তলী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>234, 'division_id'=> '3', 'district_id'=> '29', 'subdistrictName'=> ' Panchlaish-পাঁচলাইশ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>235, 'division_id'=> '3', 'district_id'=> '30', 'subdistrictName'=> ' Ali Kadam-আলিকদম ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>236, 'division_id'=> '3', 'district_id'=> '30', 'subdistrictName'=> ' Bandarban Sadar-বান্দরবন সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>237, 'division_id'=> '3', 'district_id'=> '30', 'subdistrictName'=> ' Lama-লামা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>238, 'division_id'=> '3', 'district_id'=> '30', 'subdistrictName'=> ' Naikhongchhari-নাক্ষ্যনছড়ি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>239, 'division_id'=> '3', 'district_id'=> '30', 'subdistrictName'=> ' Rowangchhari-রোয়াংছড়ি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>240, 'division_id'=> '3', 'district_id'=> '30', 'subdistrictName'=> ' Ruma-রুমা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>241, 'division_id'=> '3', 'district_id'=> '30', 'subdistrictName'=> ' Thanchi-থনচি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>242, 'division_id'=> '3', 'district_id'=> '31', 'subdistrictName'=> ' Akhaura-আখাওড়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>243, 'division_id'=> '3', 'district_id'=> '31', 'subdistrictName'=> ' Bancharampur-বানচারামপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>244, 'division_id'=> '3', 'district_id'=> '31', 'subdistrictName'=> ' Brahmanbaria Sadar-ব্রামনবড়িয়া সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>245, 'division_id'=> '3', 'district_id'=> '31', 'subdistrictName'=> ' Kasba-কাসবা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>246, 'division_id'=> '3', 'district_id'=> '31', 'subdistrictName'=> ' Nabinagar-নবীনগর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>247, 'division_id'=> '3', 'district_id'=> '31', 'subdistrictName'=> ' Nasirnagar-নাছিরনগর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>248, 'division_id'=> '3', 'district_id'=> '31', 'subdistrictName'=> ' Sarail-সরাইল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>249, 'division_id'=> '3', 'district_id'=> '31', 'subdistrictName'=> ' Ashuganj-আসুগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>250, 'division_id'=> '3', 'district_id'=> '31', 'subdistrictName'=> ' Bijoynagar-বিজয়নগর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>251, 'division_id'=> '3', 'district_id'=> '32', 'subdistrictName'=> ' Chandpur Sadar-চাদপুর সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>252, 'division_id'=> '3', 'district_id'=> '32', 'subdistrictName'=> ' Faridganj-ফরিদগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>253, 'division_id'=> '3', 'district_id'=> '32', 'subdistrictName'=> ' Haimchar-হিমছড়ি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>254, 'division_id'=> '3', 'district_id'=> '32', 'subdistrictName'=> ' Haziganj-হাজিগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>255, 'division_id'=> '3', 'district_id'=> '32', 'subdistrictName'=> ' Kachua-কচুয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>256, 'division_id'=> '3', 'district_id'=> '32', 'subdistrictName'=> ' Matlab Dakshin-মতলব দক্ষিন ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>257, 'division_id'=> '3', 'district_id'=> '32', 'subdistrictName'=> ' Matlab Uttar-মতলব উত্তর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>258, 'division_id'=> '3', 'district_id'=> '32', 'subdistrictName'=> ' Shahrasti-স্বরসতী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>259, 'division_id'=> '3', 'district_id'=> '33', 'subdistrictName'=> ' Barura-বরুরা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>260, 'division_id'=> '3', 'district_id'=> '33', 'subdistrictName'=> ' Brahmanpara-ব্রামনপাড়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>261, 'division_id'=> '3', 'district_id'=> '33', 'subdistrictName'=> ' Burichang-বুরিচং ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>262, 'division_id'=> '3', 'district_id'=> '33', 'subdistrictName'=> ' Chandina-চান্দিনা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>263, 'division_id'=> '3', 'district_id'=> '33', 'subdistrictName'=> ' Chauddagram-চুয়াডাঙ্গা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>264, 'division_id'=> '3', 'district_id'=> '33', 'subdistrictName'=> ' Daudkandi-দাওদকান্দি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>265, 'division_id'=> '3', 'district_id'=> '33', 'subdistrictName'=> ' Debidwar-দেবিদ্দার ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>266, 'division_id'=> '3', 'district_id'=> '33', 'subdistrictName'=> ' Homna-হোমনা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>267, 'division_id'=> '3', 'district_id'=> '33', 'subdistrictName'=> ' Laksam-লাকসাম ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>268, 'division_id'=> '3', 'district_id'=> '33', 'subdistrictName'=> ' Muradnagar-মুরাদনগর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>269, 'division_id'=> '3', 'district_id'=> '33', 'subdistrictName'=> ' Nangalkot-নাঙ্গলকোট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>270, 'division_id'=> '3', 'district_id'=> '33', 'subdistrictName'=> ' Comilla Adarsha Sadar-কুমিণ্ণা আদর্শ সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>271, 'division_id'=> '3', 'district_id'=> '33', 'subdistrictName'=> ' Meghna-মেঘনা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>272, 'division_id'=> '3', 'district_id'=> '33', 'subdistrictName'=> ' Titas-তিতাস ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>273, 'division_id'=> '3', 'district_id'=> '33', 'subdistrictName'=> ' Monohargonj-মনোহরগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>274, 'division_id'=> '3', 'district_id'=> '33', 'subdistrictName'=> ' Comilla Sadar Dakshin-কুমিল্লা সদর দক্ষিন ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>275, 'division_id'=> '3', 'district_id'=> '34', 'subdistrictName'=> ' Chakaria-চাখারিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>276, 'division_id'=> '3', 'district_id'=> '34', 'subdistrictName'=> ' Coxs Bazar Sadar-কক্সবাজার সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>277, 'division_id'=> '3', 'district_id'=> '34', 'subdistrictName'=> ' Kutubdia-কুতুবদিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>278, 'division_id'=> '3', 'district_id'=> '34', 'subdistrictName'=> ' Maheshkhali-মহেশখালী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>279, 'division_id'=> '3', 'district_id'=> '34', 'subdistrictName'=> ' Ramu-রামু ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>280, 'division_id'=> '3', 'district_id'=> '34', 'subdistrictName'=> ' Teknaf-টেকনাফ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>281, 'division_id'=> '3', 'district_id'=> '34', 'subdistrictName'=> ' Ukhia-উখিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>282, 'division_id'=> '3', 'district_id'=> '34', 'subdistrictName'=> ' Pekua-পেকুয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>283, 'division_id'=> '3', 'district_id'=> '35', 'subdistrictName'=> ' Chhagalnaiya-ছাগলনাইয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>284, 'division_id'=> '3', 'district_id'=> '35', 'subdistrictName'=> ' Daganbhuiyan-দাগনভুইয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>285, 'division_id'=> '3', 'district_id'=> '35', 'subdistrictName'=> ' Feni Sadar-ফেনি সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>286, 'division_id'=> '3', 'district_id'=> '35', 'subdistrictName'=> ' Parshuram-পরসুরাম ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>287, 'division_id'=> '3', 'district_id'=> '35', 'subdistrictName'=> ' Sonagazi-সোনাগাজী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>288, 'division_id'=> '3', 'district_id'=> '35', 'subdistrictName'=> ' Fulgazi-ফুলগাজী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>289, 'division_id'=> '3', 'district_id'=> '36', 'subdistrictName'=> ' Dighinala-দিঘিনালা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>290, 'division_id'=> '3', 'district_id'=> '36', 'subdistrictName'=> ' Khagrachhari-খাগরাছড়ি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>291, 'division_id'=> '3', 'district_id'=> '36', 'subdistrictName'=> ' Lakshmichhari-লক্ষিছারি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>292, 'division_id'=> '3', 'district_id'=> '36', 'subdistrictName'=> ' Mahalchhari-মাহালছড়ি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>293, 'division_id'=> '3', 'district_id'=> '36', 'subdistrictName'=> ' Manikchhari-মানিকছড়ি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>294, 'division_id'=> '3', 'district_id'=> '36', 'subdistrictName'=> ' Matiranga-মাটিরাঙ্গা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>295, 'division_id'=> '3', 'district_id'=> '36', 'subdistrictName'=> ' Panchhari-পানছরি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>296, 'division_id'=> '3', 'district_id'=> '36', 'subdistrictName'=> ' Ramgarh-রামগড় ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>297, 'division_id'=> '3', 'district_id'=> '37', 'subdistrictName'=> ' Lakshmipur Sadar-লক্ষিপুর সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>298, 'division_id'=> '3', 'district_id'=> '37', 'subdistrictName'=> ' Raipur-রাইপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>299, 'division_id'=> '3', 'district_id'=> '37', 'subdistrictName'=> ' Ramganj-রামগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>300, 'division_id'=> '3', 'district_id'=> '37', 'subdistrictName'=> ' Ramgati-রামঘাটি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>301, 'division_id'=> '3', 'district_id'=> '37', 'subdistrictName'=> ' Kamalnagar-কামালনগর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>302, 'division_id'=> '3', 'district_id'=> '38', 'subdistrictName'=> ' Begumganj-বেগমগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>303, 'division_id'=> '3', 'district_id'=> '38', 'subdistrictName'=> ' Noakhali Sadar-নোয়াখালী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>304, 'division_id'=> '3', 'district_id'=> '38', 'subdistrictName'=> ' Chatkhil-চাটখিল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>305, 'division_id'=> '3', 'district_id'=> '38', 'subdistrictName'=> ' Companiganj-কোম্পানীগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>306, 'division_id'=> '3', 'district_id'=> '38', 'subdistrictName'=> ' Hatiya-হাতিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>307, 'division_id'=> '3', 'district_id'=> '38', 'subdistrictName'=> ' Senbagh-সেনবাগ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>308, 'division_id'=> '3', 'district_id'=> '38', 'subdistrictName'=> ' Sonaimuri-সোনাইমুরি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>309, 'division_id'=> '3', 'district_id'=> '38', 'subdistrictName'=> ' Subarnachar-সুবর্নচর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>310, 'division_id'=> '3', 'district_id'=> '38', 'subdistrictName'=> ' Kabirhat-কবিরহাট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>311, 'division_id'=> '3', 'district_id'=> '39', 'subdistrictName'=> ' Bagaichhari-বাঘাইছরি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>312, 'division_id'=> '3', 'district_id'=> '39', 'subdistrictName'=> ' Barkal-বরকল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>313, 'division_id'=> '3', 'district_id'=> '39', 'subdistrictName'=> ' Kawkhali-কাওখালি (Betbunia) ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>314, 'division_id'=> '3', 'district_id'=> '39', 'subdistrictName'=> ' Belaichhari-বিলাইছরি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>315, 'division_id'=> '3', 'district_id'=> '39', 'subdistrictName'=> ' Kaptai-কাপ্তাই ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>316, 'division_id'=> '3', 'district_id'=> '39', 'subdistrictName'=> ' Juraichhari-কুরিছরি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>317, 'division_id'=> '3', 'district_id'=> '39', 'subdistrictName'=> ' Langadu-লাঙ্গাদু ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>318, 'division_id'=> '3', 'district_id'=> '39', 'subdistrictName'=> ' Naniyachar-নানীয়াচর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>319, 'division_id'=> '3', 'district_id'=> '39', 'subdistrictName'=> ' Rajasthali-রাজস্থালী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>320, 'division_id'=> '3', 'district_id'=> '39', 'subdistrictName'=> ' Rangamati Sadar-রাঙ্গামটি সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>321, 'division_id'=> '4', 'district_id'=> '40', 'subdistrictName'=> ' Bagha-বাঘা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

  
            [ 'subdist_id'=>323, 'division_id'=> '4', 'district_id'=> '40', 'subdistrictName'=> ' Charghat-চরঘাট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>324, 'division_id'=> '4', 'district_id'=> '40', 'subdistrictName'=> ' Durgapur-দুর্ঘাপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>325, 'division_id'=> '4', 'district_id'=> '40', 'subdistrictName'=> ' Godagari-গোদাগারী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>326, 'division_id'=> '4', 'district_id'=> '40', 'subdistrictName'=> ' Mohanpur-মোহনপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>327, 'division_id'=> '4', 'district_id'=> '40', 'subdistrictName'=> ' Paba-পাবা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>328, 'division_id'=> '4', 'district_id'=> '40', 'subdistrictName'=> ' Puthia-পুঠিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>329, 'division_id'=> '4', 'district_id'=> '40', 'subdistrictName'=> ' Tanore-তানরি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>330, 'division_id'=> '4', 'district_id'=> '40', 'subdistrictName'=> ' Boalia-বোয়ালিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>331, 'division_id'=> '4', 'district_id'=> '40', 'subdistrictName'=> ' Matihar-মতিহর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>332, 'division_id'=> '4', 'district_id'=> '40', 'subdistrictName'=> ' Rajpara-রাজপাড়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>333, 'division_id'=> '4', 'district_id'=> '40', 'subdistrictName'=> ' Shah Mokdum-শাহ মকদুম ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'subdist_id'=>335, 'division_id'=> '4', 'district_id'=> '40', 'subdistrictName'=> ' Bagmara-বাঘমারা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>336, 'division_id'=> '4', 'district_id'=> '40', 'subdistrictName'=> ' Charghat-চরঘাট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>337, 'division_id'=> '4', 'district_id'=> '40', 'subdistrictName'=> ' Durgapur-দর্গাপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>338, 'division_id'=> '4', 'district_id'=> '40', 'subdistrictName'=> ' Godagari-গোদগাড়ী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>339, 'division_id'=> '4', 'district_id'=> '40', 'subdistrictName'=> ' Mohanpur-মোহনপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>360, 'division_id'=> '4', 'district_id'=> '41', 'subdistrictName'=> ' Adamdighi-আদমদিঘি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>361, 'division_id'=> '4', 'district_id'=> '41', 'subdistrictName'=> ' Bogra Sadar-বগুড়া সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>362, 'division_id'=> '4', 'district_id'=> '41', 'subdistrictName'=> ' Dhunat-ধনুট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>363, 'division_id'=> '4', 'district_id'=> '41', 'subdistrictName'=> ' Dhupchanchia-দুপচাচিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>364, 'division_id'=> '4', 'district_id'=> '41', 'subdistrictName'=> ' Gabtali-গাবতলী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>365, 'division_id'=> '4', 'district_id'=> '41', 'subdistrictName'=> ' Kahaloo-কাহালু ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>366, 'division_id'=> '4', 'district_id'=> '41', 'subdistrictName'=> ' Nandigram-নন্দীগ্রাম ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>367, 'division_id'=> '4', 'district_id'=> '41', 'subdistrictName'=> ' Sariakandi-সারিয়াকান্দী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>368, 'division_id'=> '4', 'district_id'=> '41', 'subdistrictName'=> ' Shajahanpur-শাজাহানপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>369, 'division_id'=> '4', 'district_id'=> '41', 'subdistrictName'=> ' Sherpur-শেরপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>370, 'division_id'=> '4', 'district_id'=> '41', 'subdistrictName'=> ' Shibganj-শিবগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>371, 'division_id'=> '4', 'district_id'=> '41', 'subdistrictName'=> ' Sonatola-সোনাতলা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>372, 'division_id'=> '4', 'district_id'=> '42', 'subdistrictName'=> ' Akkelpur-আক্কেলপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>373, 'division_id'=> '4', 'district_id'=> '42', 'subdistrictName'=> ' Joypurhat Sadar-জয়পুরহাট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>374, 'division_id'=> '4', 'district_id'=> '42', 'subdistrictName'=> ' Kalai-কালাই ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>375, 'division_id'=> '4', 'district_id'=> '42', 'subdistrictName'=> ' Khetlal-খেতলাল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>376, 'division_id'=> '4', 'district_id'=> '42', 'subdistrictName'=> ' Panchbibi-পাঁচবিবি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>377, 'division_id'=> '4', 'district_id'=> '43', 'subdistrictName'=> ' Atrai-আত্রাই ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>378, 'division_id'=> '4', 'district_id'=> '43', 'subdistrictName'=> ' Badalgachhi-বাদলগাছি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>379, 'division_id'=> '4', 'district_id'=> '43', 'subdistrictName'=> ' Manda-মান্দা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>380, 'division_id'=> '4', 'district_id'=> '43', 'subdistrictName'=> ' Dhamoirhat-ধামুরহাট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>381, 'division_id'=> '4', 'district_id'=> '43', 'subdistrictName'=> ' Mohadevpur-মহাদেবপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>382, 'division_id'=> '4', 'district_id'=> '43', 'subdistrictName'=> ' Naogaon Sadar-নওগা সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>383, 'division_id'=> '4', 'district_id'=> '43', 'subdistrictName'=> ' Niamatpur-নিয়ামতপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>384, 'division_id'=> '4', 'district_id'=> '43', 'subdistrictName'=> ' Patnitala-পত্নীতলা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>385, 'division_id'=> '4', 'district_id'=> '43', 'subdistrictName'=> ' Porsha-পরসা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>386, 'division_id'=> '4', 'district_id'=> '43', 'subdistrictName'=> ' Raninagar-রানিনগর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>387, 'division_id'=> '4', 'district_id'=> '43', 'subdistrictName'=> ' Sapahar-শাপাহার ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>388, 'division_id'=> '4', 'district_id'=> '44', 'subdistrictName'=> ' Bagatipara-বাগাতিপাড়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>389, 'division_id'=> '4', 'district_id'=> '44', 'subdistrictName'=> ' Baraigram-বড়াইগ্রামে- ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>390, 'division_id'=> '4', 'district_id'=> '44', 'subdistrictName'=> ' Gurudaspur-গুরুদাসপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>391, 'division_id'=> '4', 'district_id'=> '44', 'subdistrictName'=> ' Lalpur-লালপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>392, 'division_id'=> '4', 'district_id'=> '44', 'subdistrictName'=> ' Natore Sadar-নাটোর সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>393, 'division_id'=> '4', 'district_id'=> '44', 'subdistrictName'=> ' Singra-সিংরা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>394, 'division_id'=> '4', 'district_id'=> '44', 'subdistrictName'=> ' Naldanga-নলডাঙ্গা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>395, 'division_id'=> '4', 'district_id'=> '45', 'subdistrictName'=> ' Bholahat-ভোলাহাট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>396, 'division_id'=> '4', 'district_id'=> '45', 'subdistrictName'=> ' Gomastapur-গোমস্তাপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>397, 'division_id'=> '4', 'district_id'=> '45', 'subdistrictName'=> ' Nachole-নাচোল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>398, 'division_id'=> '4', 'district_id'=> '45', 'subdistrictName'=> ' Nawabganj Sadar-নবাবগঞ্জ সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>399, 'division_id'=> '4', 'district_id'=> '45', 'subdistrictName'=> ' Shibganj-শিবগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>400, 'division_id'=> '4', 'district_id'=> '46', 'subdistrictName'=> ' Ataikula-আতাইকুলা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>401, 'division_id'=> '4', 'district_id'=> '46', 'subdistrictName'=> ' Atgharia-আতঘারিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>402, 'division_id'=> '4', 'district_id'=> '46', 'subdistrictName'=> ' Bera-বেড়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>403, 'division_id'=> '4', 'district_id'=> '46', 'subdistrictName'=> ' Bhangura-ভাঙ্গুরা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>404, 'division_id'=> '4', 'district_id'=> '46', 'subdistrictName'=> ' Chatmohar-চাদমোহর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>405, 'division_id'=> '4', 'district_id'=> '46', 'subdistrictName'=> ' Faridpur-ফরিদপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>406, 'division_id'=> '4', 'district_id'=> '46', 'subdistrictName'=> ' Ishwardi-ইশ্বরদি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>407, 'division_id'=> '4', 'district_id'=> '46', 'subdistrictName'=> ' Pabna Sadar-পাবনা সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>408, 'division_id'=> '4', 'district_id'=> '46', 'subdistrictName'=> ' Santhia-সান্তিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>409, 'division_id'=> '4', 'district_id'=> '46', 'subdistrictName'=> ' Sujanagar-সুজানগর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>410, 'division_id'=> '4', 'district_id'=> '47', 'subdistrictName'=> ' Belkuchi-বেলকুচি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>411, 'division_id'=> '4', 'district_id'=> '47', 'subdistrictName'=> ' Chauhali-চৌহালি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>412, 'division_id'=> '4', 'district_id'=> '47', 'subdistrictName'=> ' Kamarkhanda-কামারখন্দ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>413, 'division_id'=> '4', 'district_id'=> '47', 'subdistrictName'=> ' Kazipur-কাজিপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>414, 'division_id'=> '4', 'district_id'=> '47', 'subdistrictName'=> ' Raiganj-রাইগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>415, 'division_id'=> '4', 'district_id'=> '47', 'subdistrictName'=> ' Shahjadpur-শাহজাদপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>416, 'division_id'=> '4', 'district_id'=> '47', 'subdistrictName'=> ' Sirajganj Sadar-সিরাজগঞ্জ সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>417, 'division_id'=> '4', 'district_id'=> '47', 'subdistrictName'=> ' Tarash-তাড়াশ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>418, 'division_id'=> '4', 'district_id'=> '47', 'subdistrictName'=> ' Ullahpara-উল্লাপাড়া', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>419, 'division_id'=> '5', 'district_id'=> '48', 'subdistrictName'=> ' Balaganj-বালাগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>420, 'division_id'=> '5', 'district_id'=> '48', 'subdistrictName'=> ' Beanibazar-বিয়ানিবাজার ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>421, 'division_id'=> '5', 'district_id'=> '48', 'subdistrictName'=> ' Bishwanath-বিশ্বনাথ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>422, 'division_id'=> '5', 'district_id'=> '48', 'subdistrictName'=> ' Companigonj-কোম্পানিগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>423, 'division_id'=> '5', 'district_id'=> '48', 'subdistrictName'=> ' Fenchuganj-ফেঞ্চুগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>424, 'division_id'=> '5', 'district_id'=> '48', 'subdistrictName'=> ' Golapganj-গোলাবগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>425, 'division_id'=> '5', 'district_id'=> '48', 'subdistrictName'=> ' Gowainghat-গোয়াইনঘাট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>426, 'division_id'=> '5', 'district_id'=> '48', 'subdistrictName'=> ' Jaintiapur-জৈন্তাপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>427, 'division_id'=> '5', 'district_id'=> '48', 'subdistrictName'=> ' Kanaighat-কানাইঘাট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>428, 'division_id'=> '5', 'district_id'=> '48', 'subdistrictName'=> ' Sylhet Sadar-সিলেট সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>429, 'division_id'=> '5', 'district_id'=> '48', 'subdistrictName'=> ' Zakiganj-জকিগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>430, 'division_id'=> '5', 'district_id'=> '48', 'subdistrictName'=> ' South Shurma-দক্ষিণ সুরমা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>431, 'division_id'=> '5', 'district_id'=> '49', 'subdistrictName'=> ' Ajmiriganj-আজমিরিগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>432, 'division_id'=> '5', 'district_id'=> '49', 'subdistrictName'=> ' Bahubal-বহুবল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>433, 'division_id'=> '5', 'district_id'=> '49', 'subdistrictName'=> ' Baniyachong-বানিয়াচং ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>434, 'division_id'=> '5', 'district_id'=> '49', 'subdistrictName'=> ' Chunarughat-চুনারুঘাট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>435, 'division_id'=> '5', 'district_id'=> '49', 'subdistrictName'=> ' Habiganj Sadar-হবিগঞ্জ সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>436, 'division_id'=> '5', 'district_id'=> '49', 'subdistrictName'=> ' Lakhai-লাখাই ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>437, 'division_id'=> '5', 'district_id'=> '49', 'subdistrictName'=> ' Madhabpur-মাধবপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>438, 'division_id'=> '5', 'district_id'=> '49', 'subdistrictName'=> ' Nabiganj-নবীগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>439, 'division_id'=> '5', 'district_id'=> '50', 'subdistrictName'=> ' Barlekha-বড়লেখা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>440, 'division_id'=> '5', 'district_id'=> '50', 'subdistrictName'=> ' Kamalganj-কামালগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>441, 'division_id'=> '5', 'district_id'=> '50', 'subdistrictName'=> ' Kulaura-কুলাউড়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>442, 'division_id'=> '5', 'district_id'=> '50', 'subdistrictName'=> ' Moulvibazar Sadar-মউলবিবাজার সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>443, 'division_id'=> '5', 'district_id'=> '50', 'subdistrictName'=> ' Rajnagar-রাজনগর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>444, 'division_id'=> '5', 'district_id'=> '50', 'subdistrictName'=> ' Sreemangal-শ্রীমঙ্গল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>445, 'division_id'=> '5', 'district_id'=> '50', 'subdistrictName'=> ' Juri-জুরি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>446, 'division_id'=> '5', 'district_id'=> '51', 'subdistrictName'=> ' Bishwamvarpur-বিশ্বম্ভরপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>447, 'division_id'=> '5', 'district_id'=> '51', 'subdistrictName'=> ' Chhatak-ছাতক ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>448, 'division_id'=> '5', 'district_id'=> '51', 'subdistrictName'=> ' Derai-দিরাই ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>449, 'division_id'=> '5', 'district_id'=> '51', 'subdistrictName'=> ' Dharampasha-ধর্র্মপাশা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>450, 'division_id'=> '5', 'district_id'=> '51', 'subdistrictName'=> ' Dowarabazar-দোয়ারাবাজার ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>451, 'division_id'=> '5', 'district_id'=> '51', 'subdistrictName'=> ' Jagannathpur-জগন্নাথপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>452, 'division_id'=> '5', 'district_id'=> '51', 'subdistrictName'=> ' Jamalganj-জামালগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>453, 'division_id'=> '5', 'district_id'=> '51', 'subdistrictName'=> ' Sullah-সুল্লাহ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>454, 'division_id'=> '5', 'district_id'=> '51', 'subdistrictName'=> ' Sunamganj Sadar-সুনামগঞ্জ সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>455, 'division_id'=> '5', 'district_id'=> '51', 'subdistrictName'=> ' Tahirpur-তাহিরপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>456, 'division_id'=> '5', 'district_id'=> '51', 'subdistrictName'=> ' South Sunamganj-দক্ষিন সুনামগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>457, 'division_id'=> '6', 'district_id'=> '52', 'subdistrictName'=> ' Badarganj-বদরগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>458, 'division_id'=> '6', 'district_id'=> '52', 'subdistrictName'=> ' Gangachhara-গংগাচড়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>459, 'division_id'=> '6', 'district_id'=> '52', 'subdistrictName'=> ' Kaunia-কাউনিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>460, 'division_id'=> '6', 'district_id'=> '52', 'subdistrictName'=> ' Rangpur Sadar-রংপুর সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>461, 'division_id'=> '6', 'district_id'=> '52', 'subdistrictName'=> ' Mithapukur-মিঠাপুকুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>462, 'division_id'=> '6', 'district_id'=> '52', 'subdistrictName'=> ' Pirgachha-পীরগাছা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>463, 'division_id'=> '6', 'district_id'=> '52', 'subdistrictName'=> ' Pirganj-পীরগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>464, 'division_id'=> '6', 'district_id'=> '52', 'subdistrictName'=> ' Taraganj-তারাগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>465, 'division_id'=> '6', 'district_id'=> '53', 'subdistrictName'=> ' Birampur-বিরামপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>466, 'division_id'=> '6', 'district_id'=> '53', 'subdistrictName'=> ' Birganj-বিরগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>467, 'division_id'=> '6', 'district_id'=> '53', 'subdistrictName'=> ' Biral-বিরল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>468, 'division_id'=> '6', 'district_id'=> '53', 'subdistrictName'=> ' Bochaganj-বোচাগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>469, 'division_id'=> '6', 'district_id'=> '53', 'subdistrictName'=> ' Chirirbandar-চিরিরবন্দর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>470, 'division_id'=> '6', 'district_id'=> '53', 'subdistrictName'=> ' Phulbari-ফুলবাড়ী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>471, 'division_id'=> '6', 'district_id'=> '53', 'subdistrictName'=> ' Ghoraghat-ঘোড়াঘাট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>472, 'division_id'=> '6', 'district_id'=> '53', 'subdistrictName'=> ' Hakimpur-হাকিমপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>473, 'division_id'=> '6', 'district_id'=> '53', 'subdistrictName'=> ' Kaharole-কাহারোল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>474, 'division_id'=> '6', 'district_id'=> '53', 'subdistrictName'=> ' Khansama-খানসামা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>475, 'division_id'=> '6', 'district_id'=> '53', 'subdistrictName'=> ' Dinajpur Sadar-দিনাজপুর সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>476, 'division_id'=> '6', 'district_id'=> '53', 'subdistrictName'=> ' Nawabganj-নওয়া্বগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>477, 'division_id'=> '6', 'district_id'=> '53', 'subdistrictName'=> ' Parbatipur-পার্বতীপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>478, 'division_id'=> '6', 'district_id'=> '54', 'subdistrictName'=> ' Phulchhari-ফুলছড়ি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>479, 'division_id'=> '6', 'district_id'=> '54', 'subdistrictName'=> ' Gaibandha Sadar-গাইবান্ধা সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>480, 'division_id'=> '6', 'district_id'=> '54', 'subdistrictName'=> ' Gobindaganj-গোবিন্দগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>481, 'division_id'=> '6', 'district_id'=> '54', 'subdistrictName'=> ' Palashbari-পলাশবাড়ী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>482, 'division_id'=> '6', 'district_id'=> '54', 'subdistrictName'=> ' Sadullapur-সাদুল্লাপুর', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>483, 'division_id'=> '6', 'district_id'=> '54', 'subdistrictName'=> ' Sughatta-সাঘাটা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>484, 'division_id'=> '6', 'district_id'=> '54', 'subdistrictName'=> ' Sundarganj-সুন্দরগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>485, 'division_id'=> '6', 'district_id'=> '55', 'subdistrictName'=> ' Bhurungamari-বরুঙ্গামারি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>486, 'division_id'=> '6', 'district_id'=> '55', 'subdistrictName'=> ' Char Rajibpur-চর রাজাপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>487, 'division_id'=> '6', 'district_id'=> '55', 'subdistrictName'=> ' Chilmari-চিলমারি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>488, 'division_id'=> '6', 'district_id'=> '55', 'subdistrictName'=> ' Phulbari-ফুলবাড়ী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>489, 'division_id'=> '6', 'district_id'=> '55', 'subdistrictName'=> ' Kurigram Sadar-কুরিগ্রমি সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>490, 'division_id'=> '6', 'district_id'=> '55', 'subdistrictName'=> ' Nageshwari-নাগেশ্বরী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>491, 'division_id'=> '6', 'district_id'=> '55', 'subdistrictName'=> ' Rajarhat-রাজারহাট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>492, 'division_id'=> '6', 'district_id'=> '55', 'subdistrictName'=> ' Raomari-রৗমারী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>493, 'division_id'=> '6', 'district_id'=> '55', 'subdistrictName'=> ' Ulipur-উলিপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>494, 'division_id'=> '6', 'district_id'=> '56', 'subdistrictName'=> ' Aditmari-অদিতমারি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>495, 'division_id'=> '6', 'district_id'=> '56', 'subdistrictName'=> ' Hatibandha-হতিবান্ধা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>496, 'division_id'=> '6', 'district_id'=> '56', 'subdistrictName'=> ' Kaliganj-কালিগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>497, 'division_id'=> '6', 'district_id'=> '56', 'subdistrictName'=> ' Lalmonirhat Sadar-লালমনিরহাট সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>498, 'division_id'=> '6', 'district_id'=> '56', 'subdistrictName'=> ' Patgram-পাটগ্রাম ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>499, 'division_id'=> '6', 'district_id'=> '57', 'subdistrictName'=> ' Dimla-ডিমলা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>500, 'division_id'=> '6', 'district_id'=> '57', 'subdistrictName'=> ' Domar-ডোমার  ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>501, 'division_id'=> '6', 'district_id'=> '57', 'subdistrictName'=> ' Jaldhaka-জলঢাকা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>502, 'division_id'=> '6', 'district_id'=> '57', 'subdistrictName'=> ' Kishoreganj-কিশোরগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>503, 'division_id'=> '6', 'district_id'=> '57', 'subdistrictName'=> ' Nilphamari Sadar-নীলফামারি সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>504, 'division_id'=> '6', 'district_id'=> '57', 'subdistrictName'=> ' Saidpur-সৈয়দপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>505, 'division_id'=> '6', 'district_id'=> '58', 'subdistrictName'=> ' Atwari-আতওয়ারি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>506, 'division_id'=> '6', 'district_id'=> '58', 'subdistrictName'=> ' Boda-বোদা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>507, 'division_id'=> '6', 'district_id'=> '58', 'subdistrictName'=> ' Debiganj-দেবিগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>508, 'division_id'=> '6', 'district_id'=> '58', 'subdistrictName'=> ' Panchagarh Sadar-পঞ্চগড় সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>509, 'division_id'=> '6', 'district_id'=> '58', 'subdistrictName'=> ' Tetulia-তেতুলিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>510, 'division_id'=> '6', 'district_id'=> '59', 'subdistrictName'=> ' Baliadangi-বালিয়াডাঙ্গি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>511, 'division_id'=> '6', 'district_id'=> '59', 'subdistrictName'=> ' Haripur-হরিপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>512, 'division_id'=> '6', 'district_id'=> '59', 'subdistrictName'=> ' Pirganj-পীরগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>513, 'division_id'=> '6', 'district_id'=> '59', 'subdistrictName'=> ' Ranisankail-রানিসংকাইল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>514, 'division_id'=> '6', 'district_id'=> '59', 'subdistrictName'=> ' Thakurgaon Sadar-ঠাকুরগাঁ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>515, 'division_id'=> '7', 'district_id'=> '60', 'subdistrictName'=> ' Batiaghata-বাতিয়ঘাটা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>516, 'division_id'=> '7', 'district_id'=> '60', 'subdistrictName'=> ' Dacope-দাকোপ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>517, 'division_id'=> '7', 'district_id'=> '60', 'subdistrictName'=> ' Dumuria-ডুমুরিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>518, 'division_id'=> '7', 'district_id'=> '60', 'subdistrictName'=> ' Dighalia-দিঘালিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>519, 'division_id'=> '7', 'district_id'=> '60', 'subdistrictName'=> ' Koyra-কয়রা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>520, 'division_id'=> '7', 'district_id'=> '60', 'subdistrictName'=> ' Paikgachha-পাইকগাছা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>521, 'division_id'=> '7', 'district_id'=> '60', 'subdistrictName'=> ' Phultala-ফুলতলা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>522, 'division_id'=> '7', 'district_id'=> '60', 'subdistrictName'=> ' Rupsha-রুপসা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>523, 'division_id'=> '7', 'district_id'=> '60', 'subdistrictName'=> ' Terokhada-তেরখাদা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>524, 'division_id'=> '7', 'district_id'=> '60', 'subdistrictName'=> ' Daulatpur-দৌলতপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>525, 'division_id'=> '7', 'district_id'=> '60', 'subdistrictName'=> ' Khalishpur-খালিশপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>526, 'division_id'=> '7', 'district_id'=> '60', 'subdistrictName'=> ' Khan Jahan Ali-খান যাহান আলি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>527, 'division_id'=> '7', 'district_id'=> '60', 'subdistrictName'=> ' Kotwali-কতোয়ালী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>528, 'division_id'=> '7', 'district_id'=> '60', 'subdistrictName'=> ' Sonadanga-সোনাডাঙ্গা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>529, 'division_id'=> '7', 'district_id'=> '60', 'subdistrictName'=> ' Harintana-হরিনটানা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>530, 'division_id'=> '7', 'district_id'=> '61', 'subdistrictName'=> ' Bagerhat Sadar-বাঘেরহাট সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>531, 'division_id'=> '7', 'district_id'=> '61', 'subdistrictName'=> ' Chitalmari-চিতলমারি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>532, 'division_id'=> '7', 'district_id'=> '61', 'subdistrictName'=> ' Fakirhat-ফকিরহাট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>533, 'division_id'=> '7', 'district_id'=> '61', 'subdistrictName'=> ' Kachua-কচুয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>534, 'division_id'=> '7', 'district_id'=> '61', 'subdistrictName'=> ' Mollahat-মোল্লাহাট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>535, 'division_id'=> '7', 'district_id'=> '61', 'subdistrictName'=> ' Mongla-মংলা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>536, 'division_id'=> '7', 'district_id'=> '61', 'subdistrictName'=> ' Morrelganj-মোরেলগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>537, 'division_id'=> '7', 'district_id'=> '61', 'subdistrictName'=> ' Rampal-রামপাল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>538, 'division_id'=> '7', 'district_id'=> '61', 'subdistrictName'=> ' Sarankhola-সরনখোলা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>539, 'division_id'=> '7', 'district_id'=> '62', 'subdistrictName'=> ' Alamdanga-আলমডাঙ্গা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>540, 'division_id'=> '7', 'district_id'=> '62', 'subdistrictName'=> ' Chuadanga Sadar-চুয়াডাঙ্গা সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>541, 'division_id'=> '7', 'district_id'=> '62', 'subdistrictName'=> ' Damurhuda-দামুরহুদা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>542, 'division_id'=> '7', 'district_id'=> '62', 'subdistrictName'=> ' Jibannagar-জীবননগর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>543, 'division_id'=> '7', 'district_id'=> '63', 'subdistrictName'=> ' Abhaynagar-অভয়নগর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>544, 'division_id'=> '7', 'district_id'=> '63', 'subdistrictName'=> ' Bagherpara-বাঘারপাড়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>545, 'division_id'=> '7', 'district_id'=> '63', 'subdistrictName'=> ' Chaugachha-চৌগাছা  ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>546, 'division_id'=> '7', 'district_id'=> '63', 'subdistrictName'=> ' Jhikargachha-ঝিকরগাছা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>547, 'division_id'=> '7', 'district_id'=> '63', 'subdistrictName'=> ' Keshabpur-কেসবপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>548, 'division_id'=> '7', 'district_id'=> '63', 'subdistrictName'=> ' Jessore Sadar-যশোর সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>549, 'division_id'=> '7', 'district_id'=> '63', 'subdistrictName'=> ' Manirampur-মনিরামপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>550, 'division_id'=> '7', 'district_id'=> '63', 'subdistrictName'=> ' Sharsha-শার্শা  ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>551, 'division_id'=> '7', 'district_id'=> '64', 'subdistrictName'=> ' Harinakunda-হরিণাকুণ্ড ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>552, 'division_id'=> '7', 'district_id'=> '64', 'subdistrictName'=> ' Jhenaidah Sadar-ঝিনাইদহ সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>553, 'division_id'=> '7', 'district_id'=> '64', 'subdistrictName'=> ' Kaliganj-কালিগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>554, 'division_id'=> '7', 'district_id'=> '64', 'subdistrictName'=> ' Kotchandpur-কোটচাঁদপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>555, 'division_id'=> '7', 'district_id'=> '64', 'subdistrictName'=> ' Maheshpur-মহেসপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>556, 'division_id'=> '7', 'district_id'=> '64', 'subdistrictName'=> ' Shailkupa-শৈলকুপা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>557, 'division_id'=> '7', 'district_id'=> '65', 'subdistrictName'=> ' Bheramara-ভেড়ামারা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>558, 'division_id'=> '7', 'district_id'=> '65', 'subdistrictName'=> ' Daulatpur-দৌলতপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>559, 'division_id'=> '7', 'district_id'=> '65', 'subdistrictName'=> ' Khoksa-খোকসা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>560, 'division_id'=> '7', 'district_id'=> '65', 'subdistrictName'=> ' Kumarkhali-কুমারখালী  ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>561, 'division_id'=> '7', 'district_id'=> '65', 'subdistrictName'=> ' Kushtia Sadar-কুষ্ঠিয়া সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>562, 'division_id'=> '7', 'district_id'=> '65', 'subdistrictName'=> ' Mirpur-মিরপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>563, 'division_id'=> '7', 'district_id'=> '65', 'subdistrictName'=> ' Shekhpara-শেখপারা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>564, 'division_id'=> '7', 'district_id'=> '66', 'subdistrictName'=> ' Magura Sadar-মাগুরা সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>565, 'division_id'=> '7', 'district_id'=> '66', 'subdistrictName'=> ' Mohammadpur-মোহাম্মদপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>566, 'division_id'=> '7', 'district_id'=> '66', 'subdistrictName'=> ' Shalikha-শালিখা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>567, 'division_id'=> '7', 'district_id'=> '66', 'subdistrictName'=> ' Sreepur-শ্রীপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>568, 'division_id'=> '7', 'district_id'=> '67', 'subdistrictName'=> ' Gangni-গাংনি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>569, 'division_id'=> '7', 'district_id'=> '67', 'subdistrictName'=> ' Meherpur Sadar-মেহেরপুর সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>570, 'division_id'=> '7', 'district_id'=> '67', 'subdistrictName'=> ' Mujibnagar-মুজিবনগর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>571, 'division_id'=> '7', 'district_id'=> '68', 'subdistrictName'=> ' Kalia-কালিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>572, 'division_id'=> '7', 'district_id'=> '68', 'subdistrictName'=> ' Lohagara-লোহাগড়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>573, 'division_id'=> '7', 'district_id'=> '68', 'subdistrictName'=> ' Narail Sadar-নরাইল সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>574, 'division_id'=> '7', 'district_id'=> '68', 'subdistrictName'=> ' Naragati-নারাগাতি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>575, 'division_id'=> '7', 'district_id'=> '69', 'subdistrictName'=> ' Assasuni-আশাশুনি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>576, 'division_id'=> '7', 'district_id'=> '69', 'subdistrictName'=> ' Debhata-দেবহাতা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>577, 'division_id'=> '7', 'district_id'=> '69', 'subdistrictName'=> ' Kalaroa-কলারোয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>578, 'division_id'=> '7', 'district_id'=> '69', 'subdistrictName'=> ' Kaliganj-কালিগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>579, 'division_id'=> '7', 'district_id'=> '69', 'subdistrictName'=> ' Satkhira Sadar-সাতকিরা সদর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>580, 'division_id'=> '7', 'district_id'=> '69', 'subdistrictName'=> ' Shyamnagar-শ্যামনগর  ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],

            [ 'subdist_id'=>581, 'division_id'=> '7', 'district_id'=> '69', 'subdistrictName'=> ' Tala-তালা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
        ]);
        $this->command->info("subdistricts table seeded :) created by Rejvi email:rejvi.nomani@gmail.com");
    }
}

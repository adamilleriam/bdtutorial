<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class DivisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('divisions')->insert([
		    [ 'division_id'=>1, 'divisionName'=> 'Dhaka-ঢাকা', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'division_id'=>2, 'divisionName'=> 'Barisal-বরিশাল', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'division_id'=>3, 'divisionName'=> 'Chittagong-চট্টগ্রাম', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'division_id'=>4, 'divisionName'=> 'Rajshahi-রাজশাহী', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'division_id'=>5, 'divisionName'=> 'Sylhet-সিলেট', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'division_id'=>6, 'divisionName'=> 'Rangpur-রংপুর', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'division_id'=>7, 'divisionName'=> 'Khulna-খুলনা', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
        
		]);
		       $this->command->info("Divisions table seeded :) created by Rejvi email:rejvi.nomani@gmail.com");
   
    }
}

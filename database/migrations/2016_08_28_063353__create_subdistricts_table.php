<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubdistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subdistricts', function (Blueprint $table) {
            $table->increments('subdist_id');
            $table->integer('division_id');
            $table->integer('district_id');
            $table->string('subdistrictName');
            $table->timestamps();
        });
        Artisan::call('db:seed', [
            '--class' => SubDistrictSeeder::class,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subdistricts');
    }
}

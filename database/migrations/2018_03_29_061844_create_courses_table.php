<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('topic_slug');
            $table->integer('trainer_id');
            $table->integer('sequence')->nullable();
            $table->string('file');
            $table->text('overview');
            $table->text('what_will_i_learn');
            $table->text('requirements');
            $table->text('url');
            $table->boolean('featured');
            $table->boolean('publish');
            $table->enum('status',['active', 'inactive'])->default('active');
            $table->softDeletes();
            $table->unsignedInteger('created_by',false)->default(0);
            $table->unsignedInteger('updated_by',false)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}

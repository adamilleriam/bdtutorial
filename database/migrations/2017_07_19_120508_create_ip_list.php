<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIpList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ip_list', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip',255);
            $table->string('attempt')->default(0);
            $table->string('last_attempt')->nullable();
            $table->string('total_short_time_attempt')->default(0);
            $table->enum('status', ['Active','Blocked'])->deafult('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ip_list');
    }
}

<?php
Route::get('academic', [
    'as' => 'website_academic.index',
    'uses' => 'AcademicController@index',
]);
Route::get('academic/{class}/{subject}', [
    'as' => 'website_academic.chapter',
    'uses' => 'AcademicController@chapter',
]);
Route::get('professional-training/{category?}', [
    'as' => 'website_training.index',
    'uses' => 'ProfessionalTrainingController@index',
]);
Route::get('course/{slug}', [
    'as' => 'website_training.details',
    'uses' => 'ProfessionalTrainingController@details',
]);
Route::get('course/{slug}/{module}', [
    'as' => 'website_module.details',
    'uses' => 'ProfessionalTrainingController@moduleDetails',
]);
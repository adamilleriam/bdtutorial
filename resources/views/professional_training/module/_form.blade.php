<div class="col-md-8">

    <div class="form-group">
        {!! Form::label('Name') !!}
        :
        {!! Form::text('name',null,['class'=>'form-control','required']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Overview') !!}
        {!! Form::textarea('overview',null,['class'=>'form-control fullEditor']) !!}
    </div>


</div>
<div class="col-md-4">
    <div class="form-group">
        {!! Form::label('Video Link ( Embed Code Url Only)') !!}
        :
        {!! Form::url('url',null,['class'=>'form-control','placeholder'=>'https://exampal.com','required']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Sequence') !!}
        :
        {!! Form::number('sequence',null,['class'=>'form-control','placeholder'=>'0']) !!}
    </div>
        @if(isset($module)&& $module->file!=null)
        <a href="{{asset($module->file)}}" target="_blank">Download File / Show File</a>
        @endif
    <div class="form-group">
        {!! Form::label('File Upload (if needed) ') !!}
        : <b>max size 5mb (pdf,doc,zip,rar)</b>
        {!! Form::file('files',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Status') !!}
        :
        {!! Form::radio('status','active',null,['class'=>'minimal','checked']) !!} Active
        {!! Form::radio('status','inactive',null,['class'=>'minimal']) !!} Inactive
    </div>
</div>
@section('customJs')
    <script src="{!! asset('asset/js/tinymce/tinymce.min.js') !!}"></script>

    <script>
        tinymce.init({
            selector: 'textarea.fullEditor',
            height: 150,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
            image_advtab: true,
            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],
            file_browser_callback_types: 'file image media'
        });

    </script>

@endsection
<div class="col-md-8 ">
    <div class="form-group">
        {!! Form::label('Name') !!}
        :
        {!! Form::text('name',null,['class'=>'form-control','required']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Overview') !!}
        {!! Form::textarea('overview',null,['class'=>'form-control fullEditor']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('What will achieve') !!}
        {!! Form::textarea('what_will_i_learn',null,['class'=>'form-control fullEditor']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Requirements') !!}
        {!! Form::textarea('requirements',null,['class'=>'form-control fullEditor']) !!}
    </div>
</div>
<div class="col-md-4">
    @if(isset($category))
        <div class="form-group">
            {!! Form::label('Category') !!}
            {!! Form::select('topic_slug',$category,null,['class'=>'form-control','placeholder'=>'-- Please Select Category --','required']) !!}
        </div>
    @endif
    @if(isset($teacher))
        <div class="form-group">
            {!! Form::label('Teacher') !!}
            {!! Form::select('trainer_id',$teacher,null,['class'=>'form-control','placeholder'=>'-- Please Select Teacher --','required']) !!}
        </div>
    @endif
    <div class="form-group">
        {!! Form::label('Courses Overview Video Link ( Embed Code Url Only)') !!}
        :
        {!! Form::url('url',null,['class'=>'form-control','placeholder'=>'https://exampal.com','required']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Sequence') !!}
        :
        {!! Form::number('sequence',null,['class'=>'form-control','placeholder'=>'0',]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Featured Item') !!}
        :
        {!! Form::radio('featured',1,null,['class'=>'minimal','required']) !!} Yes
        {!! Form::radio('featured',0,null,['class'=>'minimal','required']) !!} No
    </div>
    <div class="form-group">
        {!! Form::label('publish') !!}
        :
        {!! Form::radio('publish',1,null,['class'=>'minimal','checked']) !!} Yes
        {!! Form::radio('publish',0,null,['class'=>'minimal']) !!} No
    </div>
        <div class="form-group">
            @if(isset($course)&& $course->file!=null)
                <img style="height: 166px;width:258px;" src="{{asset($course->file)}}" id="preview2">
                @else
            <img style="height: 166px;width:258px;" src="http://via.placeholder.com/263x195" id="preview2">
            @endif
            <div class="upload-box">
                <label class="btn" style="background: gainsboro;width: 77%;">
                    <input name="files"  onchange="document.getElementById('preview2').src = window.URL.createObjectURL(this.files[0])" style="display:none" type="file">
                    <i class="fa fa-cloud-upload"></i> Upload Feature Image
                    <i class="fa fa-cog fa-spin  fa-fw margin-bottom" id="loaderIcon"
                       style="color:green;margin-top:10px;display: none"></i>
                </label>
            </div>
        </div>
    <div class="form-group">
        {!! Form::label('Status') !!}
        :
        {!! Form::radio('status','active',null,['class'=>'minimal','checked']) !!} Active
        {!! Form::radio('status','inactive',null,['class'=>'minimal']) !!} Inactive
    </div>

</div>
@section('customJs')
    <script src="{!! asset('asset/js/tinymce/tinymce.min.js') !!}"></script>

    <script>
        tinymce.init({
            selector: 'textarea.fullEditor',
            height: 150,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
            image_advtab: true,
            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],
            file_browser_callback_types: 'file image media'
        });

    </script>

@endsection
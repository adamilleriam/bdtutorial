@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
                <div class="box-header">

                    {!! Form::open(['route'=>'training.index','method'=>'get']) !!}

                    <div class="col-md-2" style="padding-top: 5px;padding-bottom: 5px;">
                        {!! Form::select('search',['active'=>'Active','inactive'=>'Inactive','trashed'=>'Trashed'],\Illuminate\Support\Facades\Input::get('search'),[ 'class'=>'form-control',
                        'placeholder'=>'Please select','required' ]) !!}
                    </div>
                    <div class="col-md-1" style="padding-top: 5px;padding-bottom: 5px;">
                        {!! Form::submit('Search',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                    <div class="col-md-9" style="padding-top: 5px;padding-bottom: 5px;">
                        <a href="{!! route('training.create') !!}" class="btn btn-warning pull-right addNew">Add New</a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body ">
                    <table class="table table-bordered table-striped">
                        @include('layouts._message')
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Sequence</th>
                            <th>Status</th>
                            <th class="text-center" style="width: 25%;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{!! $serial++ !!}</td>
                                <td>{!! $category->name !!}</td>
                                <td>{!! $category->sequence !!}</td>
                                <td>{!!  ucfirst($category->status) !!}</td>
                                <td class="text-center">
                                    <div class="dropdown">
                                        <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown">Options
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu" style="background: ghostwhite;">

                                            <li>
                                                <a href="{!! route('training.edit',$category->id) !!}" title="Edit"> Edit
                                                            </a>
                                            </li>
                                             <li>
                                                 @if(\Illuminate\Support\Facades\Input::get('search')=='trashed')
                                                     <a href="{{route('training.restore',$category->id)}}"
                                                        onclick="return confirm('Are you confirm to restore this ?')"
                                                        title="Restore">Restore</a>
                                                     <a href="{{route('training.delete',$category->id)}}"
                                                        onclick="return confirm('Are you confirm to delete this ?')"
                                                        title="Delete">Delete</a>
                                                 @else
                                                     <a href="{!! route('training.trash',$category->id) !!}" title="Trash" onclick="return confirm('Are you confirm to trash this  ?')">Trash</a>
                                                 @endif
                                            </li>
                                        </ul>
                                    </div>


                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                    {{$categories->render()}}
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection

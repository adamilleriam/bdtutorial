@extends('layouts.master')
@section('select2css')
    <link rel="stylesheet" href="{!! asset('dist/css/select2.min.css') !!}">
@endsection
@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <form action="{{ route('book.update',$books->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('layouts._message')
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="selectIns">Select Book Category</label>
                            <select name="book_category" class="bookcategory form-control" id="selectIns">
                                <option selected disabled value="">Choose Category</option>
                                @forelse($categories as $category)
                                    <option {!! $category->id == $books->category_id ? 'selected' : ''  !!} value="{!!$category->id !!}">{!! $category->category_name !!}</option>
                                @empty
                                    <p>No category found!</p>
                                @endforelse
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="inputName">Book Name</label>
                            <input type="text" name="book_name" id="inputName" class="form-control" value="{!! $books->name !!}">
                        </div>
                        <div class="form-group">
                            <label for="typeDescription">Book Overview</label>
                            <textarea name="overview" id="typeDescription" class="form-control">{!! $books->book_overview !!}</textarea>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="author">Author Name</label>
                            <input type="text" name="author_name" id="author" class="form-control" value="{!! $books->author !!}">
                        </div>
                        <div class="form-group">
                            <label for="pdffile">Upload PDF Book</label>
                            <input type="file" name="pdf_book" id="pdffile" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Status</label><br>
                            <label class="radio-inline"><input type="radio" name="status" {!! $books->status == 'active'? 'checked' : '' !!} value="active"> Active</label>
                            <label class="radio-inline"><input type="radio" name="status" {!! $books->status == 'inactive'? 'checked' : '' !!} value="inactive"> Inactive</label>
                        </div>
                        <div class="form-group">
                            <img style="height: 210px;width:258px;" src="{!! asset($books->book_cover) !!}" id="preview">

                            <div class="upload-box">
                                <label class="btn" style="background: gainsboro;width: 50%;">
                                    <input name="book_cover" id="imgupload" style="display:none" type="file">
                                    <i class="fa fa-cloud-upload"></i> Upload Book Cover
                                    <i class="fa fa-cog fa-spin  fa-fw margin-bottom" id="loaderIcon"
                                       style="color:green;margin-top:10px;display: none"></i>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <input type="submit" name="Save"  value="Submit" class="btn btn-success pull-right">
                    </div>
                    <div class="col-xs-6">
                        <a href="{!! route('book.list') !!}" class="btn btn-warning" onclick="return confirm('Are you confirm to cancel !')">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('select2js')
    <script src="{!! asset('dist/js/select2.min.js') !!}"></script>

    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=bl182cv0ud67yxrbs9uor8xfscrz38s7bb5swlqoo5y4v7o6"></script>
    <script>
        $(document).ready(function() {
            tinymce.init({
                selector: 'textarea',
                height: 180,
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor textcolor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code help wordcount'
                ],
                toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'

            });

            $('.bookcategory').select2();
        })
    </script>
@endsection
@section('customJs')
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview').fadeIn(1000);
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgupload").change(function(){
            readURL(this);
        });

    </script>
@endsection
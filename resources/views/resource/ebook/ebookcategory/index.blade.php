@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
                <div class="box-header">
                    {!! Form::open(['route'=>'book.category.list','method'=>'get']) !!}
                    <div class="col-md-2" style="padding-top: 5px;padding-bottom: 5px;">
                        {!! Form::select('search',['active'=>'Active','inactive'=>'Inactive','trashed'=>'Trashed'],\Illuminate\Support\Facades\Input::get('search'),[ 'class'=>'form-control',
                        'placeholder'=>'Please select','required' ]) !!}
                    </div>
                    <div class="col-md-1" style="padding-top: 5px;padding-bottom: 5px;">
                        {!! Form::submit('Search',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                    <div class="col-md-9" style="padding-top: 5px;padding-bottom: 5px;">
                        <a href="{!! route('book.category.add') !!}" class="btn btn-warning pull-right addNew">Add New</a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1"  class="table table-bordered table-striped">
                        @include('layouts._message')
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th class="text-center" style="width: 25%;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($categories as $category)
                            <tr>
                                <td>{!! $serial++ !!}</td>
                                <td>{!!  $category->category_name !!}</td>
                                <td>{!!  $category->status !!}</td>
                                <td class="text-center">
                                    <a href="{!! route('book.category.edit',$category->id) !!}" class="btn btn-info"><i
                                                class="fa fa-edit" title="Edit"></i></a>
                                    @if(\Illuminate\Support\Facades\Input::get('search')=='trashed')
                                        <a href="{!! route('book.category.restore',$category->id) !!}" class="btn btn-primary"
                                           onclick="return confirm('Are you confirm to restore this?')"
                                           title="Restore"><i class="fa fa-recycle"></i></a>
                                        <a href="{!! route('book.category.delete',$category->id) !!}" class="btn btn-danger"
                                           onclick="return confirm('Are you confirm to delete this?')"
                                           title="Delete"><i class="fa fa-eraser"></i></a>

                                    @else
                                        <a href="{!! route('book.category.trash',$category->id) !!}" class="btn btn-danger"
                                           onclick="return confirm('Are you confirm to make trash this?')"
                                           title="Trash"><i class="fa fa-trash"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <p>No record found!</p>
                            @endforelse
                        </tbody>
                    </table>

                    {{  $categories->render()}}
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection

@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
                <div class="box-header">
                    {!! Form::open(['route'=>'institution.list','method'=>'get']) !!}
                    <div class="col-md-2" style="padding-top: 5px;padding-bottom: 5px;">
                        {!! Form::select('search',['active'=>'Active','inactive'=>'Inactive','trashed'=>'Trashed'],\Illuminate\Support\Facades\Input::get('search'),[ 'class'=>'form-control',
                        'placeholder'=>'Please select','required' ]) !!}
                    </div>
                    <div class="col-md-1" style="padding-top: 5px;padding-bottom: 5px;">
                        {!! Form::submit('Search',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                    <div class="col-md-9" style="padding-top: 5px;padding-bottom: 5px;">
                        <a href="{!! route('institution.add') !!}" class="btn btn-warning pull-right addNew">Add New</a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1"  class="table table-bordered table-striped">
                        @include('layouts._message')
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Logo</th>
                            <th>Status</th>
                            <th>Feature</th>
                            <th class="text-center" style="width: 25%;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($institutions as $institution)

                            <tr>
                                <td>{!! $serial++ !!}</td>
                                <td>{!! $institution->name !!}</td>
                                <td>{!! $institution->type !!}</td>
                                <td><img src="{!! asset($institution->logo) !!}" width="60" alt=""></td>
                                <td>{!! $institution->status !!}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-sm">Option <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="{!! route('institution.slider.list',$institution->id) !!}">Institution Slider</a></li>
                                            <li><a href="{!! route('institution.news.list',$institution->id) !!}">Institution News</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <a href="{!! route('institution.edit',$institution->id) !!}" class="btn btn-info"><i
                                                class="fa fa-edit" title="Edit"></i></a>
                                    @if(\Illuminate\Support\Facades\Input::get('search')=='trashed')
                                        <a href="{!! route('institution.restore',$institution->id) !!}" class="btn btn-primary"
                                           onclick="return confirm('Are you confirm to restore this?')"
                                           title="Restore"><i class="fa fa-recycle"></i></a>
                                        <a href="{!! route('institution.delete',$institution->id) !!}" class="btn btn-danger"
                                           onclick="return confirm('Are you confirm to delete this?')"
                                           title="Delete"><i class="fa fa-eraser"></i></a>

                                    @else
                                        <a href="{!! route('institution.trash',$institution->id) !!}" class="btn btn-danger"
                                           onclick="return confirm('Are you confirm to make trash this?')"
                                           title="Trash"><i class="fa fa-trash"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <p>No record found!</p>
                            @endforelse
                        </tbody>
                    </table>

                    {{  $institutions->render()}}
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection

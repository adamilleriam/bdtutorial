@extends('layouts.master')
@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <!-- /.box-header -->

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>

        <form action="{!! route('institution.slider.update',$slider->id) !!}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        @include('layouts._message')
                        <div class="form-group">
                            <label>Status</label><br>
                            <label class="radio-inline"><input type="radio" name="status"  {!! $slider->status == 'active' ? 'checked' : ''  !!}  value="active"> Active</label>
                            <label class="radio-inline"><input type="radio" name="status"  {!! $slider->status == 'inactive' ? 'checked' : ''  !!}  value="inactive"> Inactive</label>
                        </div>
                        <div class="form-group">
                            <img style="height: 250px;width:350px;" src="{!! asset($slider->image) !!}" id="preview">
                            <div class="upload-box">
                                <label class="btn" style="background: gainsboro;width: 50%;">
                                    <input name="image" id="imgupload" style="display:none" type="file">
                                    <i class="fa fa-cloud-upload"></i> Upload Institution Slider
                                    <i class="fa fa-cog fa-spin  fa-fw margin-bottom" id="loaderIcon"
                                       style="color:green;margin-top:10px;display: none"></i>
                                </label>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-success"  value="Update">
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('customJs')
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview').fadeIn(1000);
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgupload").change(function(){
            readURL(this);
        });

    </script>
@endsection

@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
                <div class="box-header">
                    {!! Form::open(['route'=>['institution.slider.list',$id],'method'=>'get']) !!}
                    <div class="col-md-2" style="padding-top: 5px;padding-bottom: 5px;">
                        {!! Form::select('search',['active'=>'Active','inactive'=>'Inactive','trashed'=>'Trashed'],\Illuminate\Support\Facades\Input::get('search'),[ 'class'=>'form-control',
                        'placeholder'=>'Please select','required' ]) !!}
                    </div>
                    <div class="col-md-1" style="padding-top: 5px;padding-bottom: 5px;">
                        {!! Form::submit('Search',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                    <div class="col-md-9" style="padding-top: 5px;padding-bottom: 5px;">
                        <a href="{!! route('institution.slider.add',$id) !!}" class="btn btn-warning pull-right addNew">Add New</a>
                        <a style="margin-right: 5px" href="{!! route('institution.list') !!}" class="btn btn-success pull-right">Back</a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1"  class="table table-bordered table-striped">
                        @include('layouts._message')
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Photos</th>
                            <th>Status</th>
                            <th class="text-center" style="width: 25%;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($sliders as $slider)

                            <tr>
                                <td>{!! $serial++ !!}</td>
                                <td><img src="{!! asset($slider->image) !!}" width="80" alt=""></td>
                                <td>{!! $slider->status !!}</td>
                                <td class="text-center">
                                    <a href="{!! route('institution.slider.edit',$slider->id) !!}" class="btn btn-info"><i
                                                class="fa fa-edit" title="Edit"></i></a>
                                    @if(\Illuminate\Support\Facades\Input::get('search')=='trashed')
                                        <a href="{!! route('institution.slider.restore',$slider->id) !!}" class="btn btn-primary"
                                           onclick="return confirm('Are you confirm to restore this?')"
                                           title="Restore"><i class="fa fa-recycle"></i></a>
                                        <a href="{!! route('institution.slider.delete',$slider->id) !!}" class="btn btn-danger"
                                           onclick="return confirm('Are you confirm to delete this?')"
                                           title="Delete"><i class="fa fa-eraser"></i></a>

                                    @else
                                        <a href="{!! route('institution.slider.trash',$slider->id) !!}" class="btn btn-danger"
                                           onclick="return confirm('Are you confirm to make trash this?')"
                                           title="Trash"><i class="fa fa-trash"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <p>No record found!</p>
                            @endforelse
                        </tbody>
                    </table>

                    {{  $sliders->render()}}
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection

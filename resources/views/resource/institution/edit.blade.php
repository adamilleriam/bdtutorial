@extends('layouts.master')
@section('select2css')
    <link rel="stylesheet" href="{!! asset('dist/css/select2.min.css') !!}">
@endsection
@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <form action="{{ route('institution.update',$institution->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('layouts._message')
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="selectIns">Select Institution Type</label>
                            <select name="institution_type" class="form-control" id="selectIns">
                                <option selected disabled value="">Choose institution</option>
                                <option {!! $institution->type == 'university' ? 'selected' : ''  !!}  value="university">University</option>
                                <option {!! $institution->type == 'college' ? 'selected' : ''  !!}  value="college">College</option>
                                <option {!! $institution->type == 'high' ? 'selected' : ''  !!}  value="high">High School</option>
                                <option {!! $institution->type == 'primary' ? 'selected' : ''  !!}  value="primary">Primary School</option>
                                <option {!! $institution->type == 'koumi' ? 'selected' : ''  !!}  value="koumi">Koumi Madrasa</option>
                                <option {!! $institution->type == 'alia' ? 'selected' : ''  !!}  value="alia">Alia Madrasa </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="inputName">Name</label>
                            <input type="text" name="name" id="inputName" class="form-control" value="{!! $institution->name !!}">
                        </div>
                        <div class="form-group">
                            <label for="inputName">Established</label>
                            <input type="text" name="established" id="inputName" class="form-control" value="{!! $institution->established !!}">
                        </div>
                        <div class="form-group">
                            <label for="typeDescription">Institution Overview</label>
                            <textarea name="overview" id="typeDescription" class="form-control">{!! $institution->overview !!}</textarea>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Status</label><br>
                            <label class="radio-inline"><input type="radio" name="status"  {!! $institution->status == 'active' ? 'checked' : ''  !!}  value="active"> Active</label>
                            <label class="radio-inline"><input type="radio" name="status"  {!! $institution->status == 'inactive' ? 'checked' : ''  !!}  value="inactive"> Inactive</label>
                        </div>
                        <div class="form-group">
                            <img style="height: 155px;width:258px;" src="{!! asset($institution->logo) !!}" id="preview">

                            <div class="upload-box">
                                <label class="btn" style="background: gainsboro;width: 50%;">
                                    <input name="logo" id="imgupload" style="display:none" type="file">
                                    <i class="fa fa-cloud-upload"></i> Upload Institution Logo
                                    <i class="fa fa-cog fa-spin  fa-fw margin-bottom" id="loaderIcon"
                                       style="color:green;margin-top:10px;display: none"></i>
                                </label>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <input type="submit" name="Save"  value="Update" class="btn btn-success pull-right">
                    </div>
                    <div class="col-xs-6">
                        <a href="{!! route('institution.list') !!}" class="btn btn-warning" onclick="return confirm('Are you confirm to cancel !')">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('select2js')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=bl182cv0ud67yxrbs9uor8xfscrz38s7bb5swlqoo5y4v7o6"></script>
    <script>
        $(document).ready(function() {
            tinymce.init({
                selector: 'textarea',
                height: 180,
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor textcolor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code help wordcount'
                ],
                toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'

            });
        })
    </script>
@endsection
@section('customJs')
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview').fadeIn(1000);
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgupload").change(function(){
            readURL(this);
        });

    </script>
@endsection
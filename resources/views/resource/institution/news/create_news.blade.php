@extends('layouts.master')
@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <!-- /.box-header -->

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>

        <form action="{!! route('institution.news.store',$id) !!}" method="post">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        @include('layouts._message')
                        <div class="form-group">
                            <label for="inputtitle">News Title</label>
                            <input type="text" name="title" class="form-control" id="inputtitle" placeholder="Enter news title">
                        </div>
                        <div class="form-group">
                            <label for="inputtitle">News Details</label>
                           <textarea name="details" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Status</label><br>
                            <label class="radio-inline"><input type="radio" name="status" checked value="active"> Active</label>
                            <label class="radio-inline"><input type="radio" name="status" value="inactive"> Inactive</label>
                        </div>
                        <input type="submit" class="btn btn-success"  value="Add">
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('select2js')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=bl182cv0ud67yxrbs9uor8xfscrz38s7bb5swlqoo5y4v7o6"></script>
    <script>
        $(document).ready(function() {
            tinymce.init({
                selector: 'textarea',
                height: 180,
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor textcolor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code help wordcount'
                ],
                toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'

            });
        })
    </script>
@endsection

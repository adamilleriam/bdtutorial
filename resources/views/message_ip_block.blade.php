<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Startup Project</title>
</head>
<body style="text-align: center;">
<h1 style="margin-top: 10%">{{ $request->ip() }}</h1>
<h3>Your IP address has been blocked due to suspicious activities. Please contact with admin.</h3>
</body>
</html>
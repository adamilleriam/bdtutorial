<div class="col-md-8">

    <div class="form-group">
        {!! Form::label('Title') !!}
        :
        {!! Form::text('name',null,['class'=>'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('Description') !!}
        :
        {!! Form::textarea('details',null,['class'=>'form-control']) !!}
    </div>

</div>
<div class="col-md-4">
    <div class="form-group">
        {!! Form::label('Video Link ( Embed Code Url Only)') !!}
        :
        {!! Form::url('url',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Sequence') !!}
        :
        {!! Form::number('sequence',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Status') !!}
        :
        {!! Form::radio('status','active',null,['class'=>'minimal','checked']) !!} Active
        {!! Form::radio('status','inactive',null,['class'=>'minimal']) !!} Inactive
    </div>

</div>
@component('mail::message')
    Dear {{$data['name']}},
    #Congratulations !!<br>
    We are very happy to inform you that, We are appointed you as a very important admin of his Website.
    Here we sent you the details of login credentials.
    Please be careful to using this credentials.<br>

    ##Credentials
    Email : {{$data['email']}}<br>
    Password : {{$data['password']}}<br>

@component('mail::button', ['url' => $data['loginUri']])
        Login
@endcomponent

    <b>N:B:</b><i>Please update your password as soon as possible.</i><br>
    Thanks,<br>
    {{ config('app.name') }}
@endcomponent

@component('mail::message')
# {{$mail['subject']}}

{{$mail['body']}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent

@component('mail::message')
# Hi, {{$user->name}}
Please verify Your Email by Clicking This Button.

@component('mail::button', ['url' => route('verify_email',[$user->id,$user->token])])
Verify
@endcomponent
Thanks,<br>
{{ config('app.name') }}
@endcomponent

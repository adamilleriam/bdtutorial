@component('mail::message')
# Hi, {{$user->name}}

Reset Your Password By Clicking This Button

@component('mail::button', ['url' => route('password_reset',[$user->id,$user->token])])
Reset Password
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent



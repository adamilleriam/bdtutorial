@component('mail::message')
    # Hi, {{$user->name}}

    Your account has been suspended due to robotic activities.Please try after 5 minutes by clicking this button.

@component('mail::button', ['url' => route('login',[$user->id,$user->token])])
    Login
@endcomponent

    If you don't do that, Someone trying to access your account.Please change your password.


    Thanks,
    {{ config('app.name') }}
@endcomponent



@component('mail::message')
    # Hi, {{$user->name}}

    We have been recorded too much wrong attempt regarding your account. As we suspect someone attempt to hack your account.That's why we forced to suspend your account.

    Please try after 5 minute by clicking this button.

@component('mail::button', ['url' => route('login',[$user->id,$user->token])])
    Login
@endcomponent

    Thanks,
    {{ config('app.name') }}
@endcomponent
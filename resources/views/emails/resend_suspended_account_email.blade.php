@component('mail::message')
    # Hi, {{$user->name}}

    For login please press this button.

@component('mail::button', ['url' => route('login',[$user->id,$user->token])])
    Login
@endcomponent

    Thanks,
    {{ config('app.name') }}
@endcomponent




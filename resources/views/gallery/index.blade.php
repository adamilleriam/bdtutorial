@extends('layouts.master')
@section ('content')

    <div class="row">
        <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
                <div class="box-header">
                    {{--<div class="col-md-5" style="padding-top: 5px;padding-bottom: 5px;">--}}
                    {{--</div>--}}
                    {{--{!! Form::open(['route'=>'user.index','method'=>'get']) !!}--}}

                    {{--<div class="col-md-2" style="padding-top: 5px;padding-bottom: 5px;">--}}
                        {{--{!! Form::select('search',['Active'=>'Active','Inactive'=>'Inactive','Suspended'=>'Suspended','Trashed'=>'Trashed'],\Illuminate\Support\Facades\Input::get('search'),[ 'class'=>'form-control',--}}
                        {{--'placeholder'=>'Please select','required' ]) !!}--}}
                    {{--</div>--}}
                    {{--<div class="col-md-1" style="padding-top: 5px;padding-bottom: 5px;">--}}
                        {{--{!! Form::submit('Search',['class'=>'btn btn-primary']) !!}--}}
                    {{--</div>--}}
                    {{--{!! Form::close() !!}--}}
                    {{--<div class="col-md-2" style="padding-top: 5px;padding-bottom: 5px;">--}}
                        {{--<a href="{!! route('addUser') !!}" class="btn btn-warning pull-right addNew">Add New</a>--}}
                    {{--</div>--}}

                    <div class="col-sm-12" style="padding-top: 5px;padding-bottom: 5px;">
                        <a href="{!! route('gallery.add') !!}" class="btn btn-warning pull-right addNew">Add New</a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table  class="table table-bordered table-striped">
                        @include('layouts._message')
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th width="10%">Title</th>
                            <th width="12%">Author</th>
                            <th>Image</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th class="text-center" style="width: 10%;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $serial= 1;?>
                        @foreach($gallerys as $gallery)
                            <tr>
                                <td>{!! $serial++ !!}</td>
                                <td>{!! $gallery->title !!}</td>
                                <td>{!! $gallery->author !!}</td>
                                <td><img height="80" width="120" src="{!! asset($gallery->image) !!}" alt=""></td>
                                <td>{!! $gallery->description !!}</td>
                                <td>{!! $gallery->status !!}</td>
                                <td class="text-center">
                                    <a class="btn btn-info" href="{!! route('gallery.edit',$gallery->id) !!}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-danger" onclick="return confirm('Are you sure to delete?')" href="{!! route('gallery.detete',$gallery->id) !!}"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
            <div class="pull-right">
                {{ $gallerys->links() }}
            </div>

            <!-- /.box -->
        </div>
    </div>

@endsection

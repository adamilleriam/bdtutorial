@extends('layouts.master')
@section('select2css')
    <link rel="stylesheet" href="{!! asset('dist/css/select2.min.css') !!}">
@endsection
@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <!-- /.box-header -->

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <form action="{{ route('gallery.store') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        @include('layouts._message')
        <div class="box-body">
            <div class="row">
                <p style="padding-left: 10px;font-weight: 700">Select Location</p>
                <div class="col-sm-4">
                    <div class="location form-group">
                        <label for="division" class="sr-only">Division</label>
                        <select name="division" id="division" class="form-control js-example-basic-single" required>
                           <option selected disabled>Select Division</option>
                            @foreach($division as $item)
                                <option value="{!! $item->division_id !!}">{!! $item->divisionName !!}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="location form-group">
                        <label for="district" class="sr-only">District</label>
                        <select name="district" id="district" class="form-control js-example-basic-single" required>

                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="location form-group">
                        <label for="" class="sr-only">Thana</label>
                        <select name="thana" id="thana" class="form-control js-example-basic-single" required>

                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Title / Caption</label>
                        <input type="text" name="title" id="inputTitle" class="form-control" placeholder="Enter image Caption">
                    </div>

                    <div class="form-group">
                        <label for="inputAuthor">Author</label>
                        <input type="text" name="author" id="inputAuthor" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="typeDescription">Description</label>
                        <textarea name="description" id="typeDescription" class="form-control"></textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Status</label><br>
                        <label class="radio-inline"><input type="radio" name="status" value="active"> Active</label>
                        <label class="radio-inline"><input type="radio" name="status" value="inactive"> Inactive</label>
                        <label class="radio-inline"><input type="radio" name="status" value="suspended"> Suspended</label>
                    </div>
                    <div class="form-group">
                                <img style="height: 200px;width:250px;" src="http://via.placeholder.com/600x770" id="preview">

                        <div class="upload-box">
                            <label class="btn" style="background: gainsboro;width: 50%;">
                                <input name="image" id="imgupload" style="display:none" type="file">
                                <i class="fa fa-cloud-upload"></i> Upload Image
                                <i class="fa fa-cog fa-spin  fa-fw margin-bottom" id="loaderIcon"
                                   style="color:green;margin-top:10px;display: none"></i>
                            </label>
                        </div>
                    </div>

                </div>

            </div>
            <div class="row">
                <div class="col-xs-6">
                    <input type="submit" name="Save"  value="Submit" class="btn btn-success pull-right">
                </div>
                <div class="col-xs-6">
                    <a href="{!! route('gallery.index') !!}" class="btn btn-danger" onclick="return confirm('Are you confirm to cancel !')">Cancel</a>
                </div>
            </div>
        </div>
        </form>
    </div>
@endsection
@section('select2js')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=bl182cv0ud67yxrbs9uor8xfscrz38s7bb5swlqoo5y4v7o6"></script>
    <script src="{!! asset('dist/js/select2.min.js') !!}"></script>
    <script>
        $(document).ready(function() {

            $('#division').on('change',function () {
                var division_id =$('#division').val();
                 var div = $('#district').parent();
                 var op=" ";
                $.ajax({
                   type:'GET',
                   url:'{{ route('findDistrict') }}',
                    data:{'id':division_id},
                    success:function (data) {
                        op+='<option value="0" selected disabled>Select District</option>';
                        for(var i=0;i<data.length;i++){
                            op+='<option value="'+data[i].district_id+'">'+data[i].districtName+'</option>';
                        }
                        div.find('#district').html(" ");
                        div.find('#district').append(op);
                    },
                    error:function () {
                    }
                });
            });
            /* For Select sub district */

            $('#district').on('change',function () {
                var division_id =$('#district').val();
                var div = $('#thana').parent();
                var op=" ";
                $.ajax({
                    type:'GET',
                    url:'{{ route('findThana') }}',
                    data:{'id':division_id},
                    success:function (data) {
                        op+='<option value="0" selected disabled>Select Thana</option>';
                        for(var i=0;i<data.length;i++){
                            op+='<option value="'+data[i].subdist_id+'">'+data[i].subdistrictName+'</option>';
                        }
                        div.find('#thana').html(" ");
                        div.find('#thana').append(op);
                    },
                    error:function () {
                    }
                });
            });



            $('.js-example-basic-single').select2();

            tinymce.init({
                selector: 'textarea',
                height: 150,
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor textcolor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code help wordcount'
                ],
                toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tinymce.com/css/codepen.min.css']
            });
        });
    </script>
@endsection
@section('customJs')
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview').fadeIn(1000);
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgupload").change(function(){
            readURL(this);
        });

    </script>
@endsection
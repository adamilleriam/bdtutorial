<!DOCTYPE html>
<html>
<head>
    @include('layouts._head')

    <link rel="stylesheet" href="{!! asset('asset/plugins/iCheck/square/blue.css') !!}">
</head>
<body class="hold-transition login-page">

<div class="login-box">
    <div class="login-logo">
        <span class="logo-lg"><b>Startup</b> Project</span>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        @include('layouts._message')
        {!! Form::open(['route'=>'user.store','method'=>'post','files'=> true]) !!}

        <div class="form-group has-feedback {{ $errors->has('name') ? ' has-error' : '' }}">
            {{ Form::text('name',old('name'),['class'=>'form-control','id'=>'name','placeholder'=>'Name','required']) }}
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
            @if ($errors->has('name'))
                <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
            @endif
        </div>
        <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
            {{ Form::text('email',old('email'),['class'=>'form-control','id'=>'email_address','placeholder'=>'Email','required']) }}
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            @if ($errors->has('email'))
                <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
            @endif
        </div>
        <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
            {{ Form::password('password', ['class'=>'form-control','id'=>'Password','placeholder'=>'Password','required','minlength'=>'6']) }}
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            @if ($errors->has('password'))
                <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
            @endif
        </div>
        <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            {{ Form::password('password_confirmation', ['class'=>'form-control','id'=>'Password','placeholder'=>'Confirm password','required','minlength'=>'6']) }}
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            @if ($errors->has('password_confirmation'))
                <span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
            @endif
        </div>
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">
                    <label>
                        <input type="checkbox" name="check"> I agree to the <a href="#">terms</a>
                        @if ($errors->has('check'))
                            <span class="help-block"><strong>{{ $errors->first('check') }}</strong></span>
                        @endif
                    </label>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                {!! Form::submit('Register',['class'=>'btn btn-primary btn-block btn-flat']) !!}
            </div>
            <!-- /.col -->
        </div>
        {!! Form::close() !!}
        <a href="{!! route('login') !!}" class="text-center">I already have a membership</a>


    </div>
    <div class="pull-right" style=" padding-top: 10px;">
        <b>Powered By :</b><a href="http://peoplentech.net" target="_blank" title="PeopleNTech Software"><strong>
                PeopleNTech Software </strong></a>
    </div>
    <!-- /.login-box-body -->
</div>
@include('layouts._script')
<script src="{!! asset('asset/plugins/iCheck/icheck.min.js') !!}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>
</html>
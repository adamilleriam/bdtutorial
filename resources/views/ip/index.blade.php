@extends('layouts.master')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                @include('layouts._message')
                <div class="box-header">
                    <div class="col-md-5" style="padding-top: 5px;padding-bottom: 5px;">

                    </div>
                    {!! Form::open(['route'=>['block_ip.index'],'method'=>'get']) !!}
                    <div class="col-md-3" style="padding-top: 5px;padding-bottom: 5px;">
                        {!! Form::text('key',\Illuminate\Support\Facades\Input::get('key'),[ 'class'=>'form-control',
                         'placeholder'=>' Enter a IP address' ]) !!}
                    </div>
                    <div class="col-md-1" style="padding-top: 5px;padding-bottom: 5px;">
                        {!! Form::submit('Search',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="organogramTable" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>IP Address</th>
                            <th>Block Time</th>
                            <th>Status</th>
                            <th class="text-center">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($ip_lists as $ip_list)
                            <tr>
                                <td>{!! $serial++ !!}</td>
                                <td>{!! $ip_list->ip !!}</td>
                                <td>{!! date('Y M d H:m:s',strtotime($ip_list->updated_at)) !!}</td>
                                <td>{!! $ip_list->status !!}</td>
                                <td class="text-center">

                                        <a href="{{ route('block_ip.destroy',$ip_list->id) }}" class="btn btn-default"
                                           onclick="return confirm('Are you confirm to unblock this IP ?')" title="Delete"><i class="fa fa-trash"></i></a>


                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {{$ip_lists->render()}}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection



<message>
    @if($errors->any())
        <ul class="alert alert-danger fade in animated slideInRight alert-dismissable" style="list-style: none">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close"><i  class="fa fa-times" aria-hidden="true"></i></a>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    {{--set some message after action--}}
    @if (Session::has('message'))
        <div class="alert-dismissable alert alert-success fade in animated slideInRight">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close"><i  class="fa fa-times" aria-hidden="true"></i></a>
            {!! Session::get("message") !!}
        </div>
    @elseif(Session::has('warning'))
        <div class="alert alert-warning fade in alert-dismissable animated slideInRight">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close"><i  class="fa fa-times" aria-hidden="true"></i></a>
            {!! Session::get("warning") !!}
        </div>
    @elseif(Session::has('info'))
        <div class="alert alert-info fade in alert-dismissable animated slideInRight">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close"><i  class="fa fa-times" aria-hidden="true"></i></a>
            {!! Session::get("info") !!}
        </div>
    @elseif(Session::has('danger'))
        <div class="alert alert-danger fade in alert-dismissable animated slideInRight">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close"><i  class="fa fa-times" aria-hidden="true"></i></a>
            {!! Session::get("danger") !!}
        </div>
    @endif
</message>
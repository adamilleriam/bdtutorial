@extends('layouts.master')
@section('select2css')
    <link rel="stylesheet" href="{!! asset('dist/css/select2.min.css') !!}">
@endsection
@section('content')

    <div class="box box-default">
        <div class="box-header with-border">
            <!-- /.box-header -->

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>

        <form action="{!! route('teacher.education.update',$id) !!}" method="post">
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        @include('layouts._message')
                        <div class="form-group">
                            <label for="inputTitle">Title</label>
                            <input type="text" name="title" class="form-control" id="inputTitle" value="{!! $educations->title !!}">
                        </div>
                        <div class="form-group">
                            <label for="inputdes">Description</label>
                            <input type="text" name="description" class="form-control" id="inputdes" value="{!! $educations->description !!}">
                        </div>

                        <input type="submit" class="btn btn-info"  value="Update">
                        <a href="{!! route('teacher.education',$educations->teacher_id) !!}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Back</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@extends('layouts.master')
@section ('content')
<div class="row">
    <div class="col-xs-12">
        <!-- /.box -->
        <div class="box">
            <div class="box-header">
                <div class="col-sm-12" style="padding-top: 5px;padding-bottom: 5px;">
                    <a href="{!! route('teacher.add') !!}" class="btn btn-success pull-right addNew">Add New</a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="example1"  class="table table-bordered table-striped">
                    @include('layouts._message')
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Title</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th>Feature</th>
                        <th class="text-center" style="width: 10%;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                        @if($teachers->count())
                            @foreach($teachers as $teacher)
                            <tr>
                                <td> {{ $teacher->name }}</td>
                                <td>{{ $teacher->title }}</td>
                                <td>{{ $teacher->email }}</td>
                                <td>{{ $teacher->mobile }}</td>
                                <td><img height="80" width="80" src="{!! asset($teacher->image) !!}" alt=""></td>
                                <td>{{ $teacher->status }}</td>
                                <td class="option-col">
                                    <div class="btn-group">
                                        <button type="button" data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-sm">Option <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="{!! route('teacher.skill', $teacher->id) !!}">Show Skill</a></li>
                                            <li><a href="{!! route('teacher.education',$teacher->id) !!}">Show Education</a></li>
                                        </ul>
                                    </div>

                                </td>
                                <td>

                                    <a class="btn btn-info btn-sm" href="{!! route('teacher.edit',$teacher->id) !!}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-danger btn-sm" onclick="return confirm('Are you sure to delete?')" href="{!! route('teacher.detete',$teacher->id) !!}"><i class="fa fa-trash"></i></a>
                                </td>

                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>

            </div>
        </div>
        <div class="pull-right">

        </div>

        <!-- /.box -->
    </div>
</div>

@endsection

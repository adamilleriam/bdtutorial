@extends('Frontend.layouts.master')

    @section('content')
        <section id="page_heading">
            <div class="container-fluid">
                <div class="row">
                    <div class="page_heading_back">
                        <img src="{!! asset('Frontend/images/page_heading.jpg') !!}" alt="">
                    </div>
                    <div class="page_heading">
                        <h1>Books Details</h1>
                    </div>
                </div>
            </div>
        </section>
        <div class="container pdnt-30 pdnb-50">
            <div class="row pdnt-30">
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="book-cover text-center">
                        <img src="{!! asset($book->book_cover) !!}" alt="">
                    </div>
                    <div class="book-download margin-top-5 text-center">
                        <a target="_blank" type="application/octet-stream" download="{!!asset($book->file_path) !!}" href="{!!asset($book->file_path) !!}" class="btn-download"><i class="fa fa-arrow-down"></i> Download eBook</a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="">
                        <h4>{!! $book->name !!}</h4>
                        <p><b>Author: </b>{!! $book->author !!}</p>
                        <div class="book-info text-left">
                            <h3>Overview</h3>
                            <p>{!! $book->book_overview !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection


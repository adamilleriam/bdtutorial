@extends('Frontend.layouts.master')
@section('title')
    Tutors
@endsection
@section('content')
    <section id="page_heading">
        <div class="container-fluid">
            <div class="row">
                <div class="page_heading_back">
                    <img src="{!! asset('Frontend/images/page_heading.jpg') !!}" alt="">
                </div>
                <div class="page_heading">
                    <h1>TEACHERS</h1>
                </div>
            </div>
        </div>
    </section>
    <section id="teacher_page">
        <div class="container pdnt-30 pdnb-50">
            <div class="col-md-9">
                <div class="row">

                    @if($tutors->count())
                        @foreach($tutors as $tutor)

                    <div class="col-md-4 col-sm-3 col-xs-12">
                        <div class="grid teacher_grid">
                            <div class="grid_hover">
                                <p class="">{!! htmlspecialchars_decode($tutor->biography) !!} </p>
                            </div>
                            <figure class="">
                                <div class="teacher_img">
                                    <img height="100%" width="100%" src="{!! asset($tutor->image) !!}" alt="tutors photos">
                                </div>
                                <figcaption>
                                    <div class="text-center teacher_text">
                                        <h2 class="teacher_name pdnb-5"><a href="{!! route('tutor.info',[$tutor->id,$tutor->slug]) !!}">{!! $tutor->name !!}</a></h2>
                                        <p class="teacher_degi pdnb-5">{!! $tutor->title !!}</p>
                                        <div class="social_icon">
                                            @if($tutor->facebook_profile !=null)
                                            <span><a target="_blank" href="https://www.facebook.com/{!! $tutor->facebook_profile !!}"><i class="fa fa-facebook" aria-hidden="true"></i></a></span>
                                            @endif
                                            @if($tutor->linkedin_profile !=null)
                                            <span><a target="_blank" href="https://bd.linkedin.com/{!! $tutor->linkedin_profile !!}"><i class="fa fa-linkedin" aria-hidden="true"></i></a></span>
                                            @endif
                                             @if($tutor->twitter_profile !=null)
                                            <span><a target="_blank" href="https://twitter.com/{!! $tutor->twitter_profile !!}"><i class="fa fa-twitter" aria-hidden="true"></i></a></span>
                                           @endif
                                           @if($tutor->skype_profile !=null)
                                            <span><a target="_blank" href="https://www.skype.com/en/{!! $tutor->skype_profile !!}"><i class="fa fa-skype" aria-hidden="true"></i></a></span>
                                           @endif
                                        </div>
                                        <div class="rating pdnt-5">
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                        @endforeach
                        @else
                        <p>No record found!</p>
                        @endif
                </div><!-- // .row-->
            </div>
            <div class="col-md-3">
                <h2 class="heading">Tutors Panel</h2>
                @forelse( $tutors as $tutor)
                <a href="{!! route('tutor.info',[$tutor->id,$tutor->slug]) !!}" style="color:#1c384b;">
                    <div class="related_teacher">
                        <div class=" col-md-4 related_teacher_img">
                            <img src="{!! asset($tutor->image) !!}" alt="Nitol">
                        </div>
                        <div class=" col-md-8 related_teacher_text">
                            <h2>{!! $tutor->name !!}</h2>
                            <p>{!! $tutor->title !!}</p>
                        </div>
                    </div>
                </a>
                @empty
                    <p>No record found!</p>
                @endforelse

            </div>
        </div>
    </section>
@endsection
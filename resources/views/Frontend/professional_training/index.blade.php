@extends('Frontend.layouts.master')
@section('content')
    <section id="page_heading" style="margin-top: 0px;">
        <div class="container-fluid">
            <div class="row">
                <div class="page_heading_back">
                    <img src="{{asset('Frontend/images/page_heading.jpg')}}" alt="">
                </div>
                <div class="page_heading">
                    <h1>{{$title}}</h1>
                </div>
            </div>
        </div>
    </section>
    <section id="teacher_page">
        <div class="container pdnt-30 pdnb-50">
            <!--****************tab left***********************-->
            <!--****************tab left***********************-->
            <!--****************tab left***********************-->

            <div class="tabbable tabs-left left-tab-process" style="margin-bottom:25px;">
                <ul class="nav nav-tabs book-process-ltab col-md-2 text-center">
                    @foreach($category as $item)
                    <li class="{{request()->segment(2) == $item->slug ? 'active' : null}}"><a href="{{route('website_training.index',$item->slug)}}">{{$item->name}} </a></li>
                    @endforeach
                </ul>
                <div class="tab-content col-md-10">
                    <div class="tab-pane active">
                        <div class="row">
                            @foreach($courses as $course)
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="course_mainn">
                                        <div class="top_course_img">
                                            <img src="{{ asset($course->file) }}" alt="top course">
                                        </div>

                                        <div class="col-md-12" style="background: white">
                                            <h2 title="{{$course->name}}" class="course_title">
                                                <a href="{{route('website_training.details',$course->slug)}}">{{$course->name}}</a></h2>
                                            <p>

                                                {{$course->relTeacher->name}}
                                            </p>
                                            {{--<span class="fa fa-star checked"></span>--}}
                                            {{--<span class="fa fa-star checked"></span>--}}
                                            {{--<span class="fa fa-star checked"></span>--}}
                                            {{--<span class="fa fa-star"></span>--}}
                                            {{--<span class="fa fa-star"></span>--}}
                                            {{--<span>3</span>--}}
                                        </div>

                                        <div class="col-md-12 top_course_footer">
                                            <div class="pdnt-10">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                        {{$courses->render()}}

                    </div>
                </div>

                <!--****************end tab left***********************-->
                <!--****************end tab left***********************-->
                <!--****************end tab left***********************-->
            </div>
        </div></section>
@endsection
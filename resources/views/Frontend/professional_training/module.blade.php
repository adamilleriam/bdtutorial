@extends('Frontend.layouts.master')

@section('content')

    <section id="page_heading" style="margin-top: 0px;">
        <div class="container-fluid">
            <div class="row">
                <div class="page_heading_back">
                    <img src="{{asset('Frontend/images/page_heading.jpg')}}" alt="">
                </div>
                <div class="page_heading">
                    <h1>{{$title}}</h1>
                </div>
            </div>
        </div>
    </section>
    <section id="course_details">
        <div class="container pdnt-30 pdnb-50">
            <div class="row" style="">
                <div class="pagecolumn_reverse">
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="row">
                            <div class="course_details_left" style="position: relative;margin-bottom: 10px;">
                                <div class="">
                                    <div class="course_main" id="module">
                                        <!--<div class="course_main_text">-->
                                        @foreach($course->relModules as $item)
                                            <a class="{{request()->segment(3) == $item->slug ? 'active' : null}}" href="{{route('website_module.details',[$course->slug,$item->slug])}}"> ◉ {{$item->name}}</a>
                                    @endforeach
                                    <!--</div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-10 col-xs-12">

                        <div class="col-md-12">
                            <h1 class="article_heading">{{$module->name}}</h1>
                        </div>
                        <div class="lesson">
                            <div class="lesson_player">
                                <iframe width="100%" height="420" src="{!! $module->url !!}?rel=0&amp;showinfo=0&amp;autoplay=1" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen=""></iframe>
                            </div>


                        </div>
                        <div class="panel panel-primary course_panel_heading">
                            <div class="panel-heading">
                                <!-- Tabs -->
                                <ul class="nav panel-tabs">
                                    <li class="active"><a href="#tab1" data-toggle="tab">Overview</a></li>
                                    <li><a href="#tab2" data-toggle="tab">Ask a Question</a></li>
                                    @if($module->file!=null)
                                        <li><a href="#tab3" data-toggle="tab">File</a></li>
                                    @endif
                                </ul>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab1">
                                        <h2 class="title">Overview</h2>
                                        <p class="page_text">  {!! $module->overview !!}</p>
                                    </div>
                                    <div class="tab-pane" id="tab2">                <!--comments-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id)) return;
                                                js = d.createElement(s); js.id = id;
                                                js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.12&appId=822337971184655&autoLogAppEvents=1';
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>
                                        <div class="fb-comments" data-href="{!! url()->current() !!}" data-numposts="10"></div>

                                    </div>
                                    @if($module->file!=null)
                                        <div class="tab-pane" id="tab3">
                                            <h2>
                                            <a href="{{asset($module->file)}}" download>Download File</a>
                                            </h2>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="row">
                        <div class="related_courses hidden-sm hidden-xs margin-top-30">
                            <h3 class="course_details_teacher">Related Top Course</h3>
                            <div class="well related_courses">
                                @foreach($relatedCourses as $relatedCourse)
                                    <a href="{{route('website_training.details',$relatedCourse->slug)}}" style="color:#363838;">
                                        <div class="media">
                                            <div class="media-body">
                                                <img class="media-object related_courses_img pull-left" src="{{asset($relatedCourse->file)}}">
                                                <p style="margin-bottom: 1px;">{{$relatedCourse->name}}</p>
                                                <ul class="list-inline list-unstyled text-center">
                                                    <li>
                                                        <span class="glyphicon glyphicon-star checked"></span>
                                                        <span class="glyphicon glyphicon-star checked"></span>
                                                        <span class="glyphicon glyphicon-star checked"></span>
                                                        <span class="glyphicon glyphicon-star checked"></span>
                                                        <span class="glyphicon glyphicon-star-empty"></span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@extends('Frontend.layouts.master')

@section('content')
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=182642995848833&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    <section id="page_heading">
        <div class="container-fluid">
            <div class="row">
                <div class="page_heading_back">
                    <img src="{!! asset('Frontend/images/page_heading.jpg') !!}" alt="">
                </div>
                <div class="page_heading">
                    <h1>Quick Learn</h1>
                </div>
            </div>
        </div>
    </section>

    <!--======##########=========page content=========##########======-->
    <section id="course_details">
        <div class="container pdnt-30 pdnb-50">
            <div class="row pdnt-30" style="background: #efefef;">
                <div class="col-md-12">
                    <h1 class="article_heading">{!! $quicklearn->title !!}</h1>
                    <ul class="list-unstyled list-inline">
                        <li><i class="fa fa-calendar"></i> {!! $quicklearn->created_at->diffForHumans()  !!}</li>
                        {{--<li><i class="fa fa-comments"></i> <a href="#" style="color:#767676;">24 Comments</a></li>--}}
                    </ul>
                </div>
            </div>
            <div class="row pdnt-30" style="">
                <div class="col-md-9">
                    @if($quicklearn->url !=null)
                    <div class="quick_learn_cover">
                        <iframe width="100%" height="400" src="{!! $quicklearn->url !!}" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                    </div>
                    @else
                        <img src="{!! asset($quicklearn->feature_image) !!}" width="100%" height="400" alt="Post image">
                    @endif
                 <p class="page_text pdn-10">
                    {!! $quicklearn->description !!}
                 </p>
                 <div class="fb-share-button">
                     <iframe src="https://www.facebook.com/plugins/share_button.php?href=http%3A%2F%2Flocalhost%2Fbdtutorial%2Fpublic%2Fquick-learn%2F&layout=button_count&size=small&mobile_iframe=true&width=69&height=20&appId" width="69" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>                     <!-- Place this tag in your head or just before your close body tag. -->
                    <!-- Place this tag where you want the share button to render. -->
                     <div class="g-plus" data-action="share" data-href="{!! Request::url() !!}"></div>
                 </div>
                    <!--comments-->
                    <div class="row" style="margin:0 0;">
                        <div  class="pdnb-50 pdnt-50">
                            <div class="fb-comments" data-href="{!! Request::url() !!}" data-numposts="10"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row">
                        <div class="related_courses">
                            <h3 class="course_details_teacher">Related Post</h3>
                            <div class="well related_courses">

                              @forelse($relatedpost as $item)
                                  <div class="single-post">
                                    <a href="{!! route('quicklearn.details',$item->slug) !!}" style="color:#363838;">
                                        <div class="media">
                                            <div class="media-body">
                                                <img class="media-object related_courses_img pull-left" src="{!! asset($item->feature_image) !!}">
                                                <p style="margin-bottom: 1px;">{!! str_limit($item->title,50)  !!}</p>
                                                {{--<ul class="list-inline list-unstyled text-center">--}}
                                                {{--<li>--}}
                                                {{--<span class="glyphicon glyphicon-star checked"></span>--}}
                                                {{--<span class="glyphicon glyphicon-star checked"></span>--}}
                                                {{--<span class="glyphicon glyphicon-star checked"></span>--}}
                                                {{--<span class="glyphicon glyphicon-star checked"></span>--}}
                                                {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                                                {{--</li>--}}
                                                {{--</ul>--}}
                                            </div>
                                        </div>
                                    </a>
                                  </div>
                                  @empty
                                  <p>No record found!</p>
                                  @endforelse


                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--======##########=========page content=========##########======-->
@endsection

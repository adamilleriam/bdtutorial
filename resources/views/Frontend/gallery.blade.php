@extends('Frontend.layouts.master')
@section('title')
    Gallery
@endsection
@section('lightboxcss')
    <link rel="stylesheet" href="{{asset('Frontend/css/lightbox.min.css')}}">
    <link rel="stylesheet" href="{!! asset('dist/css/select2.min.css') !!}">
@endsection
@section('lightboxjs')
    <script src="{{asset('Frontend/js/lightbox.min.js')}}"></script>
    <script src="{!! asset('dist/js/select2.min.js') !!}"></script>
@endsection

@section('content')

    <section id="page_heading">
        <div class="container-fluid">
            <div class="row">
                <div class="page_heading_back">
                    <img src="{!! asset('Frontend/images/page_heading.jpg') !!}" alt="">
                </div>
                <div class="page_heading">
                    <h1> Gallery LIST </h1>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container-fluid gal-container">
            <div class="row search-bar">
                <div class="col-sm-12 col-md-6 col-md-offset-2">
                    <div class="simple-search">
                        <div class="form-group">
                            <form action="{!! route('search.gallery') !!}" method="get">
                                <input type="text" name="ajaxsearch" id="ajaxsearch" class="form-control" placeholder="Search gallery...." style="border-radius:  0px;">
                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-sm-12 col-md-2">
                    <button class="search-btn btn btn-info" id="adsearch" style="width: 100%;">Advance Search</button>
                </div>
            </div>

         <div class="advance-search" style="display: none;">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-md-offset-1">
                    <form action="" class="form-inline text-center">
                        <div class="form-group">
                            <label for="division" class="sr-only">Division</label>
                            <select name="division" id="division" class="form-control js-example-basic-single" required>
                                <option selected disabled>Select Division</option>
                                @foreach($divisions as $division)
                                    <option value="{!! $division->division_id !!}">{!! $division->divisionName !!}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" id="loading2">
                            <label for="district" class="sr-only">District</label>
                            <select name="district" id="district" class="form-control js-example-basic-single" required>

                            </select>
                        </div>
                        <div class="form-group" id="loading3">
                            <label for="" class="sr-only">Thana</label>
                            <select name="subdistrict" id="subdistrict" class="form-control js-example-basic-single" required>

                            </select>
                        </div>
                    </form>
                </div>

            </div>
         </div>

            <div class="generaldata">
                @if($gallerys->count())
                    @foreach($gallerys as $gallery)
                        <div class="col-md-4 col-sm-6 co-xs-12 gal-item">
                            <div class="box">
                                <a href="{{ asset($gallery->image )}}" data-lightbox="image-1" data-title="{!! $gallery->title !!}">
                                    <img src=" {{ asset($gallery->image) }}" alt="">
                                </a>
                                <div class="col-md-12 description">
                                    <h4>{!! $gallery->title !!}- Photographer:{!! $gallery->author !!}</h4>

                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
                <div class="text-center">
                    {!! $gallerys->links() !!}
                </div>

            </div>
            <div class="ajaxdata" id="success" style="display: none">

            </div>
        </div>
    </section>

    <script>
        $(document).ready(function () {

            $('#loading2').hide();
            $('#loading3').hide();
            $('#ajaxsearch').on('keyup',function () {
                var search = $(this).val();

                if(search){
                    $('.ajaxdata').show();
                    $('.generaldata').hide();
                }else {
                    $('.ajaxdata').hide();
                    $('.generaldata').show();
                }
                $.ajax({
                    type:'GET',
                    url:'{{ route('search.gallery') }}',
                    data:{'search':search},
                    datatype:'html',
                    success:function (response) {
                        console.log(response);
                        $('#success').html(response);

                    },
                    error:function () {
                    }
                });
           });
         });

        /* Search gallery by division*/
        $(document).ready(function () {
              $('#division').on('change',function () {
                  var division = $(this).val();
                  $('#loading2').show();
                  $('#loading3').hide();

                  if(division){
                      $('.ajaxdata').show();
                      $('.generaldata').hide();
                  }else {
                      $('.ajaxdata').hide();
                      $('.generaldata').show();
                  }
                  $.ajax({
                      type:'GET',
                      url:'{{ route('galleryBydivision') }}',
                      data:{'search':division},
                      datatype:'html',
                      success:function (response) {

                          $('#success').html(response);
                      },
                      error:function () {
                      }
                  });
              });
        });

   /* Find district */
        $(document).ready(function () {

            $('#division').on('change',function () {
                var division = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: '{{ route('search.district') }}',
                    data: {'id': division },
                    datatype: 'html',
                    success: function (response) {
                        //  console.log(response);
                        $('#district').html(response);
                    },
                    error: function () {
                    }
                });
            });
        });
        /* Search gallery by district */
        $(document).ready(function () {
            $('#district').on('change',function () {
                $('#loading3').show();
                var district = $(this).val();
                if(district){
                    $('.ajaxdata').show();
                    $('.generaldata').hide();
                }else {
                    $('.ajaxdata').hide();
                    $('.generaldata').show();
                }
                $.ajax({
                    type:'GET',
                    url:'{{ route('galleryBydistrict') }}',
                    data:{'id':district},
                    datatype:'html',
                    success:function (response) {
                        $('#success').html(response);
                    },
                    error:function () {
                    }
                });
            });
        });



    /* for sub districts */

        $(document).ready(function () {
            /* Find sub district */
            $('#district').on('change',function () {
                var district = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: '{{ route('search.subdistrict') }}',
                    data: {'id': district },
                    datatype: 'html',
                    success: function (response) {
                       $('#subdistrict').html(response);
                    },
                    error: function () {
                    }
                });
            });
        });

        /* find district gallery */
        $(document).ready(function () {

            $('#subdistrict').on('change',function () {
                var subdistrict = $(this).val();
                if(district){
                    $('.ajaxdata').show();
                    $('.generaldata').hide();
                }else {
                    $('.ajaxdata').hide();
                    $('.generaldata').show();
                }
                $.ajax({
                    type:'GET',
                    url:'{{ route('searchBysubdistrict') }}',
                    data:{'id':subdistrict},
                    datatype:'html',
                    success:function (response) {
                        $('#success').html(response);
                    },
                    error:function () {
                    }
                });
            });

        });

        $(document).ready(function(){
            $('.js-example-basic-single').select2();
            // Toggles paragraphs display with sliding
            $("#adsearch").click(function(){
                $(".advance-search").slideToggle(500);
            });
        });

    </script>
@endsection
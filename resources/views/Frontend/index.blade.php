@extends('Frontend.layouts.master')
@section('title')
    Home
@endsection
@section('content')
    <!--=========##########=========banner=========##########=========-->
    <section id="banner">
        <div class="container-fluid">
            <div class="row">
                <div class="banner_main">
                    <div class="banner_cover">
                        <div class="banner_aaset text-center">
                            <h3 class="bnr_subtitle">Check Out</h3>
                            <h1 class="bnr_title pdnb-30">OUR LATEST TUTORIALS</h1>
                            <div class="col-md-12 col-sm-12 col-xs-12 course_search">
                                <form class="search_item text-center" action="" method="get">
                                    <input type="text" placeholder="Search by Keyword">
                                    <select class="text-left" name="" title="select category">
                                        <option value="" disabled="" selected="" hidden="">Chose Category</option>
                                        <option value="1">Academic</option>
                                        <option value="2">Professional</option>
                                    </select>

                                    <select class="search_item text-center" name="loc" title="Select topic">
                                        <option value="" disabled="" selected="" hidden="">Chose Course</option>
                                        <option value="1">Graphics Design</option>
                                        <option value="2">Web Development</option>
                                        <option value="3">SEO</option>
                                        <option value="">CMS</option>
                                    </select>
                                    <input type="submit" name="search" value="⌕ Search">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--=========##########=========banner=========##########=========-->

    <!--=========##########=========Service=========##########=========-->
    <section id="service">
        <div class="container">
            <div class="row pdnt-40 pdnb-40">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="{!! route('quick.learn') !!}">
                        <div class="service_img">
                            <img src="{{ asset('Frontend/images/quick.jpg') }}" alt="quick learn">
                            <h2 class="service_title">Quick learn</h2>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="{{route('website_training.index')}}">
                        <div class="service_img">
                            <img src="{{ asset('Frontend/images/course.jpg') }}" alt="quick learn">
                            <h2 class="service_title">Courses</h2>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="{!! route('resource') !!}">
                        <div class="service_img">
                            <img src="{{ asset('Frontend/images/resource.jpg') }}" alt="quick learn">
                            <h2 class="service_title">Resources</h2>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!--=========##########=========Service=========##########=========-->

    <!--=========##########=======Top courses=======##########=========-->
    <section id="top_course">
        <div class="container pdnt-40 pdnb-40">
            <div class="row">
                <div class="panel with-nav-tabs panel-default">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs text-center ">

                            <li><a class="title pdnb-20" href="{{route('website_training.index')}}">Top Professional Courser</a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane fade in active">
                                @foreach($courses as $course)
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                    <div class="course_mainn">
                                        <div class="top_course_img">
                                            <img src="{{ asset($course->file) }}" alt="top course">
                                        </div>

                                        <div class="col-md-12" style="background: white">
                                            <h2 title="Making website with HTML,CSS,jquery Bootstrap" class="course_title">
                                                <a href="{{route('website_training.details',$course->slug)}}">{{$course->name}}</a></h2>
                                            <p>

                                                {{$course->relTeacher->name}}
                                            </p>
                                            {{--<span class="fa fa-star checked"></span>--}}
                                            {{--<span class="fa fa-star checked"></span>--}}
                                            {{--<span class="fa fa-star checked"></span>--}}
                                            {{--<span class="fa fa-star"></span>--}}
                                            {{--<span class="fa fa-star"></span>--}}
                                            {{--<span>3</span>--}}
                                        </div>

                                        <div class="col-md-12 top_course_footer">
                                            <div class="pdnt-10">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row text-center browse_all pdnt-30">
                <a class="title" href="{{route('website_training.index')}}">Browse all courses</a>
            </div>
        </div>
    </section>
    <!--=========##########=======Top courses=======##########=========-->

    <!--=========##########=======News letter=======##########=========-->
    <section id="ad_horizontal" class="pdnt-10 pdnb-10">
        <div class="container">
            <div class="row">
                <div class="ad_horizontal_img">
                    <img src="{{ asset('Frontend/images/horizontal.png') }}" alt="">
                </div>
            </div>
        </div>
    </section>
    <!--=========##########=======News letter=======##########=========-->

    <!--=========##########=========Teacher=========##########=========-->
    <section id="teacher">
        <div class="container">
            <div class="row pdnb-50 pdnt-50">
                <div class="text-center" style="color: #fff;">
                    <h2 class="title">Our Instructor</h2>
                    <p class="title_details"></p>
                </div>
                <div class="teacher_slide" data-slick='{"slidesToShow": 4, "slidesToScroll": 1}'>
                    @forelse($tutors as $tutor)
                    <div class="col-md-3">
                        <div class="grid">
                            <figure class="effect-bubba">
                                <div class="teacher_img">
                                    <img src="{{ asset($tutor->image) }}" alt="img02"/>
                                </div>
                                <figcaption>
                                    <div class="text-center teacher_text">
                                        <h2 class="teacher_name pdnb-5"><a href="{!! route('tutor.info',[$tutor->id,$tutor->slug]) !!}">{!! $tutor->name !!}</a></h2>
                                        <p class="teacher_degi pdnb-5">{!! $tutor->title !!} </p>
                                        <div class="social_icon">
                                            @if($tutor->facebook_profile !=null)
                                                <span><a target="_blank" href="https://www.facebook.com/{!! $tutor->facebook_profile !!}"><i class="fa fa-facebook" aria-hidden="true"></i></a></span>
                                            @endif
                                            @if($tutor->linkedin_profile !=null)
                                                <span><a target="_blank" href="https://bd.linkedin.com/{!! $tutor->linkedin_profile !!}"><i class="fa fa-linkedin" aria-hidden="true"></i></a></span>
                                            @endif
                                            @if($tutor->twitter_profile !=null)
                                                <span><a target="_blank" href="https://twitter.com/{!! $tutor->twitter_profile !!}"><i class="fa fa-twitter" aria-hidden="true"></i></a></span>
                                            @endif
                                            @if($tutor->skype_profile !=null)
                                                <span><a target="_blank" href="https://www.skype.com/en/{!! $tutor->skype_profile !!}"><i class="fa fa-skype" aria-hidden="true"></i></a></span>
                                            @endif
                                        </div>
                                        <div class="rating pdnt-5">

                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                        @empty
                        <p>No tutors found!</p>
                    @endforelse
                </div>
            </div>
        </div>
    </section>
    <!--=========##########=========Teacher=========##########=========-->
    <!--=====##########=======Become a instractor=======##########=====-->
    <section id="become_instructor">
        <div class="container">
            <div class="row pdnb-50 pdnt-50">
                <div class="col-md-8" style="background:#ddd;">
                    <div class="col-md-5">
                        <div class="become_img">
                            <img src="{{ asset('Frontend/images/become.png') }}" alt="">
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="instructor_main text-center pdnt-50 pdnb-50">
                            <h1>Become a instrator</h1>
                            <p>But I must explain to you how all this mistaken idea of denouncing
                                pleasure and praising pain was born and I will give</p>
                            <a class="cortana" href="#">Start here</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="ad_main">
                        <img src="{{ asset('Frontend/images/ad.png') }}" alt="ad">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--=====##########=======Become a instractor=======##########=====-->
    <!--=========##########=======News letter=======##########=========-->
    <section id="news_letter" class="pdnt-30 pdnb-40">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div class="text-center">
                        <h2 class="bnr_subtitle pdn-10">Stay informed with our news letter</h2>
                        <form action="" method="post">
                            <div class="">
                                <input type="text" id="" name="" placeholder="ENTER YOUR EMAIL ">
                                <input type="submit" value="SUBSCRIBE" class="btn btn-large" />
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--=========##########=======News letter=======##########=========-->

    <!--========##########=======photo gallery=======##########========-->
    <section id="gallery">
        <div class="container pdnb-50 pdnt-50">
            <div class="row">
                <div class="text-center">
                    <h2 class="title">Photo gallery</h2>
                    <p class="title_details">Lorem ipsum dolor sit amet, consectetur adipiscing elit <br>
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                </div>
                <div class="gallery">
                    @if($gallerys->count())
                        @foreach($gallerys as $gallery)
                            <div class="col-md-3">
                                <div class="gallery_img">
                                    <img src="{{ asset($gallery->image) }}" alt="gallery photo">
                                    <p>{!! $gallery->title !!}</p>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="row text-center browse_all pdnt-30">
                <a style="color:#2c4658;" class="title" href="{!! route('gallery') !!}">Browse all Photos</a>
            </div>
        </div>
    </section>
    <!--========##########=======photo gallery=======##########========-->

    <!--========##########========Testimonial========##########========-->
    <section id="carousel" style="background-image:url({!! asset('Frontend/images/testi.jpg') !!});background-repeat: no-repeat;background-size: 100% 100%;">
        <div class="container pdnt-50 pdnb-50">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="quote"><i class="fa fa-quote-left fa-4x"></i></div>
                    <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
                        <!-- Carousel indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#fade-quote-carousel" data-slide-to="0"></li>
                            <li data-target="#fade-quote-carousel" data-slide-to="1"></li>
                            <li data-target="#fade-quote-carousel" data-slide-to="2" class="active"></li>
                            <li data-target="#fade-quote-carousel" data-slide-to="3"></li>
                            <li data-target="#fade-quote-carousel" data-slide-to="4"></li>
                            <li data-target="#fade-quote-carousel" data-slide-to="5"></li>
                        </ol>
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div class="item">
                                <blockquote>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
                                </blockquote>
                                <div class="profile-circle testi_image">
                                    <img src="{{ asset('Frontend/images/teacher.jpg') }}" alt="">
                                </div>
                            </div>
                            <div class="item">
                                <blockquote>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
                                </blockquote>
                                <div class="profile-circle testi_image">
                                    <img src="{{ asset('Frontend/images/teacher.jpg') }}" alt="">
                                </div>
                                <span>-</span><span>Tauhidul Islam</span>
                            </div>
                            <div class="active item">
                                <blockquote>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
                                </blockquote>
                                <div class="profile-circle testi_image">
                                    <img src="{{ asset('Frontend/images/teacher.jpg') }}" alt="">
                                </div>
                                <span>-</span><span>Tauhidul Islam</span>
                            </div>
                            <div class="item">
                                <blockquote>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
                                </blockquote>
                                <div class="profile-circle testi_image">
                                    <img src="{{ asset('Frontend/images/teacher.jpg') }}" alt="">
                                </div>
                                <span>-</span><span>Tauhidul Islam</span>
                            </div>
                            <div class="item">
                                <blockquote>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
                                </blockquote>
                                <div class="profile-circle testi_image">
                                    <img src="{{ asset('Frontend/images/teacher.jpg') }}" alt="">
                                </div>
                                V
                            </div>
                            <div class="item">
                                <blockquote>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
                                </blockquote>
                                <div class="profile-circle testi_image">
                                    <img src="{{ asset('Frontend/images/teacher.jpg') }}" alt="">
                                </div>
                                <span>-</span><span>Tauhidul Islam</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--========##########========Testimonial========##########========-->

    <!--========##########========slide nav========##########========-->
    <!--<div class="navigation" id="fp-nav">
        <div class="numbers">
            <span class="n1"><a href="#service">Service</a></span>
            <span class="n2"><a href="#top_course">Top course</a></span>
            <span class="n3"><a href="#teacher">Instructor</a></span>
            <span class="n4"><a href="#gallery">Gallery</a></span>
            <span class="n5"><a href="#common/footer.php/footer">Footer</a></span>
        </div>
    </div>-->
    <!--========##########========slide nav========##########========-->

@endsection

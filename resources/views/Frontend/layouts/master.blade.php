<!DOCTYPE html>
<html lang="en">
<!--========================== head ==============================-->
@include('Frontend.layouts._head')
<!--========================== head ==============================-->
<body>
<!--=========##########=========header=========##########=========-->
@include('Frontend.layouts._header')
<!--=========##########=========header=========##########=========-->

@yield('content')

<!--=========##########=========Footer=========##########==========-->
@include('Frontend.layouts._footer')
<!--=========##########=========Footer=========##########==========-->
{{--<script type="text/javascript" src="{!! asset('Frontend/js/SmoothScroll.js') !!}"></script>--}}
<script src="{!! asset('Frontend/js/custom.js') !!}"></script>
</body>
</html>
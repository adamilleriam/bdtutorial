<head>
    <!-- TITLE OF SITE -->
    <title> @if(isset($title)){{$title}} ::@endif BD Tutorial Zone  </title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="description" content="StartUp Landing Page Template" />
    <meta name="keywords" content="" />
    <meta name="developer" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
@yield('meta')
<!-- FAV AND TOUCH ICONS   -->
<link rel="icon" href="{!! asset('Frontend/images/favicon.ico') !!}" type="image/x-icon">

<link rel="stylesheet" href="{!! asset('Frontend/css/bootstrap.min.css') !!}">
<link rel="stylesheet" href="{!! asset('Frontend/css/slick.css') !!}"/>
@yield('lightboxcss')
<link rel="stylesheet" href="{!! asset('Frontend/css/slick-theme.css') !!}"/>
<link rel="stylesheet" href="{!! asset('Frontend/css/font-awesome.min.css') !!}">
<link rel="stylesheet" href="{!! asset('Frontend/css/style.css') !!}">
<link rel="stylesheet" href="{!! asset('Frontend/css/responsive.css') !!}">

<!--Scripts-->
<script src="{!! asset('Frontend/js/jquery-3.2.0.min.js') !!}"></script>
<script src="{!! asset('Frontend/js/bootstrap.min.js') !!}"></script>
<script src="{!! asset('Frontend/js/slick.min.js') !!}"></script>
@yield('barchartjs')
@yield('lightboxjs')
<script src="{!! asset('Frontend/js/jquery-comments.min.js') !!}"></script>
<!-- Place this tag in your head or just before your close body tag. -->
<script src="https://apis.google.com/js/platform.js" async defer></script>
</head>
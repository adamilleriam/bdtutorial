<!--home page modal-->
{{--<div id="modal-content" class="modal fade" tabindex="-1" role="dialog">--}}
    {{--<div class="modal-dialog">--}}
        {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
        {{--<h2>Lorem ipsum</h2>--}}
        {{--<span><a href="" target="_top"></a></span>--}}
    {{--</div>--}}
{{--</div>--}}
<!--home page modal-->
<footer id="footer">
    <div class="container">
        <div class="row pdnt-30 pdnb-30">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <ul class="adress" style="padding-left: 0;">
                    <span class="pdnb-10">Adress</span>
                    <li><p>Lorem ipsum dolor sit amet, vero omnis vocibus</p></li>
                    <li><p>01739529347</p></li>
                    <li><p>info@gmail.com</p></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <ul class="contact">
                    <span class="pdnb-10">Information</span>
                    <li><a href="#">Term & Condition</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Resources</a></li>
                    <li><a href="#">Carees with us </a></li>
                    <li><a href="#">Site map</a></li>
                    <li><a href="#">Contact us</a></li>
                    <li><a href="#">FAQs</a></li>
                    <li><a href="#">Notice</a></li>
                    <li><a href="#">Security Advises</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <ul class="contact">
                    <span class="pdnb-10">Information</span>
                    <li><a href="#">Term & Condition</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Resources</a></li>
                    <li><a href="#">Carees with us </a></li>
                    <li><a href="#">Site map</a></li>
                    <li><a href="#">Contact us</a></li>
                    <li><a href="#">FAQs</a></li>
                    <li><a href="#">Notice</a></li>
                    <li><a href="#">Security Advises</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="top_social pdn-5">
                    <span class=""><i class="fa fa-facebook" aria-hidden="true"></i></span>
                    <span class=""><i class="fa fa-google-plus" aria-hidden="true"></i></span>
                    <span class=""><i class="fa fa-youtube" aria-hidden="true"></i></span>
                    <span class=""><i class="fa fa-linkedin" aria-hidden="true"></i></span>
                    <span class=""><i class="fa fa-twitter" aria-hidden="true"></i></span>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom_footer" style="background: #1c384b;">
        <div class="container">
            <div class="row pdnt-15">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="#">Terms of Service</a>
                    <a href="#">Privacy</a>
                    <a href="#">Security</a>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <p style="color:#fff;" class="pull-right"> &copy;<?php echo date('Y')?> All Rights Reserved by PeopleNTech Software
                    Developed By <a style="color: #43B4AE;" target="_blank" href="https://peoplentech.net/">PeopleNTech Software</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<header>
    <div class="afix hidden-xs">
        <div class="top_nav">
            <div class="container">
                <div class="row top_nav_section">
                    <div class="col-md-6 col-sm-6">
                        <ul class="list-inline pull-left hidden-xs">
                            <li style="color:#fff;"><span>Have a question? </span> Call 01700000000</li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-6 pull">
                        <div class="top_social pull-right pdn-5">
                            <a href="#"><span class=""><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
                            <a href="#"><span class=""><i class="fa fa-google-plus" aria-hidden="true"></i></span></a>
                            <a href="#"><span class=""><i class="fa fa-youtube" aria-hidden="true"></i></span></a>
                            <a href="#"><span class=""><i class="fa fa-linkedin" aria-hidden="true"></i></span></a>
                            <a href="#"><span class=""><i class="fa fa-twitter" aria-hidden="true"></i></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="top_bar">
            <div class="container">
                <div class="row logo_section">
                    <div class="col-md-3 col-sm-3 main_logo"><a href="{{route('Home')}}"><img src="{!! asset('Frontend/images/logo.gif') !!}" alt="logo"></a></div>
                    <div class="col-md-9 col-sm-9"><!--<img class="pull-right" src="images/topad.png" alt="ad">--></div>
                </div>
            </div>
        </div>
    </div>
    <div class="menu" id="menuu">
        <div class="container">
            <div class="row">
                <div class="navbar-wrapper">
                    <div class="container-fluid row">
                        <nav class="navbar">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand hidden-md hidden-sm hidden-lg" href="#"><img src="{!! asset('Frontend/images/logo.png') !!}" alt="logo"></a>
                            </div>
                            <div id="navbar" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav">
                                    <li class="active"><a href="{!! route('Home') !!}" class="">Home</a></li>
                                    <li class="{{request()->segment(1) == 'academic' ? 'active' : null}}"><a href="{{route('website_academic.index')}}">Academic Training</a></li>
                                    <li class="{{request()->segment(1) == 'academic' ? 'active' : null}}"><a href="{{route('website_training.index')}}">Professional Training</a></li>
                                    <li><a href="{!! route('tutors') !!}">Tutors</a></li>
                                    {{--<li class=" dropdown">--}}
                                        {{--<a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Skill Development  <span class="caret"></span></a>--}}
                                        {{--<ul class="dropdown-menu">--}}
                                            {{--<!--<li class=" dropdown">--}}
                                                {{--<a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">View Departments</a>--}}
                                            {{--</li>-->--}}
                                            {{--<li><a href="#">Web Development</a></li>--}}
                                            {{--<li><a href="#">Resume Preparetion</a></li>--}}
                                            {{--<li><a href="#">Interview Preparetion</a></li>--}}
                                            {{--<li><a href="#">Reknowned Places</a></li>--}}
                                            {{--<li><a href="#">Reknowned Places</a></li>--}}
                                            {{--<li><a href="#">Reknowned Places</a></li>--}}
                                        {{--</ul>--}}
                                    {{--</li>--}}
                                    <li><a href="{!! route('gallery') !!}">Gallary</a></li>
                                    <li><a href="#">Contact us</a></li>
                                </ul>
                                   <ul class="nav hidden-sm navbar-nav pull-right">
                                       @if(Auth::guest())
                                       <li><a href="{!! route('login') !!}">Sign In</a></li>
                                        @else
                                           <li class="dropdown">
                                               <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                   {{ Auth::user()->name }} <span class="caret"></span>
                                               </a>

                                               <ul class="dropdown-menu" role="menu">
                                                   <li>
                                                       <a href="{{ route('logout') }}"
                                                          onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                           Logout
                                                       </a>
                                                       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                           {{ csrf_field() }}
                                                       </form>
                                                   </li>
                                               </ul>
                                           </li>
                                       @endif
                                    </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>


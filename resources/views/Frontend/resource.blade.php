@extends('Frontend.layouts.master')
@section('title')
    Resources
@endsection
@section('content')
    <section id="page_heading">
        <div class="container-fluid">
            <div class="row">
                <div class="page_heading_back">
                    <img src="{!! asset('Frontend/images/page_heading.jpg') !!}" alt="">
                </div>
                <div class="page_heading">
                    <h1>RESOURCE</h1>
                </div>
            </div>
        </div>
    </section>
    <!--======##########=========page heading=========##########======-->

    <!--======##########=========page content=========##########======-->
    <section id="resource_page">
        <div class="container pdnt-30 pdnb-50">
            <div class="row pdnt-30">
                <div class="col-md-3" style="margin-bottom: 15px;">
                    <div class="resource_img text-center">
                        <img src="{!! asset('Frontend/images/histo.jpg') !!}" alt="">
                    </div>
                    <div class="resource_img_hover">
                        <p class="hover_text">Here is a platonic journey through the top eight historical places in Bangladesh. Lalbagh Fort in Dhaka. Kotila Mura in Comilla. The Liberation War Museum in Dhaka. Somapura Mahavira at Naogaon. Shalban Vihar at Comilla. Ahsan Manzil at Dhaka. The American Church in Dhaka. Mahasthangarh in Bogra</p>
                        <a href="{!! route('historicalplace.list') !!}">View</a>
                    </div>
                </div>
                <div class="col-md-3" style="margin-bottom: 15px;">
                    <div class="resource_img text-center">
                        <img src="{!! asset('Frontend/images/ebook.jpg') !!}" alt="">
                    </div>
                    <div class="resource_img_hover">
                        <p class="hover_text text-center">The world's leading online source of ebooks, with a vast range of ebooks from academic, popular and professional publishers.</p>
                        <a href="{!! route('ebooks') !!}">View</a>
                    </div>
                </div>
                <div class="col-md-3" style="margin-bottom: 15px;">
                    <div class="resource_img text-center">
                        <img src="{!! asset('Frontend/images/teach.jpg') !!}" alt="">
                    </div>
                    <div class="resource_img_hover">
                        <p class="hover_text">The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters normal distribution of letters.The point of using Lorem Ipsum more-or-less normal distribution of letters normal distribution</p>
                        <a href="#">View</a>
                    </div>
                </div>
                <div class="col-md-3" style="margin-bottom: 15px;">
                    <div class="resource_img text-center">
                        <img src="{!! asset('Frontend/images/bank.jpg') !!}" alt="">
                    </div>
                    <div class="resource_img_hover">
                        <p class="hover_text">The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters normal distribution of letters.The point of using Lorem Ipsum more-or-less normal distribution of letters normal distribution</p>
                        <a href="#">View</a>
                    </div>
                </div>
            </div>
            <div class="row pdnt-30">
                <div class="col-md-3" style="margin-bottom: 15px;">
                    <div class="resource_img text-center">
                        <img src="{!! asset('Frontend/images/school.jpg') !!}" alt="">
                    </div>
                    <div class="resource_img_hover">
                        <p class="hover_text text-center">Here is you can find all educational institution list</p>
                        <a href="{!! route('institution') !!}">View</a>
                    </div>
                </div>
                <div class="col-md-3" style="margin-bottom: 15px;">
                    <div class="resource_img text-center">
                        <img src="{!! asset('Frontend/images/bcs.jpg') !!}" alt="">
                    </div>
                    <div class="resource_img_hover">
                        <p class="hover_text">The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters normal distribution of letters.The point of using Lorem Ipsum more-or-less normal distribution of letters normal distribution</p>
                        <a href="#">View</a>
                    </div>
                </div>
                <div class="col-md-3" style="margin-bottom: 15px;">
                    <div class="resource_img text-center">
                        <img src="{!! asset('Frontend/images/teach.jpg') !!}" alt="">
                    </div>
                    <div class="resource_img_hover">
                        <p class="hover_text">The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters normal distribution of letters.The point of using Lorem Ipsum more-or-less normal distribution of letters normal distribution</p>
                        <a href="#">View</a>
                    </div>
                </div>
                <div class="col-md-3" style="margin-bottom: 15px;">
                    <div class="resource_img text-center">
                        <img src="{!! asset('Frontend/images/bank.jpg') !!}" alt="">
                    </div>
                    <div class="resource_img_hover">
                        <p class="hover_text">The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters normal distribution of letters.The point of using Lorem Ipsum more-or-less normal distribution of letters normal distribution</p>
                        <a href="#">View</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--======##########=========page content=========##########======-->
@endsection

@extends('Frontend.layouts.master')

@section('content')
<div class="loginpage">
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="login-form">
               <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Password</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary"> Login</button>
                            <a class="btn btn-link" href="{{ route('registration') }}">
                               Not register?<span class="text-danger">Register here..</span>
                            </a>
                        </div>
                    </div>
                </form>
                {{--<form action="{!! route('login.form') !!}" method="post">--}}
                    {{--{!! csrf_field() !!}--}}
                    {{--<div class="form-group">--}}
                        {{--<label for="inputEmail">Email</label>--}}
                        {{--<input type="email" class="form-control" name="email" id="inputEmail" placeholder="Enter email">--}}
                    {{--</div>--}}
                    {{--<div class="form-group">--}}
                        {{--<label for="inputPas">Password</label>--}}
                        {{--<input type="password" class="form-control" name="password" id="inputPas" placeholder="Enter password">--}}
                    {{--</div>--}}
                    {{--<input type="submit" name="submit" class="btn btn-primary" value="Login">--}}
                    {{--<span>Not Registred? <a href="{!! route('registration') !!}">Register here</a> </span>--}}
                {{--</form>--}}
            </div>
        </div>

    </div>
</div>
</div>
@endsection
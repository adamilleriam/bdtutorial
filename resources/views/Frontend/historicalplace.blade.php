@extends('Frontend.layouts.master')
@section('title')
    Historical Place
@endsection
@section('content')
    <section id="page_heading">
        <div class="container-fluid">
            <div class="row">
                <div class="page_heading_back">
                    <img src="{!! asset('Frontend/images/page_heading.jpg') !!}" alt="">
                </div>
                <div class="page_heading">
                    <h1>HISTORICAL PLACE</h1>
                </div>
            </div>
        </div>
    </section>
    <!--======##########=========page heading=========##########======-->
    <!--======##########=========page content=========##########======-->
    <section id="historical_place_list">
        <div class="container pdnt-30 pdnb-50">
            <div class="row text-center">
                <ul class="pagination pagination-sm">
                    <li class='active'><a href='#A'>A</a></li>
                    <li ><a href='#B'>B</a></li>
                    <li ><a href='#C'>C</a></li>
                    <li ><a href='#D'>D</a></li>
                    <li ><a href='#E'>E</a></li>
                    <li ><a href='#F'>F</a></li>
                    <li ><a href='#G'>G</a></li>
                    <li ><a href='#H'>H</a></li>
                    <li ><a href='#I'>I</a></li>
                    <li ><a href='#J'>J</a></li>
                    <li ><a href='#K'>K</a></li>
                    <li ><a href='#L'>L</a></li>
                    <li ><a href='#M'>M</a></li>
                    <li ><a href='#N'>N</a></li>
                    <li ><a href='#O'>O</a></li>
                    <li ><a href='#P'>P</a></li>
                    <li ><a href='#Q'>Q</a></li>
                    <li ><a href='#R'>R</a></li>
                    <li ><a href='#S'>S</a></li>
                    <li ><a href='#T'>T</a></li>
                    <li ><a href='#U'>U</a></li>
                    <li ><a href='#V'>V</a></li>
                    <li ><a href='#W'>W</a></li>
                    <li ><a href='#X'>X</a></li>
                    <li ><a href='#Y'>Y</a></li>
                    <li ><a href='#Z'>Z</a></li>
                    <li ><a href='#'>Home</a></li></ul>
            </div>
            <div class="row pdnt-30">

                @forelse($historicalplaces as $historicalplace)
                <a href="{!! route('historicalplace.details',$historicalplace->slug) !!}">
                    <div class="col-md-6 col-sm-6 col-xs-12 ">
                        <div class="place-list-item">
                            <div class="col-md-4 place-list-item-img">
                                <img src="{!! asset($historicalplace->image) !!}" alt="">
                            </div>
                            <div class="col-md-6 place-list-item-text">
                                <p href="school-details.php"> {!! $historicalplace->name !!} </p>
                            </div>
                        </div>
                    </div>
                </a>
                 @empty
                <p>No record found</p>
                 @endforelse
            </div>
            <div class="page-nation text-center">
              {!! $historicalplaces->links() !!}
            </div>

        </div>
    </section>
    <!--======##########=========page content=========##########======-->
@endsection

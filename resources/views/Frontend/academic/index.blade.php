@extends('Frontend.layouts.master')
@section('content')
    <section id="page_heading" style="margin-top: 0px;">
        <div class="container-fluid">
            <div class="row">
                <div class="page_heading_back">
                    <img src="{{asset('Frontend/images/page_heading.jpg')}}" alt="">
                </div>
                <div class="page_heading">
                    <h1>{{$title}}</h1>
                </div>
            </div>
        </div>
    </section>
    <section id="teacher_page">
        <div class="container pdnt-30 pdnb-50">
            <!--****************tab left***********************-->
            <!--****************tab left***********************-->
            <!--****************tab left***********************-->

            <div class="tabbable tabs-left left-tab-process" style="margin-bottom:25px;">
                <ul class="nav nav-tabs book-process-ltab col-md-2 text-center">
                    <li class="{{request('search') == 'primary' ? 'active' : null}}@if(isset($active)) {{$active}} @endif"><a href="{{route('website_academic.index')}}?search=primary"><i class="fa fa-book" aria-hidden="true"></i> Primary</a></li>
                    <li class="{{request('search') == 'high-school' ? 'active' : null}}"><a href="{{route('website_academic.index')}}?search=high-school" ><i class="fa fa-book" aria-hidden="true"></i> High School</a></li>
                    <li class="{{request('search') == 'collage' ? 'active' : null}}"><a href="{{route('website_academic.index')}}?search=collage" ><i class="fa fa-book" aria-hidden="true"></i> Collage</a></li>
                    <li class="{{request('search') == 'university' ? 'active' : null}}"><a href="{{route('website_academic.index')}}?search=university" ><i class="fa fa-book" aria-hidden="true"></i> University</a></li>

                </ul>
                <div class="tab-content col-md-10">
                    <div class="tab-pane active">
                        <div class="row">
                            @foreach($AcademicClass as $classlist)

                            <div class="col-md-4" style="position: relative;margin-bottom: 10px;">
                                <div class="cortanna">
                                    <div class="course_maiin text-center">
                                        <h2 class="course_main_title"> {{$classlist->name}}</h2>

                                    </div>
                                    <div class="course_main">
                                        <!--<div class="course_main_text">-->
                                        <h3 class="course_main_title"> {{$classlist->name}}</h3>
                                    @foreach($classlist->relSubject as $subject)
                                        <a class="" href="{{route('website_academic.chapter',[$subject->class_id,$subject->slug])}}"> ◉ {{$subject->name}}</a>
                                    @endforeach

                                    </div>
                                </div>


                            </div>

                            @endforeach

                        </div>

                    </div>
                </div>

                <!--****************end tab left***********************-->
                <!--****************end tab left***********************-->
                <!--****************end tab left***********************-->
            </div>
        </div></section>
@endsection
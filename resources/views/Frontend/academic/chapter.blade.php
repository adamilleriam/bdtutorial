@extends('Frontend.layouts.master')
@section('content')
    <section id="page_heading" style="margin-top: 0px;">
        <div class="container-fluid">
            <div class="row">
                <div class="page_heading_back">
                    <img src="{{asset('Frontend/images/page_heading.jpg')}}" alt="">
                </div>
                <div class="page_heading">
                    <h1>{{$title}}</h1>
                </div>
            </div>
        </div>
    </section>
    <section id="teacher_page">
        <div class="container pdnt-30 pdnb-50">
            <!--****************tab left***********************-->

            <div class="tabbable tabs-left left-tab-process" style="margin-bottom:25px;">

                <ul class="nav nav-tabs book-process-ltab col-md-2 text-center">
                    <p class="list-header">Subject List</p>
                @foreach($subjects as $subject)
                    <li class="{{request()->segment(3) == $subject->slug ? 'active' : null}}"><a href="{{route('website_academic.chapter',[$subject->class_id,$subject->slug])}}"><i class="fa fa-book" aria-hidden="true"></i> {{$subject->name}}</a></li>
                @endforeach
                </ul>
                @if(count($chapters)>0)
                <div class="col-md-4">


                        <div class="panel-group">
                            @foreach($chapters as $chapter)

                            <div class="panel panel-default chapter-panel">
                                <div class="panel-heading chapter-heading" style="">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#{{$chapter->id}}">{{$chapter->name}}</a>
                                    </h4>
                                </div>
                                <div id="{{$chapter->id}}" class="panel-collapse collapse">
                                    <div class="panel-body chapter-body" >
                                        @foreach($chapter->relContent as $content)
                                      <?php
                                      $url=$content->url.'?autoplay=1';
                                      $id='content'.$content->id;
                                      ?>
                                        <p class="pointer" id="{{$id}}" onclick="plyVideo('{{$url}}')"> ◉ {{$content->name}}</p>

                                          <script>
                                              $('#{{$id}}').click(function (e) {
                                                  $('.pointer').removeClass('contentActive');
                                                  $('#{{$id}}').addClass('contentActive');
                                              });
                                          </script>

                                         @endforeach
                                    </div>

                                </div>
                            </div>
                            @endforeach
                        </div>


                </div>
                @endif

                <div class="tab-content col-md-6">
                    <div class="tab-pane active">
                        <div class="row">
                            <iframe id="plyVideo"  width="100%" height="320" src="{{$class_subject->url}}?autoplay=1" frameborder="0"  allowfullscreen></iframe>
                       </div>

                    </div>
                </div>

                <!--****************end tab left***********************-->
            </div>
        </div></section>
    <script>
        function plyVideo(url) {
            var videoUrl = url;
            $('#plyVideo').attr('src', videoUrl);
        }
    </script>
@endsection




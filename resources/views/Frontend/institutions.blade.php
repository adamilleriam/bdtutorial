@extends('Frontend.layouts.master')

@section('content')
    <section id="page_heading">
        <div class="container-fluid">
            <div class="row">
                <div class="page_heading_back">
                    <img src="{!! asset('Frontend/images/page_heading.jpg') !!}" alt="">
                </div>
                <div class="page_heading">
                    <h1>Institutions</h1>
                </div>
            </div>
        </div>
    </section>
    <!--======##########=========page heading=========##########======-->
    <!--======##########=========page content=========##########======-->
    <section id="historical_place_list">
        <div class="container pdnt-30 pdnb-50">
            <div class="row text-center">
                <ul class="pagination pagination-sm">
                    <li class='active'><a href='#A'>A</a></li>
                    <li ><a href='#B'>B</a></li>
                    <li ><a href='#C'>C</a></li>
                    <li ><a href='#D'>D</a></li>
                    <li ><a href='#E'>E</a></li>
                    <li ><a href='#F'>F</a></li>
                    <li ><a href='#G'>G</a></li>
                    <li ><a href='#H'>H</a></li>
                    <li ><a href='#I'>I</a></li>
                    <li ><a href='#J'>J</a></li>
                    <li ><a href='#K'>K</a></li>
                    <li ><a href='#L'>L</a></li>
                    <li ><a href='#M'>M</a></li>
                    <li ><a href='#N'>N</a></li>
                    <li ><a href='#O'>O</a></li>
                    <li ><a href='#P'>P</a></li>
                    <li ><a href='#Q'>Q</a></li>
                    <li ><a href='#R'>R</a></li>
                    <li ><a href='#S'>S</a></li>
                    <li ><a href='#T'>T</a></li>
                    <li ><a href='#U'>U</a></li>
                    <li ><a href='#V'>V</a></li>
                    <li ><a href='#W'>W</a></li>
                    <li ><a href='#X'>X</a></li>
                    <li ><a href='#Y'>Y</a></li>
                    <li ><a href='#Z'>Z</a></li>
                    <li ><a href='#'>Home</a></li></ul>
            </div>
            <div class="row pdnt-30">
                    <section id="school_list">
                        <div class="container pdnt-30 pdnb-50">
                            <div class="row pdnt-30">
                                <div class="col-md-4" style="margin-bottom: 15px;">
                                    <a href="{!! route('institution.type','university') !!}">
                                        <div class="text-center institution_img">
                                            <img src="{!! asset('Frontend/images/university.jpg') !!}" alt="">
                                        </div>
                                        <div class="institution_img_hover">
                                            <h2 class="text-center bnr_subtitle">University List</h2>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-4" style="margin-bottom: 15px;">
                                    <a href="{!! route('institution.type','college') !!}">
                                        <div class="text-center institution_img">
                                            <img src="{!! asset('Frontend/images/college.jpg') !!}" alt="">
                                        </div>
                                        <div class="institution_img_hover">
                                            <h2 class="text-center bnr_subtitle">College List</h2>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-4" style="margin-bottom: 15px;">
                                    <a href="{!! route('institution.type','high') !!}">
                                        <div class="text-center institution_img">
                                            <img src="{!! asset('Frontend/images/high.jpg') !!}" alt="">
                                        </div>
                                        <div class="institution_img_hover">
                                            <h2 class="text-center bnr_subtitle">High School List</h2>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="row pdnt-30">
                                <div class="col-md-4" style="margin-bottom: 15px;">
                                    <a href="{!! route('institution.type','primary') !!}">
                                        <div class="text-center institution_img">
                                            <img src="{!! asset('Frontend/images/primary-school.jpg') !!}" alt="">
                                        </div>
                                        <div class="institution_img_hover">
                                            <h2 class="text-center bnr_subtitle">Primary School List</h2>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-4" style="margin-bottom: 15px;">
                                    <a href="{!! route('institution.type','koumi') !!}">
                                        <div class="text-center institution_img">
                                            <img src="{!! asset('Frontend/images/koumi.jpg') !!}" alt="">
                                        </div>
                                        <div class="institution_img_hover">
                                            <h2 class="text-center bnr_subtitle">Koumi Madrasha List</h2>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-4" style="margin-bottom: 15px;">
                                    <a href="{!! route('institution.type','alia') !!}">
                                        <div class="text-center institution_img">
                                            <img src="{!! asset('Frontend/images/alia.jpg') !!}" alt="">
                                        </div>
                                        <div class="institution_img_hover">
                                            <h2 class="text-center bnr_subtitle">Alia Madrasha List</h2>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </section>
            </div>


        </div>
    </section>
    <!--======##########=========page content=========##########======-->
@endsection

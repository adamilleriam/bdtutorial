@extends('Frontend.layouts.master')

@section('content')
    <!--======##########=========page heading=========##########======-->
    <section id="page_heading">
        <div class="container-fluid">
            <div class="row">
                <div class="page_heading_back">
                    <img src="{!! asset($historicalplace->image) !!}" alt="">
                </div>
                <div class="page_heading" style="border: 0;">
                    <h1 style="color:#fff;text-shadow: 2px 2px 2px #222;">{!! $historicalplace->name !!}</h1>
                </div>
            </div>
        </div>
    </section>
    <!--======##########=========page heading=========##########======-->

    <!--======##########=========page content=========##########======-->
    <section id="school_details">
        <div class="container pdnt-30">
            <div class="">
                <div class="history_details">
                    <div class="history">
                        <div class="site_panell">
                            <div class="panel_img">
                                <img src="{!! asset($historicalplace->image) !!}" alt="">
                            </div>
                            <ul class="panel_short_his">
                                <li>Built: <span>{!! $historicalplace->build_time !!}</span></li>
                                <li>ঠিকানা: <span>{!! $historicalplace->location !!}</span></li>
                                <li>ঘণ্টা: <span>{!! $historicalplace->opening_hour !!}</span></li>
                                <li>কার্যকারিতা: <span>{!! $historicalplace->type !!}</span></li>
                                <li>স্থাপত্য শৈলী: <span>{!! $historicalplace->architectural_style !!}</span></li>
                                <li>ফোন: <span>{!! $historicalplace->phone !!}</span></li>
                            </ul>
                            <div class="place_location">
                                <h2 class="entry_fees">Map Location</h2>
                                <hr>
                                <div style="width: 280px">
                                    {!! $historicalplace->map_url !!}
                                </div>

                            </div>
                            {{--<h2 class="entry_fees">Entry fees</h2>--}}
                            {{--<hr>--}}
                            {{--<ul class="entry_fees_dtl">--}}
                                {{--<li><strong>Local Tourists:</strong> 20 TK</li>--}}
                                {{--<li><strong>SAARC Country Tourists:</strong> 100 TK</li>--}}
                                {{--<li><strong>Other Foreigners:</strong> 200 TK</li>--}}
                            {{--</ul>--}}
                        </div>
                        <h2 class="title">ইতিহাস</h2>
                        <p class="page_text">{!! $historicalplace->history  !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--======##########=========page content=========##########======-->
@endsection

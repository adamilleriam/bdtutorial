@extends('Frontend.layouts.master')

@section('content')
    <!--======##########=========page heading=========##########======-->
    {{--<section id="page_heading">--}}
        {{--<div class="container-fluid">--}}
            {{--<div class="row">--}}
                {{--<div class="page_heading_back">--}}
                    {{--<img src="{!! asset('Frontend/images/school_baner.jpg') !!}" alt="">--}}
                {{--</div>--}}
                {{--<div class="page_heading" style="background:#f9edc7;border: 0;">--}}
                    {{--<h1 style="color:#fff;text-shadow: 2px 2px 2px #222;">{!! $institution->name !!}</h1>--}}
                    {{--<p class="established">Established in {!! $institution->established !!}</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}
    <!--======##########=========page heading=========##########======-->

    <!--======##########=========page content=========##########======-->
    <section id="school_details">
        <div class="container pdnt-30">
            <div class="row">
                <div class="col-md-8">

                    <div>
                        <h3>{!! $news->title !!}</h3>
                        <hr>
                        {!! $news->details !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="school_panel">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title title">Institution News</h3>
                            </div>
                            <ul class="list-group">
                                @forelse($institution_news as $news)
                                <a href="{!! route('institution.news.details',[$news->institution_id,$news->slug]) !!}" class="list-group-item">{!! $news->title !!}</a>
                                @empty
                                    <p>News not available</p>
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--======##########=========page content=========##########======-->
@endsection

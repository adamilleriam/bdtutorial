@extends('Frontend.layouts.master')

@section('content')
<div class="loginpage">
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="login-form">
                @include('layouts._message')
                <form action="{!! route('add.user') !!}" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="inputName">Name</label>
                        <input type="text" class="form-control" name="name" id="inputName" placeholder="Enter name">
                    </div>
                    <div class="form-group">
                        <label for="inputEmail">Email</label>
                        <input type="email" class="form-control" name="email" id="inputEmail" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label for="inputPas">Password</label>
                        <input type="password" class="form-control" name="password" id="inputPas" placeholder="Enter password">
                    </div>
                    <div class="form-group">
                        <label for="inputcPas">Confirm Password</label>
                        <input type="password" class="form-control" name="password_confirmation" id="inputcPas" placeholder="Enter password again">
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="inputFile">Choose Profile Image</label>
                            <input type="file" name="image" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])" class="form-control"  id="inputFile">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <img id="blah"  width="120" height="120"  src="http://via.placeholder.com/120x120">
                    </div>
                    <div class="clearfix"></div>
                    <input type="submit" name="submit" class="btn btn-primary" value="Sign Up">
                    <span>You are Registred? <a href="{!! route('login') !!}">Login here</a></span>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@extends('Frontend.layouts.master')
@section('barchartjs')
    <script src="{!! asset('Frontend/js/hBarChart.js') !!}"></script>
    <script>
        $(function() {
            $("ul.chart").hBarChart({
                sorting: false
            });
        })
    </script>
@endsection
@section('content')
    <!--======##########=========page heading=========##########======-->
    <section id="page_heading">
        <div class="container-fluid">
            <div class="row">
                <div class="page_heading_back">
                    <img src="{!! asset('Frontend/images/page_heading.jpg') !!}" alt="">
                </div>
                <div class="page_heading">
                    <h1>TEACHERS DETAILS </h1>
                </div>
            </div>
        </div>
    </section>
    <!--======##########=========page heading=========##########======-->

    <!--======##########=========Bio=========##########======-->
    <section id="bio" style="background:#F9F9F9; ">
        <div class="container">

            <div class="row pdnt-50">
                <div class="col-md-4 text-center">
                    <div class="bio_img">
                        <img src="{!! asset($tutorsinfo->image) !!}" alt="">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class=" bio_degi">
                       <h2 class="">{!! $tutorsinfo->name !!}</h2>
                        <p class="">{!! $tutorsinfo->title !!}</p>
                        <div class="bio_social">
                            @if($tutorsinfo->facebook_profile !=null)
                                <span><a target="_blank" href="https://www.facebook.com/{!! $tutorsinfo->facebook_profile !!}"><i class="fa fa-facebook" aria-hidden="true"></i></a></span>
                            @endif
                            @if($tutorsinfo->linkedin_profile !=null)
                                <span><a target="_blank" href="https://bd.linkedin.com/{!! $tutorsinfo->linkedin_profile !!}"><i class="fa fa-linkedin" aria-hidden="true"></i></a></span>
                            @endif
                            @if($tutorsinfo->twitter_profile !=null)
                                <span><a target="_blank" href="https://twitter.com/{!! $tutorsinfo->twitter_profile !!}"><i class="fa fa-twitter" aria-hidden="true"></i></a></span>
                            @endif
                            @if($tutorsinfo->skype_profile !=null)
                                <span><a target="_blank" href="https://www.skype.com/en/{!! $tutorsinfo->skype_profile !!}"><i class="fa fa-skype" aria-hidden="true"></i></a></span>
                            @endif
                        </div>
                    </div>
                    <div>
                        <p class="page_text">
                            {!! $tutorsinfo->biography !!}
                        </p>
                    </div>
                    <div class="bio_link">
                        @if($tutorsinfo->url !=null)
                        <span><a href="{!! $tutorsinfo->url !!}" target="_blank"><i class="fa fa-globe" aria-hidden="true"></i>   {!! $tutorsinfo->url !!}</a></span><br>
                        @endif
                       @if($tutorsinfo->email !=null)
                        <span class="pdnt-10"><a href="mailto:tauhid@piit.us" target="_blank"><i class="fa fa-envelope" aria-hidden="true"></i>   {!! $tutorsinfo->email !!}</a></span><br>
                        @endif
                        @if($tutorsinfo->mobile !=null)
                        <span class="pdnt-10"><a href="#" target="_blank"><i class="fa fa-phone" aria-hidden="true"></i>   {!! $tutorsinfo->mobile !!}</a></span>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!--======##########=========Bio=========##########======-->

    <!--======##########=========skill=========##########======-->
    <section id="skill">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="titlle text-center pdnb-20">Skills</h2>
                    <div class="skill_bar">
                        @foreach($tutorskills as $tutorskill)
                        <span class="bar_inner">{!! $tutorskill->title !!}</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{!! $tutorskill->skilllimit !!}" aria-valuemin="0" aria-valuemax="100" style="width:{!! $tutorskill->skilllimit !!}%">
                                    {!! $tutorskill->skilllimit !!}%
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-6">
                    <h2 class="titlle text-center pdnb-20">Educations</h2>
                    <div class="education">
                        @foreach($tutoreducations as $tutoreducation)
                            <span class="edu_name">{!! $tutoreducation->title !!}</span>
                            <p class="page_text">{!! $tutoreducation->description !!}</p>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--======##########=========skill=========##########======-->

    <!--======##########=========rating=========##########======-->
    <section id="rating" style="background:#F9F9F9;">
        <div class="container">
            <div class="row pdnb-50 pdnt-50">
                <div class="col-md-6">

                    <p class="text-center page_text">Rate your experience with <span style="font-family: popi_bold;">{!! $tutorsinfo->name !!}</span></p>
                   @if(isset($rating) && $rating!=null)
                        <form role="form" id="contact-form"  class="contact-form">
                            <input type="hidden" id="_token" value="{!! csrf_token() !!}">
                            <input type="hidden" id="tutor_id" value="{!! $tutorsinfo->id !!}">
                            <div class="rate_ur_ex">
                                <div class="start-rating text-center">
                                    <span class="fa <?php if ($rating->rating_value >= 1){echo 'fa-star';} ?>"   data-rating = "1" data-toggle="tooltip" data-placement="top" title="Poor"  onclick="getRating(1)" ></span>
                                    <span class="fa <?php if ($rating->rating_value >= 2){echo 'fa-star';}else{ echo 'fa-star-o'; } ?>"   data-rating = "2" data-toggle="tooltip" data-placement="top" title="Fair"  onclick="getRating(2)"></span>
                                    <span class="fa <?php if ($rating->rating_value >= 3){echo 'fa-star';}else{ echo 'fa-star-o'; } ?>"   data-rating = "3" data-toggle="tooltip" data-placement="top" title="Good"  onclick="getRating(3)"></span>
                                    <span class="fa <?php if ($rating->rating_value >= 4){echo 'fa-star';}else{ echo 'fa-star-o'; } ?>"   data-rating = "4" data-toggle="tooltip" data-placement="top" title="Very Good"  onclick="getRating(4)"></span>
                                    <span class="fa <?php if ($rating->rating_value >= 5){echo 'fa-star';}else{ echo 'fa-star-o';} ?>"   data-rating = "5" data-toggle="tooltip" data-placement="top" title="Excellent"  onclick="getRating(5)"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control textarea" rows="3"  id="Message" placeholder="Review">{!! $rating->review !!} </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    @if(Auth::check())
                                        <input type="hidden" id="user_id" value="{{ Auth::user()->id }}">
                                        <button  id="submit" class="btn main-btn">Update review</button>
                                    @else
                                        <h3 class="text-danger">Please login first for review! </h3>
                                    @endif
                                </div>
                            </div>
                        </form>
                @else
                    <form role="form" id="contact-form"  class="contact-form">
                        <input type="hidden" id="_token" value="{!! csrf_token() !!}">
                        <input type="hidden" id="tutor_id" value="{!! $tutorsinfo->id !!}">
                        <div class="rate_ur_ex">
                            <div class="start-rating text-center">
                                <span class="fa fa-star"     data-rating = "1" data-toggle="tooltip" data-placement="top" title="Poor"  onclick="getRating(1)" ></span>
                                <span class="fa fa-star-o"   data-rating = "2" data-toggle="tooltip" data-placement="top" title="Fair"  onclick="getRating(2)"></span>
                                <span class="fa fa-star-o"   data-rating = "3" data-toggle="tooltip" data-placement="top" title="Good"  onclick="getRating(3)"></span>
                                <span class="fa fa-star-o"   data-rating = "4" data-toggle="tooltip" data-placement="top" title="Very Good"  onclick="getRating(4)"></span>
                                <span class="fa fa-star-o"   data-rating = "5" data-toggle="tooltip" data-placement="top" title="Excellent"  onclick="getRating(5)"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea class="form-control textarea" rows="3"  id="Message" placeholder="Review"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                @if(Auth::check())
                                    <input type="hidden" id="user_id" value="{{ Auth::user()->id }}">
                                <button  id="submit" class="btn main-btn">Submit</button>
                                @else
                                    <h3 class="text-danger">Please login first for review! </h3>
                                @endif
                            </div>
                        </div>
                    </form>
   @endif
                    <input type="hidden" id="rating_value" value="@if(isset($rating->rating_value)){{$rating->rating_value}} @else 1 @endif">

                <h3 id="reviewsuccess" class="text-success"></h3>
                  <script>
                  //   $("#reviewsuccess").fadeOut(50000);
                  </script>
                    <script>
                        $(function () {
                            $('[data-toggle="tooltip"]').tooltip()
                        });

                        function getRating(rating) {
                            var rating_val = rating;
                            $('#rating_value').attr('value', rating_val);
                            $(".fa").each(function () {
                                var value = $(this).data('rating');
                                if (value <= rating) {
                                    $(this).removeClass('fa-star-o').addClass('fa-star');
                                }
                                else {
                                    $(this).removeClass('fa-star').addClass('fa-star-o');
                                }
                            });

                        }
                            $('#submit').click(function (e) {
                                e.preventDefault();
                                var tutor_id = $('#tutor_id').val();
                                var token = $('#_token').val();
                                var user_id = $('#user_id').val();
                                var review = $('#Message').val();
                                var rating = $('#rating_value').val();

                                  $.ajax({
                                                type:'POST',
                                                cache: false,
                                                url:'{{ route('addRating') }}',
                                    data:{
                                        _token:token,
                                        rat_val : rating,
                                        user_id: user_id,
                                        tutor_id:tutor_id,
                                        review  :review
                                    },
                                    datatype:'html',
                                    success:function (response) {

                                        $('#reviewsuccess').html(response);
                                        window.setTimeout(function(){location.reload()},2000); //reload with time
                                     //   location.reload(); //reload without time
                                    },
                                    error:function () {
                                    }
                                });
                               // $('#Message').val('');

                            });


                    </script>
                </div>
                <div class="col-md-6">
                    <div class="inner">
                        <div class="rating">
                            <span class="rating-num">
                                @if($tutortotalrating>0)
                                <?php
                                $total= (5*$fivestar+4*$fourstar+3*$threestar+2*$twostar+1*$onestar)/$tutortotalrating;
                                echo number_format($total,1);
                                ?>
                                @endif
                             </span>
                            <div class="rating-stars">
                                <span><i class="active icon-star"></i></span>
                                <span><i class="active icon-star"></i></span>
                                <span><i class="active icon-star"></i></span>
                                <span><i class="active icon-star"></i></span>
                                <span><i class="icon-star"></i></span>
                            </div>
                            <div class="rating-users">
                                <i class="icon-user"></i>
                                {{ $tutortotalrating  }} total
                            </div>
                        </div>
                        <div class="histo">
                            <ul class="chart">
                                <p id="star-val">5</p><li data-data="{!! $fivestar !!}"> {!! $fivestar !!}</li>
                                <p id="star-val">4</p><li data-data="{!! $fourstar !!}" class=""> {!! $fourstar !!} </li>
                                <p id="star-val">3</p><li data-data="{!! $threestar !!}" class="">{!!$threestar  !!}</li>
                                <p id="star-val">2</p><li data-data="{!! $twostar !!}" class="">{!! $twostar !!}</li>
                                <p id="star-val">1</p><li data-data="{!! $onestar !!}" class="">{!! $onestar !!}</li>
                            </ul>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    <!--======##########=========rating=========##########======-->

    <!--======##########=========review=========##########======-->
    <section id="review">
        <div class="container">
            <div class="row">
                <div class="review_slide" data-slick='{"slidesToShow": 2, "slidesToScroll": 1}'>
                    @forelse($tutorreviews as $tutorreview)
                        <div class="col-md-6">
                            <div class="Review">
                                <div class="Review-details">
                                    <img src="{!! asset($tutorreview->image) !!}">
                                    <div class="Review-meta">
                                        <p class="Review-author">{!! $tutorreview->name !!}</p>

                                        <p class="Review-date">{!! $tutorreview->created_at->diffForHumans() !!}</p>
                                        <div class="Review-rating">
                                        <?php  $i=1;  for($i;$i<=5;$i++){?>
                                            <span class="Review-star @if($i<=$tutorreview->rating_value) Review-star-active @endif" >&#9733;</span>
                                        <?php    }   ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="Review-body">
                                    <p>{!! $tutorreview->review !!}</p>
                                </div>
                            </div>
                        </div>
                    @empty
                    <p>No reviews found!</p>
                    @endforelse
                </div>
            </div>
        </div>
    </section>
    <!--======##########=========review=========##########======-->
@endsection
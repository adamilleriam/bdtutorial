@extends('layouts.master')
@section ('content')

<div class="row">
    <div class="col-xs-12">
        <!-- /.box -->
        <div class="box">
            <div class="box-header">
                <div class="col-sm-12" style="padding-top: 5px;padding-bottom: 5px;">
                    <a href="{!! route('quick.learn.add') !!}" class="btn btn-success pull-right addNew">Add New</a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="example1"  class="table table-bordered table-striped">
                    @include('layouts._message')
                    <thead>
                    <tr>
                        <th>SL.</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th class="text-center" style="width: 10%;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1;?>
                        @forelse($quicklearns as $quicklearn)
                            <tr>
                                <td>{!! $i++ !!}</td>
                                <td>{!! $quicklearn->title !!}</td>
                                <td><img width="80" src="{!! asset($quicklearn->feature_image) !!}" alt=""></td>
                                <td>{!! $quicklearn->status !!}</td>
                                <td>
                                    <a class="btn btn-info btn-sm" href="{!! route('quick.learn.edit',$quicklearn->id) !!}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-danger btn-sm" onclick="return confirm('Are you sure to delete?')" href="{!! route('quick.learn.delete',$quicklearn->id) !!}"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @empty
                        <p>No record found!</p>
                      @endforelse
                    </tbody>
                </table>

            </div>
        </div>
        <div class="pull-right">

        </div>

        <!-- /.box -->
    </div>
</div>

@endsection

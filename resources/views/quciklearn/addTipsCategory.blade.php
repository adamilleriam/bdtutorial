@extends('layouts.master')
@section('select2css')
    <link rel="stylesheet" href="{!! asset('dist/css/select2.min.css') !!}">
@endsection
@section('content')
   <div class="box box-default">
        <div class="box-header with-border">
            <!-- /.box-header -->

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <form action="{{ route('tips.category.store') }}" method="post">
            {{ csrf_field() }}
            @include('layouts._message')
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">Tips Category</label>
                            <input type="text" name="category" id="inputName" class="form-control" placeholder="Enter tips Category">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Status</label><br>
                            <label class="radio-inline"><input type="radio" name="status" value="active"> Active</label>
                            <label class="radio-inline"><input type="radio" name="status" value="inactive"> Inactive</label>
                            <label class="radio-inline"><input type="radio" name="status" value="suspended"> Suspended</label>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <input type="submit" name="Save"  value="Submit" class="btn btn-success pull-right">
                    </div>
                    <div class="col-xs-6">
                        <a href="{!! route('tips.category.list') !!}" class="btn btn-warning" onclick="return confirm('Are you confirm to cancel !')">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

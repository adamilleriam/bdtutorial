@extends('layouts.master')
@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <!-- /.box-header -->
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            @include('layouts._message')
            <div class="col-md-5">
                <div class="image" >
                @if(Auth::user()->image!=null)
                    <img src="{{asset(Auth::user()->image)}}" class="img-rounded " alt="User Image" style="width:100%;">
                @endif
                @if(Auth::user()->image==null)
                    <img src="{{asset('asset/img/user_logo.jpg')}}" class="img-rounded " alt="User Image" style="width:100%;">
                @endif
                </div>
                    <div class="name" style="margin-top:10px;">

                        <p> <b>Name:</b> {!! $user->name !!}</p>


                    </div>
                    <div class="email">

                        <p><b>Email:</b> {!! $user->email !!}</p>

                    </div>

            </div>
            <div class="col-md-7">
        {!! Form::model($user,['route'=>['update.profile',$user->id],'method'=>'put','files'=> true]) !!}

                @include('user.changeProfile._form')

            <div class="row">
                <div class="col-xs-6">
                    {!! Form::submit('Update',['class'=>'btn btn-success pull-right']) !!}

                </div>
                <div class="col-xs-6">
                    <a href="{!! route('dashboard') !!}" class="btn btn-danger"
                       onclick="return confirm('Are you confirm to cancel !')">Cancel</a>
                </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>

    </div>
    <!-- /.box -->
@endsection
@section('customJs')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview').fadeIn(1000);
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgupload").change(function(){
            readURL(this);
        });
        $(function () {
            $('.yes').on('change', function () {

                $('#change').append('<div id="removepass"><div class="form-group"><label>Password</label> <input type="password" name="password" required class="form-control"></div> ' +
                    '<div class="form-group"><label>Confirm Password</label> <input type="password" name="password_confirmation" required  class="form-control"></div></div>')

            });
            $('.no').on('change', function () {

                $('#removepass').remove();
            });
        });

    </script>
    <!-- /.box -->
@endsection
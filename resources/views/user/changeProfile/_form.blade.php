

    <h4 class="text-center">Update Information</h4>
    <div class="form-group">
        {!! Form::label('Name') !!}
        {!! Form::text('name',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Email') !!}
        {!! Form::text('email',null,['class'=>'form-control']) !!}
    </div>
    @if(!isset($user))
        <div class="form-group">
            {!! Form::label('Password') !!}
            {!! Form::password('password',['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Confirm Password') !!}
            {!! Form::password('password_confirmation',['class'=>'form-control']) !!}
        </div>
    @endif
    @if(isset($user))
        <div class="form-group"><label>Password Change</label>:
            <input type="radio" name="ok1" class="minimal yes">Yes
            <input type="radio" name="ok1" class="minimal no">No
        </div>
        <div id="change">

        </div>
    @endif

    <div class="form-group">
        <img style="padding:10px;height: 100px;width:100px; display: none" id="preview" src="" alt="">
        <div class="upload-box">
            <label class="btn" style="background: gainsboro;width: 50%;">
                <input name="image" id="imgupload" style="display:none" type="file">
                <i class="fa fa-cloud-upload"></i> Upload Image
                <i class="fa fa-cog fa-spin  fa-fw margin-bottom" id="loaderIcon"
                   style="color:green;margin-top:10px;display: none"></i>
            </label>
        </div>
    </div>

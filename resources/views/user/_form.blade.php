<div class="col-md-6">
    <div class="form-group">
        {!! Form::label('Name') !!}
        {!! Form::text('name',null,['class'=>'form-control']) !!}
    </div>

    @if(!isset($user))
        <div class="form-group">
            {!! Form::label('Email') !!}
            {!! Form::text('email',null,['class'=>'form-control']) !!}
        </div>
    @else
        <div class="form-group">
            {!! Form::label('Email') !!}
            {!! Form::text('email',null,['class'=>'form-control','readonly']) !!}
        </div>
    @endif
    @if(isset($user))
        <div class="form-group"><label>Password Change</label>:
            <input type="radio" name="ok1" class="minimal yes">Yes
            <input type="radio" name="ok1" class="minimal no">No
        </div>
        <div id="change">

        </div>
    @endif
</div>
<div class="col-md-6">
    <div class="form-group">
        {!! Form::label('Status') !!}
        :
        {!! Form::radio('status','Active',null,['class'=>'minimal','checked']) !!} Active
        {!! Form::radio('status','Inactive',null,['class'=>'minimal']) !!} Inactive
        {!! Form::radio('status','Suspended',null,['class'=>'minimal']) !!} Suspended
    </div>
    <div class="form-group">
        @if(isset($user))
            @if($user->image!=null)
                <img style="padding:10px;height: 100px;width:100px;" id="preview" src="{{asset($user->image)}}">
            @else
                <img style="padding:10px;height: 100px;width:100px; display: none" id="preview" src="" alt="">
            @endif
        @else
            <img style="padding:10px;height: 100px;width:100px; display: none" id="preview" src="" alt="">
        @endif
        <div class="upload-box">
            <label class="btn" style="background: gainsboro;width: 50%;">
                <input name="image" id="imgupload" style="display:none" type="file">
                <i class="fa fa-cloud-upload"></i> Upload Image
                <i class="fa fa-cog fa-spin  fa-fw margin-bottom" id="loaderIcon"
                   style="color:green;margin-top:10px;display: none"></i>
            </label>
        </div>
    </div>
</div>

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    //

    protected $fillable = ['division_id', 'district_id','subdist_id', 'title', 'author', 'image','status','description'];
}

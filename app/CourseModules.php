<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
class CourseModules extends Model
{
    use SoftDeletes;

    protected $table='course_modules';
    protected $fillable = [
        'name','slug','sequence','course_id','overview','file','url','status'
    ];
    protected $dates = ['deleted_at'];
    public function relCourse()
    {
        return $this->belongsTo('App\Course', 'course_id', 'id');
    }
    public static function boot()
    {
        parent::boot();
        static::creating(function ($query) {
            if (Auth::check()) {
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function ($query) {
            if (Auth::check()) {
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}

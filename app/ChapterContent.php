<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
class ChapterContent extends Model
{
    use SoftDeletes;

    protected $table='chapter_contents';
    protected $fillable = [
        'name','sequence','class_id','subject_id','chapter_id','url','status'
    ];
    protected $dates = ['deleted_at'];
    public function relClass()
    {
        return $this->belongsTo('App\Chapter', 'class_id', 'id');
    }
    public static function boot()
    {
        parent::boot();
        static::creating(function ($query) {
            if (Auth::check()) {
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function ($query) {
            if (Auth::check()) {
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipscategory extends Model
{
    protected  $fillable = ['id','category','status'];
}

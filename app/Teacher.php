<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    //
    protected $fillable =['name','title','email','mobile','url','biography','image','slug','facebook_profile','twitter_profile','linkedin_profile','skype_profile','status'];

    public function relCourse()
    {
        return $this->hasMany('App\Course', 'trainer_id', 'id');
    }


}

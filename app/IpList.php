<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IpList extends Model
{
    protected $table = 'ip_list';
    protected $fillable = [
        'ip',
        'attempt',
        'last_attempt',
        'total_short_time_attempt',
    ];
}

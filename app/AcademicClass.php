<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
class AcademicClass extends Model
{
    use SoftDeletes;

    protected $table='academic_classes';
    protected $fillable = [
        'name', 'slug', 'sequence', 'type', 'status'
    ];
    protected $dates = ['deleted_at'];


    // Relation Content Table
    public function relSubject()
    {
        return $this->hasMany('App\ClassSubject', 'class_id', 'slug');
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($query) {
            if (Auth::check()) {
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function ($query) {
            if (Auth::check()) {
                $query->updated_by = Auth::user()->id;
            }
        });
    }

}

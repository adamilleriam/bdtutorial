<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
class ClassSubject extends Model
{
    use SoftDeletes;

    protected $table='class_subjects';
    protected $fillable = [
        'name', 'slug', 'url','class_id','status'
    ];
    protected $dates = ['deleted_at'];

    // Relation Announcement Table
    public function relClass()
    {
        return $this->belongsTo('App\AcademicClass', 'class_id', 'slug');
    }
    public static function boot()
    {
        parent::boot();
        static::creating(function ($query) {
            if (Auth::check()) {
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function ($query) {
            if (Auth::check()) {
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}

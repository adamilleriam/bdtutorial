<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quciklearn extends Model
{
    protected $fillable = ['id','title','slug','tag','url','feature_image','description','status'];
}

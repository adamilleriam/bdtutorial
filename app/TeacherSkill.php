<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherSkill extends Model
{
    protected $fillable = ['title', 'skilllimit'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
class Course extends Model
{
    use SoftDeletes;

    protected $table='courses';
    protected $fillable = [
        'name', 'slug','topic_slug','trainer_id','overview','what_will_i_learn','requirements','url','featured','publish', 'sequence','file','status'
    ];
    protected $dates = ['deleted_at'];
    // Relation Course Modules Table
    public function relModules()
    {
        return $this->hasMany('App\CourseModules', 'course_id', 'id');
    }

    public function relTeacher()
    {
        return $this->belongsTo('App\Teacher', 'trainer_id', 'id');
    }
    public static function boot()
    {
        parent::boot();
        static::creating(function ($query) {
            if (Auth::check()) {
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function ($query) {
            if (Auth::check()) {
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}

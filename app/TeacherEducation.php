<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherEducation extends Model
{
    protected $fillable = ['title', 'description'];
}

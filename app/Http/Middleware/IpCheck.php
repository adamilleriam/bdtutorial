<?php

namespace App\Http\Middleware;

use App\IpList;
use Closure;

class IpCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ipList=IpList::where('ip',$request->ip())->first();
        if(($ipList==null) || ($ipList != null && $ipList->status=='Active'))
        {
            return $next($request);
        }
        return response()->view('message_ip_block',['request'=>$request]);
    }
}

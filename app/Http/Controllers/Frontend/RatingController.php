<?php

namespace App\Http\Controllers\Frontend;
use App\Rating;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RatingController extends Controller
{
    //

    public function store(Request $request)
    {
        $rating = Rating::where('tutor_id',$request->tutor_id)->where('user_id',$request->user_id)->first();
        if($rating==null){
            $rating=New Rating();
            $rating->tutor_id = $request->tutor_id;
            $rating->user_id = $request->user_id;
        }
        $rating->rating_value  = $request->rat_val;
        $rating->review        = $request->review;
        $rating->save();
        return response()->json('Thanks for your Review! ');

    }
}

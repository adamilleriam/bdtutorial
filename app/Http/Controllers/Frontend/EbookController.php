<?php

namespace App\Http\Controllers\Frontend;
use App\Ebook;
use App\EbookCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EbookController extends Controller
{
    public function index()
    {
       $data['categories'] = EbookCategory::paginate(12);
        return view('Frontend.ebook.index',$data);
    }
    public function bookCategory($id)
    {
        $data['books'] = Ebook::where('category_id',$id)->paginate(12);
        return view('Frontend.ebook.category',$data);
    }
    public function bookinof($slug)
    {
        $data['book'] = Ebook::where('slug',$slug)->first();
        return view('Frontend.ebook.bookinfo',$data);
    }
}

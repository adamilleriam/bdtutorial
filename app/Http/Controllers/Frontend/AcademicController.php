<?php

namespace App\Http\Controllers\Frontend;

use App\AcademicClass;
use App\Chapter;
use App\ClassSubject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AcademicController extends Controller
{
    public function index(Request $request){
        $data['title']="Academic Training";
        $Class = New AcademicClass();
        if ($request->search == 'primary') {
            $Class = $Class->where('type', 'primary');
        }elseif ($request->search == 'high-school') {
            $Class = $Class->where('type', 'high');
        }elseif ($request->search == 'collage') {
            $Class = $Class->where('type', 'collage');
        }elseif ($request->search == 'university') {
            $Class = $Class->where('type', 'university');
        }else{
            $Class = $Class->where('type', 'primary');
            $data['active']='active';
        }
        $Class->with(['relSubject'=>function($query){
            $query->where('status','active');
            $query->select('name','slug','class_id');
        }]);
        $Class = $Class->orderBy('id', 'DESC')->get();

        $data['AcademicClass'] = $Class;

        return view('Frontend.academic.index',$data);
    }

    public function chapter($class,$subject){

        $data['title']="Chapter List";
        $data['subjects']=ClassSubject::where('class_id',$class)->where('status','active')->get();
        $data['class_subject']=ClassSubject::where('slug',$subject)->select('url')->first();
        $chapter=New Chapter();
        $chapter=$chapter->where('class_id',$class)->where('subject_id',$subject);
        $chapter=$chapter->where('status','active');
               $chapter=  $chapter->with(['relContent'=>function($query){
                   $query->where('status','active');
                   $query->orderBy('sequence', 'ASC');
               }]);
        $chapter=$chapter->orderBy('sequence', 'ASC')->get();
        $data['chapters']=$chapter;
        return view('Frontend.academic.chapter',$data);
    }
}

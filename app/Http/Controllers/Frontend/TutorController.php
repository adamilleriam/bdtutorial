<?php

namespace App\Http\Controllers\Frontend;
use App\Rating;
use DB;
use App\Teacher;
use App\TeacherSkill;
use App\TeacherEducation;
//use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
class TutorController extends Controller
{
    //
    public function index()
    {
       $data['tutors'] = Teacher::get();
        return view('Frontend.tutors',$data);
    }

    public function show($id,$slug)
    {
       $data['tutorsinfo']       = Teacher::where('slug',$slug)->first();
       $data['tutorskills']      = TeacherSkill::where('teacher_id',$id)->get();
       $data['tutoreducations']  = TeacherEducation::where('teacher_id',$id)->get();
       $data['tutortotalrating'] = Rating::where('tutor_id',$id)->sum('rating_value');
       $data['fivestar']  = Rating::where('tutor_id',$id)->where('rating_value',5)->count();
       $data['fourstar']  = Rating::where('tutor_id',$id)->where('rating_value',4)->count();
       $data['threestar'] = Rating::where('tutor_id',$id)->where('rating_value',3)->count();
       $data['twostar']   = Rating::where('tutor_id',$id)->where('rating_value',2)->count();
       $data['onestar']   = Rating::where('tutor_id',$id)->where('rating_value',1)->count();
       $rating= New Rating();
       $rating= $rating->join('users', 'ratings.user_id', '=', 'users.id');
       $rating=  $rating->select('ratings.*','users.name','users.image');
       $rating=  $rating->where('tutor_id',$id);
       $rating=  $rating->get();
        $data['tutorreviews']=$rating;
      // dd($data);
      if( Auth::check()) {
          $data['rating'] = Rating::where('tutor_id','=',$id)
              ->where('user_id',Auth::user()->id)->first();
      }

        return view('Frontend.tutor_details',$data);
    }



}

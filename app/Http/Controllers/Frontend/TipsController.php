<?php

namespace App\Http\Controllers\Frontend;
use App\Quciklearn;
use App\Tipscategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TipsController extends Controller
{
    public function index($slug)
    {
        $quciklearn= Quciklearn::where('slug',$slug)->first();
        $data['quicklearn']=$quciklearn;
        $data['relatedpost'] = Quciklearn::where('cat_id',$quciklearn->cat_id)->where('status','active')->whereNotIn('id',[$quciklearn->id])->orderBy('id', 'DESC')->paginate(10);
        return view('Frontend.quick_learn_details',$data);
    }


}

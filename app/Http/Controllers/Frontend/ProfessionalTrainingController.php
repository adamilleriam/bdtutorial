<?php

namespace App\Http\Controllers\Frontend;

use App\Course;
use App\CourseModules;
use App\ProfessionalTraining;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfessionalTrainingController extends Controller
{
    public function index($category=false){

    $data['title']="Professional Training";
    $data['category']=ProfessionalTraining::where('status','active')->orderBy('sequence','ASC')->get();
    $courses  = Course::where('status','active')->where('publish',1);
    if(isset($category)&&$category!=null){
        $courses  = $courses->where('topic_slug',$category);
    }
    $courses  = $courses->orderBy('sequence','asc')->paginate(15);

        $data['courses'] =$courses;
        return view('Frontend.professional_training.index',$data);
    }
    public function details($slug){
        $course  = Course::where('slug',$slug)->first();
        $data['title']=$course->name;
        $data['course']=$course;
        $data['relatedCourses']   = Course::where('status','active')->where('publish',1)->where('featured',1)->whereNotIn('slug',[$slug])->orderBy('sequence','ASC')->limit(10)->get();
        return view('Frontend.professional_training.details',$data);
    }
    public function moduleDetails($slug,$module){
        $course  = Course::where('slug',$slug)->first();
        $data['title']=$course->name;
        $data['course']=$course;
        $data['module']=CourseModules::where('slug',$module)->where('course_id',$course->id)->first();
        $data['relatedCourses']   = Course::where('status','active')->where('publish',1)->where('featured',1)->whereNotIn('slug',[$slug])->orderBy('sequence','ASC')->limit(10)->get();
        return view('Frontend.professional_training.module',$data);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Mail\SendUserPassword;
use App\User;
use Auth;
use App\Mail\UserEmailValidation;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Cache;
class UserController extends Controller
{
    public function index(Request $request)
    {

        $users = New User();
        $data['title'] = 'Manage User';
        if ($request->search == 'Trashed') {
            $users = $users->onlyTrashed();

        } elseif ($request->search == 'Inactive') {
            $users = $users->where('status', 'Inactive');
        }elseif ($request->search == 'Suspended') {
            $users = $users->where('status', 'Inactive');
        }elseif ($request->search == 'Active') {
            $users = $users->where('status', 'Active');
        }

        $users = $users->orderBy('id', 'DESC')->paginate(Cache::get('per_page'));
        if (isset($request->search)) {
            $render['search'] = $request->search;
            $users = $users->appends($render);
        }
        $data['user'] = $users;
        $data['serial'] = managePagination($users);
        return view('user.index', $data);
    }


    public function create(){


        $data['title']='Add User';
        return view('user.create',$data);
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users|max:255',
            'status' => 'required',
        ]);

        $user = New User();
        $user->name = $request->name;
        $user->email = $request->email;
        $data['password']=rand(0000000, 9999999);
        $data['name']=$request->name;
        $user->password = bcrypt($data['password']);
        $user->status = $request->status;
        $image = $request->file('image');
        if ($image) {
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/user/';
            $image_url = $upload_path . $image_full_name;
            $image->move($upload_path, $image_full_name);
            if ($ext=='jpg' || $ext=='png'|| $ext=='jpeg'|| $ext=='JPG' || $ext=='PNG'|| $ext=='JPEG'){
                $user->image = $image_url;
            }else{

                Session::flash('warning','File is not valid!!');
                return redirect()->back();
            }
        }
        $data['loginUri']=config('app.loginUri');
        $data['email']=$request->email;
//        dd($data);
        Mail::to($request->email)->send(new SendUserPassword($data));
        $user->save();
        Session::flash('message', 'User Create SuccessFully!! Password Has been Send to your Mail Address.');
        return redirect()->back();

    }


    public function edit($id)
    {
        $data['title'] = 'Edit User';

        $data['user'] = User::withTrashed()->where('id', $id)->first();
        return view('user.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'password' => 'confirmed|min:6',
            'email' => 'required|unique:users,email,' . $id,
            'status' => 'required',

        ]);
        $user = User::withTrashed()->where('id', $id)->first();
        $user->name = $request->name;
        $user->email = $request->email;

        if ($request->password) {
            $user->password = bcrypt($request->password);

        }
        $user->status = $request->status;
        $image = $request->file('image');
        if ($image) {
            if ($user->image != null) {
                @unlink($user->image);
            }
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/user/';
            $image_url = $upload_path . $image_full_name;
            $image->move($upload_path, $image_full_name);
            if ($ext=='jpg' || $ext=='png'|| $ext=='jpeg'|| $ext=='JPG' || $ext=='PNG'|| $ext=='JPEG'){
                $user->image = $image_url;
            }else{

                Session::flash('warning','File is not valid!!');
                return redirect()->back();
            }
        }
        $user->save();
        Session::flash('message', 'User Update SuccessFully');
        return redirect()->route('user.index');
    }
    public function changeProfile(){

        $data['title'] = 'Profile';
        $data['user']=User::withTrashed()->where('id',Auth::user()->id)->first();
        return view('user.changeProfile.changeProfile',$data);
    }
    public function updateProfile(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'password' => 'confirmed',
            'email' => 'required|unique:users,email,' .Auth::user()->id,
        ]);
        $user = User::withTrashed()->where('id', Auth::user()->id)->first();
        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->password) {
            $user->password = bcrypt($request->password);

        }
        $image = $request->file('image');
        if ($image) {
            if ($user->image != null) {
                @unlink($user->image);
            }
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/user/';
            $image_url = $upload_path . $image_full_name;
            $image->move($upload_path, $image_full_name);
            if ($ext=='jpg' || $ext=='png'|| $ext=='jpeg'|| $ext=='JPG' || $ext=='PNG'|| $ext=='JPEG'){
                $user->image = $image_url;
            }else{

                Session::flash('warning','File is not valid!!');
                return redirect()->back();
            }
        }
        $user->save();
        Session::flash('message', 'User Profile Update SuccessFully');
        return redirect()->back();


    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function trash($id)
    {
        User::findorfail($id)->delete();
        Session::flash('message', 'User successfully Trashed.');
        return redirect()->back();
    }

    public function restore($id)
    {
        User::withTrashed()->where('id', $id)->first()->restore();
        Session::flash('message', 'User successfully restored.');
        return redirect()->back();


    }

    public function destroy($id)
    {
        User::withTrashed()->where('id', $id)->first()->forceDelete();
        Session::flash('message', 'User successfully Deleted.');
        return redirect()->back();


    }

///user registeration



    public function registration(){

        return view('registeration.index');

    }



    public function regStore(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users|max:255',
            'password' => 'confirmed|min:6',

        ]);
        if (isset($request->check)){
            $user = New User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->status ='Unverified';
            $user->token=$this->_codeGenerator();
            $user->save();
            $this->_sendConfirmationEmail($user);
            Session::flash('message','We sent an Ectivation Code In your Mail Please confirm email For Sucrssfully Login');
            return redirect()->back();
        }
        else{
            Session::flash('danger','You should agree with the terms.');
            return redirect()->back()->withInput();


        }

    }


    private function _sendConfirmationEmail($user){
        Mail::to($user->email)->send(new UserEmailValidation($user));
    }

    private function _codeGenerator(){
        $t=microtime();
        $encode=crypt($t,'st');
        $encode=str_replace('/',rand(1,9),$encode);
        return str_replace('.',rand(1,9),$encode);

    }

    public function verify_email($id,$token=false)
    {
        if(!empty($token)){
            $user=User::where('token',$token)->where('id',$id)->first();
            if(!empty($user)){
                if($user->status=='Unverified')
                {
                    $user->status='Active';
                    $user->token=null;
                    $user->save();
                }
                Session::flash('message','Email confirm successfully');
                return redirect()->route('login.index');
            }
        }
    }
    public function resend_email($id)
    {
        $user=User::findOrFail($id);
        $this->_sendConfirmationEmail($user);
        Session::flash('message','Please check your email for confirmation link');
        return redirect()->route('login.index');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\IpList;
use App\Mail\IpBlockedMail;
use App\Mail\ResendSuspendedAccountEmail;
use App\Mail\UserPasswordReset;
use App\Mail\UserSuspendForHackingActivities;
use App\Mail\UserSuspendMail;
use App\Mail\UserSuspnedForRoboticActivities;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Cache;
use File;
use Session;
use App\User;

class UserAuthController extends Controller
{
    public function index()
    {

        if (Auth::check()) {
            return redirect()->route('dashboard');
        }
        return view('login.login');

    }

    //login
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $user = User::where('email', $request->email)->first();

        if (isset($user) && !empty($user)) {
            if($user->status=='Suspended')
            {
                $url=route('resend_suspend_email',$user->id);
                Session::flash('warning', 'Sorry,Your account has been suspended. Please check your email.Click <a href="'.$url.'">here</a> to resend email');
                return redirect()->route('login.index');
            }else {
                $this->_checkLoginDetails($request,$user);
            }
        }elseif($user==null){
            $this->_ipAction($request);
        }
        Session::flash('warning', 'Sorry,Invalid email or password');
        return redirect()->route('login.index');

    }
    private function _checkLoginDetails($request,$user)
    {
        if (Hash::check($request->password, $user->password)) {
            if ($user->status == 'Unverified') {
                $url = route('resend', $user->id);
                Session::flash('warning', 'Sorry,Please confirm your email address. If you don\'t get your confirmation email please <a href="' . $url . '">Click here</a> to resend your email');
            } elseif ($user->status == 'Inactive') {
                Session::flash('warning', 'Sorry,Your account is inactive. Please contact with support.');
            } elseif ($user->status == 'Suspended') {
                $user->status='Active';
                $user->attempt=0;
                $user->last_attempt=null;
                $user->total_short_time_attempt=0;
                $user->save();
                $this->_login($request,$user);
            } else {
//        Auth::attempt(['email' => $request->email, 'password' => $request->password], true);
                $this->_login($request,$user);
            }
        } else {
//                dd(time()-$user->last_attempt);
//                dd($user);
            if ($user->total_short_time_attempt <= 3) {
                if (time() - ($user->last_attempt) < 5) {
                    $user->total_short_time_attempt = $user->total_short_time_attempt + 1;
                }
            } else {
                $user->last_attempt = time();
                $user->status = 'Suspended';
                $user->token= $this->_codeGenerator();
                Mail::to($user->email)->send(new UserSuspnedForRoboticActivities($user));
            }
            if ($user->attempt >= 5) {
                $user->last_attempt = time();
                $user->status = 'Suspended';
                $user->token= $this->_codeGenerator();
                Mail::to($user->email)->send(new UserSuspendForHackingActivities($user));
            }
            $user->last_attempt = time();
            $user->attempt = $user->attempt + 1;
            $user->save();
        }
    }
    private function _ipAction($request)
    {
        $lastDate=date('Y-m-d',strtotime('- 7 days'));

        if(!IpList::where('ip',$request->ip())->where('created_at','>',$lastDate)->exists())
        {
            if(IpList::where('ip',$request->ip())->exists())
            {
                $ipList=IpList::where('ip',$request->ip())->first();
            }else{
                $ipList=new IpList();
                $ipList->ip=$request->ip();
            }
            $ipList->attempt=0;
            $ipList->last_attempt=0;
            $ipList->total_short_time_attempt=0;
            $ipList->created_at=date('Y-m-d H:i:s');
            $ipList->save();
        }
        $this->_checkIp($request,IpList::where('ip',$request->ip())->first());
    }
    private function _checkIp($request,$ip)
    {
        if ($ip->total_short_time_attempt < 3) {
            if (time() - ($ip->last_attempt) < 2) {
//            dd(time() - ($ip->last_attempt));
                $ip->total_short_time_attempt = $ip->total_short_time_attempt + 1;
            }
//            exit('no');
        } else {
            $email=Setting::where(['name'=>'contactInfo','type'=>'email'])->toArray();
            $ip->status = 'Blocked';
            Mail::to($this->_getContactEmails())->send(new IpBlockedMail($ip));
        }
        if ($ip->attempt >= 5) {
            $ip->status = 'Blocked';
            Mail::to($this->_getContactEmails())->send(new IpBlockedMail($ip));
        }
        $ip->last_attempt = time();
        $ip->attempt = $ip->attempt + 1;
        $ip->save();
    }
    private function _getContactEmails()
    {
        $emails=Setting::select('content')->where(['name'=>'contactInfo','type'=>'email'])->get();
        $adminEmails=[];
        foreach ($emails as $email) {
            $adminEmails[]=$email->content;
        }
        return $adminEmails;
    }
    private function _login($request,$user)
    {
        if (isset($request->remember_me)) {
            Auth::login($user, true);
        } else {

            Auth::login($user);
        }
        $this->_listItemPerPage();
        return redirect()->route('dashboard');
    }

    //logout
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

//        $request->session()->regenerate();

        return redirect()->route('login.index');
    }

    protected function guard()
    {
        return Auth::guard();
    }

    //password Reset With Email verifiaction


    private function _codeGenerator()
    {
        $t = microtime();
        $encode = crypt($t, 'st');
        $encode = str_replace('/', rand(1, 9), $encode);
        return str_replace('.', rand(1, 9), $encode);

    }

    public function reset_user_password(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if ($user) {
            $user->token = $this->_codeGenerator();
            $user->save();
            Mail::to($user->email)->send(new UserPasswordReset($user));
            Session::flash('message', 'Please check your email for Reset your Password');
            return redirect()->back();
        } else {
            Session::flash('message', 'Your Email is InCorrecet');
            return redirect()->back();
        }
    }

    public function password_reset($id, $token)
    {
        $data['title'] = 'Change Password';
        $data['user'] = User::Where('id', $id)->where('token', $token)->first();
        return view('login.reset_password', $data);
    }

    public function update_password(Request $request, $id)
    {
        $this->validate($request, [
            'password' => 'required|min:6|confirmed',
        ]);
        $user = User::findOrFail($id);
        $user->password = bcrypt($request->password);
        $user->token = null;
        if($user->status=='Suspended')
        {
            $user->status = 'Active';
        }
        $user->save();
        Session::flash('message', 'Password Reset SuccessFully');
        return redirect()->route('login.index');
    }

    private function _listItemPerPage()
    {

        if (Cache::has('per_page')) {
            $per_page = cache('per_page');
        } else {
            $per_page = 10;
        }

    }

    // Login Suspended account
    public function suspended_account_login_panel($user_id=false,$token=false)
    {
        if(isset($user_id) && $user_id!=null && isset($token) && $token!=null) {
            $user = User::Where('id', $user_id)->where('token', $token)->first();
            if (isset($user) && count($user) > 0) {
                $data['title'] = 'Login Password';
                $data['user'] = $user;
                return view('login.login_suspended_account', $data);
            }
        }
        return redirect('/');
    }
    public function login_suspended_account(Request $request)
    {
        $suspended_time=5;
        $user=User::findOrFail($request->user_id);
        if(time()-($user->last_attempt+($suspended_time*60))>0) {
            if (isset($user) && count($user) > 0 && $user->status == 'Suspended' && Hash::check($request->password, $user->password)) {
                $user->status = 'Active';
                $user->token = null;
                $user->attempt= 0;
                $user->last_attempt= 0;
                $user->total_short_time_attempt= 0;
                $user->save();
                $this->_login($request, $user);
            }
            Session::flash('warning', 'Sorry, Password dosen\'t match. Please try again or reset your password.');
            return redirect()->back();
        }else{
            Session::flash('danger', 'Please try after 5 minute');
            return redirect()->back();
        }
    }
    public function resend_suspend_email($user_id)
    {
        $user=User::FindOrFail($user_id);
        if(isset($user) && count($user)>0) {
            Mail::to($user->email)->send(new ResendSuspendedAccountEmail($user));
            Session::flash('message', 'Successfully resend email.Please check your email.');
        }else{
            Session::flash('warning', 'Sorry something went wrong. Please try again.');
        }
        return redirect()->back();
    }
}

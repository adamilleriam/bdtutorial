<?php

namespace App\Http\Controllers\Admin;
use App\Quciklearn;
use App\Tipscategory;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuickLearnController extends Controller
{
    //
    public function index()
    {
        $data['title'] = 'Tips & Ticks List';
        $data['quicklearns'] = Quciklearn::get();
        return view('quciklearn.index',$data);
    }

    public function create()
    {
        $data['title'] = 'Add Tips';
        $data['categories'] = Tipscategory::get();
        return view('quciklearn.addtipsandticks',$data);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'category'   => 'required',
            'description' => 'required',
            'featureimage' => 'required',
            'status' => 'required',
        ]);
        $quicklearn = new Quciklearn();

        $image = $request->file('featureimage');
        if ($image) {
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/tips/';
            $image_url = $upload_path . $image_full_name;
            $image->move($upload_path, $image_full_name);
            if ($ext=='jpg' || $ext=='png'|| $ext=='jpeg'|| $ext=='JPG' || $ext=='PNG'|| $ext=='JPEG'){
                $quicklearn->feature_image = $image_url;
            }else{

                Session::flash('warning','File is not valid!!');
                return redirect()->back();
            }
        }
        $quicklearn->title  = $request->title;
        function make_slug($string) {
            $slug = preg_replace('/\s+/u', '-', trim($string));
            $slug = mb_strtolower($slug);
           return $slug;
        }
        $quicklearn->slug     = make_slug($request->title);
        $quicklearn->cat_id  = $request->category;
        $quicklearn->description  = $request->description;
        $quicklearn->url  = $request->url;
        $quicklearn->status  = $request->status;
        $quicklearn->save();
        Session::flash('message', 'New Tips&Ticks added SuccessFully!!');
        return redirect()->back();

    }

    public function edit($id)
    {
        $data['title'] = 'Edit Tips & ticks';
        $data['quicklearns'] = Quciklearn::find($id);
        $data['categories'] = Tipscategory::get();
        return view('quciklearn.edittipsandticks',$data);

    }

    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'title' => 'required',
            'category'   => 'required',
            'description' => 'required',
            'status' => 'required',
        ]);
        $quicklearn = Quciklearn::find($id);

        $image = $request->file('featureimage');
        if ($image) {
            if ($quicklearn->feature_image != null) {
                @unlink($quicklearn->feature_image);
            }
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/tips/';
            $image_url = $upload_path . $image_full_name;
            $image->move($upload_path, $image_full_name);
            if ($ext=='jpg' || $ext=='png'|| $ext=='jpeg'|| $ext=='JPG' || $ext=='PNG'|| $ext=='JPEG'){
                $quicklearn->feature_image = $image_url;
            }else{

                Session::flash('warning','File is not valid!!');
                return redirect()->back();
            }
        }
        $quicklearn->title  = $request->title;
        function make_slug($string) {
            $slug = preg_replace('/\s+/u', '-', trim($string));
            $slug = mb_strtolower($slug);
            return $slug;
        }
        $quicklearn->slug   = make_slug($request->title);
        $quicklearn->cat_id  = $request->category;
        $quicklearn->description  = $request->description;
        $quicklearn->url  = $request->url;
        $quicklearn->status  = $request->status;
        $quicklearn->save();
        Session::flash('message', 'New Tips&Ticks update SuccessFully!!');
        return redirect()->back();

    }

    public function delete($id)
    {
        $quicklearn = Quciklearn::find($id);
        if ($quicklearn->feature_image) {
            @unlink($quicklearn->feature_image);
        }
        $quicklearn->delete();
        Session::flash('message', 'New Tips&Ticks  delete Successfully!!');
        return redirect()->back();
    }
}

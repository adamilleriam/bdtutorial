<?php

namespace App\Http\Controllers\Admin;
use DB;
use App\Gallery;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{
    //

    public function index()
    {
        $data['gallerys'] = Gallery::paginate(10);
        $data['title']    = 'Gallery List';
        return view("gallery.index",$data);
    }

    public function create()
    {
        $data['title'] ='Add Gallery';
        $data['division'] = DB::table('divisions')->select('division_id','divisionName')->get();

        return view("gallery.create",$data);
    }

    public function getDistrict(Request $request)
    {
        $districts = DB::table('districts')->select('district_id','districtName')->where('division_id',$request->id)->get();

        return response()->json($districts);
    }

    public function getThana(Request $request)
    {
        $subdistrict = DB::table('subdistricts')->select('subdist_id','subdistrictName')->where('district_id',$request->id)->get();

        return response()->json($subdistrict);
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'division' => 'required|not_in:0',
            'district' => 'required',
            'thana' => 'required',
            'author' => 'required',
            'image' => 'required',
            'description' => 'required',
            'status' => 'required',
        ]);

        $gallery = new Gallery();
        $gallery->division_id = $request->division;
        $gallery->district_id = $request->district;
        $gallery->division_id = $request->division;
        $gallery->subdist_id = $request->thana;
        $gallery->title = $request->title;
        $gallery->author = $request->author;
        $gallery->status = $request->status;
        $gallery->description = $request->description;

        $image = $request->file('image');
        if ($image) {
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/gallery/';
            $image_url = $upload_path . $image_full_name;
            $image->move($upload_path, $image_full_name);
            if ($ext=='jpg' || $ext=='png'|| $ext=='jpeg'|| $ext=='JPG' || $ext=='PNG'|| $ext=='JPEG'){
                $gallery->image = $image_url;
            }else{

                Session::flash('warning','File is not valid!!');
                return redirect()->back();
            }
        }
        $gallery->save();
        Session::flash('message', 'New Gallery added SuccessFully!!');
        return redirect()->back();
    }

    public function edit($id)
    {
        $data['title'] ='Edit Gallery';
        $data['gallerys'] = Gallery::find($id);
        $data['divisions']    = DB::table('divisions')->select('division_id','divisionName')->get();
        $data['districts']    = DB::table('districts')->select('district_id','districtName')->get();
        $data['subdistricts'] = DB::table('subdistricts')->select('subdist_id','subdistrictName')->get();

        return view('gallery.edit',$data);


    }

    public function update(Request $request,$id)
    {

       $this->validate($request, [
            'title' => 'required',
            'division' => 'required|not_in:0',
            'district' => 'required',
            'thana' => 'required',
            'author' => 'required',
            'description' => 'required',
            'status' => 'required',
        ]);

        $gallery = Gallery::find($id);
        $gallery->division_id = $request->division;
        $gallery->district_id = $request->district;
        $gallery->division_id = $request->division;
        $gallery->subdist_id = $request->thana;
        $gallery->title = $request->title;
        $gallery->author = $request->author;
        $gallery->status = $request->status;
        $gallery->description = $request->description;

        $image = $request->file('image');
        if ($image) {
            if ($gallery->image != null) {
                @unlink($gallery->image);
            }
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/gallery/';
            $image_url = $upload_path . $image_full_name;
            $image->move($upload_path, $image_full_name);
            if ($ext=='jpg' || $ext=='png'|| $ext=='jpeg'|| $ext=='JPG' || $ext=='PNG'|| $ext=='JPEG'){
                $gallery->image = $image_url;
            }
            else
            {

                Session::flash('Warning','File is not valid!!');
                return redirect()->back();
            }
        }
        $gallery->save();
        Session::flash('message', 'Gallery update Successfully!!');
        return redirect()->back();
    }

    public function delete($id)
    {
        $gallery = Gallery::find($id);
        if ($gallery->image) {
            @unlink($gallery->image);
        }
        $gallery->delete();
        Session::flash('message', 'Gallery delete Successfully!!');
        return redirect()->back();
    }
}

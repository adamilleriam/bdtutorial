<?php

namespace App\Http\Controllers\Admin;

use App\Chapter;
use App\ChapterContent;
use App\ClassSubject;
use App\SubjectList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
class ClassSubjectController extends Controller
{
    public function index(Request $request,$slug){
        $data['title']='Subject List';
        $subject = New ClassSubject();
        if ($request->search == 'trashed') {
            $subject = $subject->onlyTrashed();

        } elseif ($request->search == 'inactive') {
            $subject = $subject->where('status', 'inactive');
        }elseif ($request->search == 'active') {
            $subject = $subject->where('status', 'active');
        }
        $subject = $subject->where('class_id', $slug);
        $subject = $subject->orderBy('id', 'DESC')->paginate(10);
        if (isset($request->search)) {
            $render['search'] = $request->search;
            $subject = $subject->appends($render);
        }
        $data['subjects'] = $subject;
        $data['slug']=$slug;
        $data['serial'] = managePagination($subject);

        return view('academic.class._class_subject.index',$data);
    }
    public function create($slug){
        $data['title']='New Class Subject Create';
        $data['slug']=$slug;
        $data['subjectList']=SubjectList::where('status','active')->pluck('name','slug');

        return view('academic.class._class_subject.create',$data);
    }
    public function store(Request $request,$class_id){
        $this->validate($request, [
            'subject' => 'required',
            'status' => 'required',
        ]);
        $subjectChek=ClassSubject::where('class_id',$class_id)->where('slug',$request->subject)->count();
     if($subjectChek>0){
         Session::flash('warning', 'Already Add This Subject.');
         return redirect()->back()->withInput();
     }else{
         $subjectList=SubjectList::where('slug',$request->subject)->select('name','slug')->first();
         $subject= New ClassSubject();
         $subject->name=$subjectList->name;
         $subject->slug=$subjectList->slug;
         $subject->url=$request->url;
         $subject->class_id=$class_id;
         $subject->status=$request->status;
         $subject->save();
         Session::flash('message', 'Subject Create Successfully.');
         return redirect()->route('class_subject.index',$class_id);
     }

    }
    public function edit($id){
        $data['title'] = 'Edit Class Subject';

        $data['subject'] = ClassSubject::withTrashed()->where('id', $id)->first();
        return view('academic.class._class_subject.edit',$data);

    }
    public function update(Request $request,$id){

        $subject=ClassSubject::withTrashed()->where('id', $id)->first();
        $subject->url=$request->url;
        $subject->status=$request->status;
        $subject->save();

        Session::flash('message', 'Class Subject Update Successfully.');
        return redirect()->route('class_subject.index',$subject->class_id);
    }
    public function trash($id)
    {
        $subject=ClassSubject::findorfail($id);
        $slugExist=Chapter::where('class_id',$subject->class_id)->where('subject_id',$subject->slug)->count();
        if($slugExist>0){
            Session::flash('warning', 'This Subject Can\'t Trashed.');
            return redirect()->back();
        }else{

            $subject->delete();
            Session::flash('message', 'Successfully Trashed.');
            return redirect()->back();
        }

    }
    public function restore($id)
    {
        ClassSubject::withTrashed()->where('id', $id)->first()->restore();
        Session::flash('message', 'Successfully restored.');
        return redirect()->back();


    }
    public function destroy($id)
    {
        $subject=ClassSubject::withTrashed()->where('id', $id)->first();
        Chapter::where('class_id',$subject->class_id)->where('subject_id',$subject->slug)->forceDelete();
        ChapterContent::where('class_id',$subject->class_id)->where('subject_id',$subject->slug)->forceDelete();

        $subject->forceDelete();

        Session::flash('message', 'Successfully Deleted.');
        return redirect()->back();

    }
}

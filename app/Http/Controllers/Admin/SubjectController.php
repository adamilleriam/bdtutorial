<?php

namespace App\Http\Controllers\Admin;

use App\ClassSubject;
use App\SubjectList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
class SubjectController extends Controller
{
    public function index(Request $request){
        $data['title']='Subject List';
        $subject = New SubjectList();
        if ($request->search == 'trashed') {
            $subject = $subject->onlyTrashed();

        } elseif ($request->search == 'inactive') {
            $subject = $subject->where('status', 'inactive');
        }elseif ($request->search == 'active') {
            $subject = $subject->where('status', 'active');
        }

        $subject = $subject->orderBy('id', 'DESC')->paginate(10);
        if (isset($request->search)) {
            $render['search'] = $request->search;
            $subject = $subject->appends($render);
        }
        $data['subjects'] = $subject;
        $data['serial'] = managePagination($subject);

        return view('academic.subject.index',$data);
    }
    public function create(){
        $data['title']='New Subject Create';
        return view('academic.subject.create',$data);
    }
    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
            'status' => 'required',
        ]);

        $slug=  str_slug($request->slug);
        $subject= New SubjectList();
        $subject->name=$request->name;
        $subject->slug=$slug;
        $subject->status=$request->status;
        $subject->save();

        Session::flash('message', 'Subject Create Successfully.');
        return redirect()->route('subject.index');
    }

    public function edit($id)
    {
        $data['title'] = 'Edit Subject';

        $data['subject'] = SubjectList::withTrashed()->where('id', $id)->first();
        $data['slugExist']=ClassSubject::where('slug',$data['subject']->slug)->count();

        return view('academic.subject.edit', $data);
    }
    public function update(Request $request,$id){
        $this->validate($request, [
            'name' => 'required',
            'status' => 'required',
        ]);
        $subject=SubjectList::withTrashed()->where('id', $id)->first();
        if(isset($request->newslug)&&$request->newslug!=null){
            $slug=  str_slug($request->newslug);
            $subject->slug=$slug;
        }
        $subject->name=$request->name;
        $subject->status=$request->status;
        $subject->save();

        Session::flash('message', 'Subject Update Successfully.');
        return redirect()->route('subject.index');
    }
    public function trash($id)
    {
        $subject=SubjectList::findorfail($id);
        $slugExist=ClassSubject::where('slug',$subject->slug)->count();
        if($slugExist>0){
            Session::flash('warning', 'This Subject Can\'t Trashed.');
            return redirect()->back();
        }else{

            $subject->delete();
            Session::flash('message', 'Successfully Trashed.');
            return redirect()->back();
        }

    }

    public function restore($id)
    {
        SubjectList::withTrashed()->where('id', $id)->first()->restore();
        Session::flash('message', 'Successfully restored.');
        return redirect()->route('subject.index');


    }

    public function destroy($id)
    {
        $subject=SubjectList::withTrashed()->where('id', $id)->first();
        ClassSubject::where('slug',$subject->slug)->forceDelete();

        $subject->forceDelete();

        Session::flash('message', 'Successfully Deleted.');
        return redirect()->route('subject.index');

    }
}

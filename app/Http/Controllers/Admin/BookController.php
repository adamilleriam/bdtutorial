<?php

namespace App\Http\Controllers\Admin;
use App\Ebook;
use DB;
use App\EbookCategory;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookController extends Controller
{
    //
    public function index(Request $request)
    {
        $data['title'] = 'Manage E-Books';
        $ebooks = New Ebook();
        $ebooks = $ebooks
            ->join('ebook_categories', 'ebooks.category_id', '=', 'ebook_categories.id')
            ->select('ebooks.*', 'ebook_categories.category_name');

        if ($request->search == 'trashed') {
            $ebooks = $ebooks->onlyTrashed();
        } elseif ($request->search == 'inactive') {
            $ebooks = $ebooks->where('ebooks.status', 'inactive');
        }elseif ($request->search == 'active') {
            $ebooks = $ebooks->where('ebooks.status', 'active');
        }
        $ebooks= $ebooks->orderBy('id', 'DESC')->paginate(10);
        if (isset($request->search)) {
            $render['search'] = $request->search;
            $ebooks = $ebooks->appends($render);
        }
        $data['ebooks'] = $ebooks;
        $data['serial'] = managePagination($ebooks);
        return view('resource.ebook.index', $data);
    }

    public function create()
    {
        $data['categories'] = EbookCategory::get();
        $data['title'] = 'Add Institution';
        return view('resource.ebook.create',$data);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'book_category' =>'required',
            'book_name' =>'required',
            'overview' =>'required',
            'author_name' =>'required',
            'pdf_book' =>'required',
            'book_cover' =>'required',
        ]);

        $books = new Ebook();
        $books->category_id           = $request->book_category;
        $books->name                  = $request->book_name;
        function make_slug($string) {
            $slug = preg_replace('/\s+/u', '-', trim($string));
            $slug = mb_strtolower($slug);
            return $slug;
        }
        $books->slug          = make_slug($request->book_name);
        $books->author        = $request->author_name;
        $books->book_overview = $request->overview;
        $books->status        = $request->status;
        if ($file = $request->file('pdf_book')) {
            $uniqueFileName = uniqid() . $file->getClientOriginalName();
            $ext = strtolower($file->getClientOriginalExtension());
            $file_full_name = $uniqueFileName;
            $upload_path = 'upload/resource/books/';
            $file_url = $upload_path . $file_full_name;
            $file->move($upload_path,$file_full_name);
            if ($ext == 'pdf'){
                $books->file_path = $file_url;
            }else{
                Session::flash('warning','File is not valid!!');
                return redirect()->back();
            }
        }
        $image = $request->file('book_cover');
        if ($image) {
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/resource/books/';
            $image_url = $upload_path . $image_full_name;
            $image->move($upload_path, $image_full_name);
            if ($ext=='jpg' || $ext=='png'|| $ext=='jpeg'|| $ext=='JPG' || $ext=='PNG'|| $ext=='JPEG'){
                $books->book_cover = $image_url;
            }else{

                Session::flash('warning','File is not valid!!');
                return redirect()->back();
            }
        }

        $books->save();
        Session::flash('message', 'Books added Successfully!!');
        return redirect()->back();

    }

    public function edit($id)
    {
        $data['title'] = 'Edit E-Book';
        $data['categories'] = EbookCategory::get();
        $data['books'] = Ebook::withTrashed()->where('id', $id)->first();
        return view('resource.ebook.edit',$data);

    }

    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'book_category' =>'required',
            'book_name' =>'required',
            'overview' =>'required',
            'author_name' =>'required'
        ]);

        $books = Ebook::withTrashed()->where('id', $id)->first();
        $books->category_id           = $request->book_category;
        $books->name                  = $request->book_name;
        function make_slug($string) {
            $slug = preg_replace('/\s+/u', '-', trim($string));
            $slug = mb_strtolower($slug);
            return $slug;
        }
        $books->slug          = make_slug($request->book_name);
        $books->author        = $request->author_name;
        $books->book_overview = $request->overview;
        $books->status        = $request->status;
        if ($file = $request->file('pdf_book')) {
            if($books->file_path !=null)
            {
                @unlink($books->file_path);
            }
            $uniqueFileName = uniqid() . $file->getClientOriginalName();
            $ext = strtolower($file->getClientOriginalExtension());
            $file_full_name = $uniqueFileName;
            $upload_path = 'upload/resource/books/';
            $file_url = $upload_path . $file_full_name;
            $file->move($upload_path,$file_full_name);
            if ($ext == 'pdf'){
                $books->file_path = $file_url;
            }else{
                Session::flash('warning','File is not valid!!');
                return redirect()->back();
            }
        }
        $image = $request->file('book_cover');
        if ($image) {
            if($books->book_cover !=null)
            {
                @unlink($books->book_cover);
            }
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/resource/books/';
            $image_url = $upload_path . $image_full_name;
            $image->move($upload_path, $image_full_name);
            if ($ext=='jpg' || $ext=='png'|| $ext=='jpeg'|| $ext=='JPG' || $ext=='PNG'|| $ext=='JPEG'){
                $books->book_cover = $image_url;
            }else{

                Session::flash('warning','File is not valid!!');
                return redirect()->back();
            }
        }

        $books->save();
        Session::flash('message', 'Books upadte Successfully!!');
        return redirect()->route('book.list');

    }

    public function trash($id)
    {
        Ebook::findorfail($id)->delete();
        Session::flash('message', 'Book successfully Trashed.');
        return redirect()->back();
    }

    public function restore($id)
    {
        Ebook::withTrashed()->where('id', $id)->first()->restore();
        Session::flash('message', 'Book successfully restored.');
        return redirect()->route('book.list');

    }
    public function delete($id)
    {
        $book = Ebook::withTrashed()->where('id', $id)->first();
        if ($book->book_cover) {
            @unlink($book->book_cover);
        }
        if ($book->file_path) {
            @unlink($book->file_path);
        }
        $book->forceDelete();
        Session::flash('message', 'Book delete Successfully!!');
        return redirect()->route('book.list');
    }
}

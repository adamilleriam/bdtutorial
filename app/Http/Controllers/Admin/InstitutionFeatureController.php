<?php
namespace App\Http\Controllers\Admin;
use App\InstitutionSlider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;

class InstitutionFeatureController extends Controller
{
    //
    public function index(Request $request,$id)
    {
        $slider = New InstitutionSlider();
        $data['title'] = 'Manage Institution Slider';
        if ($request->search == 'trashed') {
            $slider = $slider->onlyTrashed();
        } elseif ($request->search == 'inactive') {
            $slider = $slider->where('status', 'inactive');
        }elseif ($request->search == 'active') {
            $slider = $slider->where('status', 'active');
        }

        $slider = $slider->where('institution_id',$id)->orderBy('id', 'DESC')->paginate(Cache::get('per_page'));
        if (isset($request->search)) {
            $render['search'] = $request->search;
            $slider = $slider->appends($render);
        }
        $data['sliders'] = $slider;
        $data['id'] = $id;
        $data['serial'] = managePagination($slider);
        return view('resource.institution.slider.slider_index', $data);
    }

    public function create($id)
    {
        $data['title'] = 'Add Institution Slider';
        $data['title']="Slider add ";
        $data['id']= $id;
        return view('resource.institution.slider.create_slider',$data);
    }

    public function store(Request $request,$id)
    {
        $this->validate($request,[
            'image' =>'required',
        ]);

        $institution_slider = new InstitutionSlider();
        $institution_slider->status         = $request->status;
        $institution_slider->institution_id = $request->id;
        $image = $request->file('image');
        if ($image) {
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/resource/institution/slider/';
            $image_url = $upload_path . $image_full_name;
            $image->move($upload_path, $image_full_name);
            if ($ext=='jpg' || $ext=='png'|| $ext=='jpeg'|| $ext=='JPG' || $ext=='PNG'|| $ext=='JPEG'){
                $institution_slider->image = $image_url;
            }else{

                Session::flash('warning','File is not valid!!');
                return redirect()->back();
            }
        }

        $institution_slider->save();
        Session::flash('message', 'Institution Slider added Successfully!!');
        return redirect()->back();

    }

    public function edit($id)
    {
        $data['title'] = 'Edit Institution Slider';
        $data['slider'] = InstitutionSlider::withTrashed()->where('id', $id)->first();
        return view('resource.institution.slider.edit_slider',$data);

    }

    public function update(Request $request,$id)
    {
        $institution_slider = InstitutionSlider::withTrashed()->where('id', $id)->first();
        $institution_slider->status         = $request->status;
        $image = $request->file('image');
        if ($image) {
            if($institution_slider->image !=null)
            {
                @unlink($institution_slider->image);
            }
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/resource/institution/slider/';
            $image_url = $upload_path . $image_full_name;
            $image->move($upload_path, $image_full_name);
            if ($ext=='jpg' || $ext=='png'|| $ext=='jpeg'|| $ext=='JPG' || $ext=='PNG'|| $ext=='JPEG'){
                $institution_slider->image = $image_url;
            }else{

                Session::flash('warning','File is not valid!!');
                return redirect()->back();
            }
        }

        $institution_slider->save();
        Session::flash('message', 'Institution Slider update Successfully!!');
        return redirect()->route('institution.slider.list',$institution_slider->institution_id);


    }

    public function trash($id)
    {
        InstitutionSlider::findorfail($id)->delete();
        Session::flash('message', 'Institution Slider successfully Trashed.');
        return redirect()->back();
    }

    public function restore($id)
    {
        $institute=InstitutionSlider::withTrashed()->where('id', $id)->first();
        $institute->restore();
        Session::flash('message', 'Institution Slider successfully restored.');
        return redirect()->route('institution.slider.list',$institute->institution_id);

    }
    public function delete($id)
    {
        $slider = InstitutionSlider::withTrashed()->where('id', $id)->first();
        if ($slider->image) {
            @unlink($slider->image);
        }
        $slider->forceDelete();
        Session::flash('message', 'Institution Slider delete Successfully!!');
        return redirect()->route('institution.slider.list',$slider->institution_id);
    }


}

<?php

namespace App\Http\Controllers\Admin;

use App\CourseModules;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
class CourseModulesController extends Controller
{
    public function index(Request $request,$course_id){
        $data['title']='Course Module List';
        $module = New CourseModules();
        if ($request->search == 'trashed') {
            $module = $module->onlyTrashed();

        } elseif ($request->search == 'inactive') {
            $module = $module->where('status', 'inactive');
        }elseif ($request->search == 'active') {
            $module = $module->where('status', 'active');
        }
        $module = $module->where('course_id',$course_id);
        $module = $module->orderBy('id', 'DESC')->paginate(10);
        if (isset($request->search)) {
            $render['search'] = $request->search;
            $module = $module->appends($render);
        }
        $data['modules'] = $module;
        $data['course_id'] = $course_id;
        $data['serial'] = managePagination($module);

        return view('professional_training.module.index',$data);
    }
    public function create($course_id){
        $data['title']='New Module Create';
        $data['course_id'] = $course_id;
        return view('professional_training.module.create',$data);
    }
    public function store(Request $request,$course_id){
        $this->validate($request, [
            'name' => 'required',
            'overview' => 'required',
            'url' => 'required',
            'status' => 'required',
        ]);

        $module= New CourseModules();
        $module->name=$request->name;
        $module->slug=$this->_slug($request->name);
        $module->course_id=$course_id;
        $module->overview=$request->overview;
        $module->url=$request->url;
        $module->sequence=$request->sequence;

        if ($request->hasFile('files')) {
            $image = $request->file('files');
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/course_module/';
            $image_url = $upload_path . $image_full_name;
            if ($ext=='pdf' || $ext=='doc'|| $ext=='zip'|| $ext=='rar'){
                $image->move($upload_path, $image_full_name);
                $module->file = $image_url;
            }else{

                Session::flash('warning','File is not valid!!');
                return redirect()->back()->withInput();
            }
        }


        $module->status=$request->status;
        $module->save();
        Session::flash('message', 'Course Module Create Successfully.');
        return redirect()->route('module.index',$course_id);


    }
    public function edit($id){
        $data['title'] = 'Edit Class Subject';
        $data['module']=CourseModules::withTrashed()->where('id',$id)->first();
        return view('professional_training.module.edit',$data);

    }
    public function update(Request $request,$id){

        $module=CourseModules::withTrashed()->where('id', $id)->first();
        $module->name=$request->name;
        $module->url=$request->url;
        $module->sequence=$request->sequence;
        $module->status=$request->status;
        if ($request->hasFile('files')) {
            $image = $request->file('files');
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/course_module/';
            $image_url = $upload_path . $image_full_name;
            if ($ext=='pdf' || $ext=='doc'|| $ext=='zip'|| $ext=='rar'){
                if($module->file!=null){
                    @unlink($module->file);
                }

                $image->move($upload_path, $image_full_name);
                $module->file = $image_url;
            }else{
                Session::flash('warning','File is not valid!!');
                return redirect()->back()->withInput();
            }
        }

        $module->save();

        Session::flash('message', 'Course Module Update Successfully.');
        return redirect()->route('module.index',$module->course_id);
    }
    public function trash($id)
    {
        CourseModules::findorfail($id)->delete();
        Session::flash('message', 'Successfully Trashed.');
        return redirect()->back();
    }
    public function restore($id)
    {
        CourseModules::withTrashed()->where('id', $id)->first()->restore();
        Session::flash('message', 'Successfully restored.');
        return redirect()->back();


    }
    public function destroy($id)
    {
        $module=CourseModules::withTrashed()->where('id', $id)->first();
        if($module->file!=null){
            @unlink($module->file);
        }
        $courseID=$module->course_id;
        $module->forceDelete();

        Session::flash('message', 'Successfully Deleted.');
        return redirect()->route('module.index',$courseID);

    }
    public function _slug($value)
    {
        // make lower case
        $value = strtolower($value);
        // replace space with -
        $slug = str_replace(' ', '-', $value);
//        $slug = str_slug($value);
        if (CourseModules::withTrashed()->where('slug', $slug)->exists()) {
            $slug = $slug . mt_rand(10, 99);
            $this->_slug($slug);
        }
        return $slug;
    }
}

<?php

namespace App\Http\Controllers\Admin;
use App\Teacher;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeachersController extends Controller
{
    //
    public function index()
    {
        $data['title'] = 'Tutor List';
        $data['teachers'] = Teacher::get();
        return view('tutors.index',$data);
    }

    public function create()
    {
        $data['title'] = 'Add Tutor';
        return view('tutors.addtutors',$data);
    }

    public function store(Request $request)
    {

       $this->validate($request,[
           'name' => 'required|max:255',
           'title' => 'required|max:255',
           'email' => 'required',
           'mobile' => 'required|numeric',
           'biography' => 'required',
           'status' => 'required',
           'image' => 'required|mimes:jpeg,jpg,png'
       ]);

       $teacher = new Teacher();
       $teacher->name = $request->name;
       $teacher->slug = str_slug($request->name);
       $teacher->title = $request->title;
       $teacher->email = $request->email;
       $teacher->mobile = $request->mobile;
       $teacher->url = $request->url;
       $teacher->biography = $request->biography;
       $teacher->facebook_profile = $request->facebook_profile;
       $teacher->twitter_profile = $request->twitter_profile;
       $teacher->linkedin_profile = $request->linkedin_profile;
       $teacher->skype_profile = $request->skype_profile;
       $teacher->status = $request->status;

        $image = $request->file('image');
        if ($image) {
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/tutors/';
            $image_url = $upload_path . $image_full_name;
            $image->move($upload_path, $image_full_name);
            if ($ext=='jpg' || $ext=='png'|| $ext=='jpeg'|| $ext=='JPG' || $ext=='PNG'|| $ext=='JPEG'){
                $teacher->image = $image_url;
            }else{

                Session::flash('warning','File is not valid!!');
                return redirect()->back();
            }
        }

       $teacher->save();
        Session::flash('message', 'New Tutors added SuccessFully!!');
        return redirect()->back();

    }

    public function edit($id)
    {
         $data['title'] = 'Edit Tutor';
         $data['teachers'] = Teacher::find($id);
        return view('tutors.edittutors',$data);

    }

    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'name' => 'required|max:255',
            'title' => 'required|max:255',
            'email' => 'required',
            'mobile' => 'required|numeric',
            'biography' => 'required',
            'status' => 'required',
        ]);

        $teacher = Teacher::find($id);
        $teacher->name = $request->name;
        $teacher->slug = str_slug($request->name);
        $teacher->title = $request->title;
        $teacher->email = $request->email;
        $teacher->mobile = $request->mobile;
        $teacher->url = $request->url;
        $teacher->biography = $request->biography;
        $teacher->facebook_profile = $request->facebook_profile;
        $teacher->twitter_profile = $request->twitter_profile;
        $teacher->linkedin_profile = $request->linkedin_profile;
        $teacher->skype_profile = $request->skype_profile;
        $teacher->status = $request->status;

        $image = $request->file('image');
        if ($image) {
            if ($teacher->image != null) {
                @unlink($teacher->image);
            }
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/tutors/';
            $image_url = $upload_path . $image_full_name;
            $image->move($upload_path, $image_full_name);
            if ($ext=='jpg' || $ext=='png'|| $ext=='jpeg'|| $ext=='JPG' || $ext=='PNG'|| $ext=='JPEG'){
                $teacher->image = $image_url;
            }
            else
            {
                Session::flash('Warning','File is not valid!!');
                return redirect()->back();
            }
        }

        $teacher->save();
        Session::flash('message', 'Tutors updated Successfully!!');
        return redirect()->back();

    }

    public function delete($id)
    {
        $teachers = Teacher::find($id);
        if ($teachers->image) {
            @unlink($teachers->image);
        }
        $teachers->delete();
        Session::flash('message', 'Tutors delete Successfully!!');
        return redirect()->back();
    }
}

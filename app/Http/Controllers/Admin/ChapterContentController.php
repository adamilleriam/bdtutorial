<?php

namespace App\Http\Controllers\Admin;

use App\ChapterContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
class ChapterContentController extends Controller
{
    public function index(Request $request,$class,$subject,$chapter){
        $data['title']='Content List';
        $chapterContent = New ChapterContent();
        if ($request->search == 'trashed') {
            $chapterContent = $chapterContent->onlyTrashed();

        } elseif ($request->search == 'inactive') {
            $chapterContent = $chapterContent->where('status', 'inactive');
        }elseif ($request->search == 'active') {
            $chapterContent = $chapterContent->where('status', 'active');
        }
        $chapterContent = $chapterContent->where('class_id', $class);
        $chapterContent = $chapterContent->where('subject_id', $subject);
        $chapterContent = $chapterContent->where('chapter_id', $chapter);
        $chapterContent = $chapterContent->orderBy('id', 'DESC')->paginate(10);
        if (isset($request->search)) {
            $render['search'] = $request->search;
            $chapterContent = $chapterContent->appends($render);
        }
        $data['chapterContent'] = $chapterContent;
        $data['class']=$class;
        $data['subject']=$subject;
        $data['chapter']=$chapter;
        $data['serial'] = managePagination($chapterContent);

        return view('academic.class._chapter_content.index',$data);
    }
    public function create($class,$subject,$chapter){
        $data['title']='New Chapter Content Create';
        $data['class']=$class;
        $data['subject']=$subject;
        $data['chapter']=$chapter;
        return view('academic.class._chapter_content.create',$data);
    }
    public function store(Request $request,$class,$subject,$chapter){
        $this->validate($request, [
            'name' => 'required',
            'url' => 'required',
            'status' => 'required',
        ]);
            $content= New ChapterContent();
            $content->name=$request->name;
            $content->class_id=$class;
            $content->subject_id=$subject;
            $content->chapter_id=$chapter;
            $content->url=$request->url;
            $content->sequence=$request->sequence;
            $content->status=$request->status;
            $content->save();
            Session::flash('message', 'Content Create Successfully.');
            return redirect()->route('chapter_content.index',[$class,$subject,$chapter]);

    }
    public function edit($id)
    {
        $data['title'] = 'Edit Chapter Content';

        $data['content'] = ChapterContent::withTrashed()->where('id', $id)->first();
       return view('academic.class._chapter_content.edit', $data);
    }
    public function update(Request $request,$id){
        $this->validate($request, [
            'name' => 'required',
            'url' => 'required',
            'status' => 'required',
        ]);
        $content=ChapterContent::withTrashed()->where('id', $id)->first();
        $content->name=$request->name;
        $content->url=$request->url;
        $content->sequence=$request->sequence;
        $content->status=$request->status;
        $content->save();

        Session::flash('message', 'Chapter Content Update Successfully.');
        return redirect()->route('chapter_content.index',[$content->class_id,$content->subject_id,$content->chapter_id]);
    }
    public function trash($id)
    {
        ChapterContent::findorfail($id)->delete();
        Session::flash('message', 'Successfully Trashed.');
        return redirect()->back();
    }
    public function restore($id)
    {
        ChapterContent::withTrashed()->where('id', $id)->first()->restore();
        Session::flash('message', 'Successfully Restored.');
        return redirect()->back();
    }
    public function destroy($id)
    {
        ChapterContent::withTrashed()->where('id', $id)->first()->forceDelete();
        Session::flash('message', 'Successfully Deleted.');
        return redirect()->back();

    }
}

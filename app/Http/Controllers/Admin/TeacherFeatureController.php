<?php

namespace App\Http\Controllers\Admin;
use App\TeacherSkill;
use App\TeacherEducation;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeacherFeatureController extends Controller
{
    public function index($id)
    {
        $data['teacherskills'] = TeacherSkill::where('teacher_id',$id)->get();
        $data['id']= $id;
       return view('tutors.tutorskills',$data);

    }

    public function create($id)
    {
       $data['title']="Skill add ";
       $data['id']= $id;

        return view('tutors.addtutorskill',$data);
    }

    public function store(Request $request,$id)
    {
        $this->validate($request,[
            'title' => 'required',
            'skilllimit'=>'required|numeric'
        ]);

        $skill = new TeacherSkill();
        $skill->title  = $request->title;
        $skill->skilllimit  = $request->skilllimit;
        $skill->teacher_id  = $request->id;
        $skill->save();
        Session::flash('message', 'New Skill added Successfully!!');
        return redirect()->back();
    }

    public function edit($id)
    {
        $date['title'] = 'Skill Edit';
        $date['id']    = $id;
        $date['teacherskills'] = TeacherSkill::find($id);
        return view('tutors.edittutorskill',$date);
    }

    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'title' => 'required',
            'skilllimit'=>'required|numeric'
        ]);
        $updateskill = TeacherSkill::find($id);
        $updateskill->title  = $request->title;
        $updateskill->skilllimit  = $request->skilllimit;
        $updateskill->save();
        Session::flash('message', 'Skill update Successfully!!');
        return redirect()->back();
    }

    public function delete($id)
    {
        $deleteskill = TeacherSkill::find($id);
        $deleteskill->delete();
        Session::flash('message', 'Skill delete Successfully!!');
        return redirect()->back();
    }

    /* Teacher Education section controller  */

    public function  TeacherEducation($id){
        $data['educations'] = TeacherEducation::where('teacher_id',$id)->get();
        $data['id']= $id;
        return view('tutors.tutoreducation', $data);
    }

    public function  addTeacherEducation($id){
        $data['title']="Education add ";
        $data['id']= $id;
        return view('tutors.addtutorEducation',$data);
    }

    public function storeEducatoin(Request $request,$id)
    {
        $this->validate($request,[
            'title' => 'required',
            'description'=>'required'
        ]);

        $education = new TeacherEducation();
        $education->title  = $request->title;
        $education->description  = $request->description;
        $education->teacher_id  = $request->id;
        $education->save();
        Session::flash('message', 'Education added Successfully!!');
        return redirect()->back();

    }


    public function editEducation($id)
    {
        $date['title'] = 'Education Edit';
        $date['id']    = $id;
        $date['educations'] = TeacherEducation::find($id);
        return view('tutors.edittutorEducation',$date);
    }

    public function updateEducation(Request $request,$id)
    {
        $this->validate($request,[
            'title' => 'required',
            'description'=>'required'
        ]);

        $education =TeacherEducation::find($id);
        $education->title        = $request->title;
        $education->description  = $request->description;
        $education->save();
        Session::flash('message', 'Education update Successfully!!');
        return redirect()->back();
    }

    public function deleteEducation($id)
    {
        $deleteeducation = TeacherEducation::find($id);
        $deleteeducation->delete();
        Session::flash('message', 'Education delete Successfully!!');
        return redirect()->back();
    }
}

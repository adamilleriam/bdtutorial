<?php

namespace App\Http\Controllers\Admin;
use App\Historicalplace;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;

class HistoricalPlaceController extends Controller
{
    //
    public function index(Request $request)
    {
        $historicalplace = New Historicalplace();
        $data['title'] = 'Manage Historicalplace';
        if ($request->search == 'trashed') {
            $historicalplace = $historicalplace->onlyTrashed();
        } elseif ($request->search == 'inactive') {
            $historicalplace = $historicalplace->where('status', 'inactive');
        }elseif ($request->search == 'active') {
            $historicalplace = $historicalplace->where('status', 'active');
        }

        $historicalplace = $historicalplace->orderBy('id', 'DESC')->paginate(Cache::get('per_page'));
        if (isset($request->search)) {
            $render['search'] = $request->search;
            $historicalplace = $historicalplace->appends($render);
        }
        $data['historicalplaces'] = $historicalplace;
        $data['serial'] = managePagination($historicalplace);
        return view('resource.historicalplace.index', $data);
    }

    public function create()
    {
        $data['title'] = 'Add Historical Place';
        return view('resource.historicalplace.create',$data);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' =>'required',
            'history' =>'required',
            'location' =>'required',
            'map_url' =>'required',
            'status' =>'required',
            'image' =>'required',
        ]);

        $historicalplace = new Historicalplace();
        $historicalplace->name                  = $request->name;
        function make_slug($string) {
            $slug = preg_replace('/\s+/u', '-', trim($string));
            $slug = mb_strtolower($slug);
            return $slug;
        }
        $historicalplace->slug                  = make_slug($request->name);
        $historicalplace->type                  = $request->type;
        $historicalplace->phone                 = $request->phone;
        $historicalplace->history               = $request->history;
        $historicalplace->architectural_style   = $request->architectural_style;
        $historicalplace->build_time            = $request->build_time;
        $historicalplace->opening_hour          = $request->open_hour;
        $historicalplace->location              = $request->location;
        $historicalplace->map_url               = $request->map_url;
        $historicalplace->status                = $request->status;

        $image = $request->file('image');
        if ($image) {
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/resource/';
            $image_url = $upload_path . $image_full_name;
            $image->move($upload_path, $image_full_name);
            if ($ext=='jpg' || $ext=='png'|| $ext=='jpeg'|| $ext=='JPG' || $ext=='PNG'|| $ext=='JPEG'){
                $historicalplace->image = $image_url;
            }else{

                Session::flash('warning','File is not valid!!');
                return redirect()->back();
            }
        }

        $historicalplace->save();
        Session::flash('message', 'Historical place added Successfully!!');
        return redirect()->back();

    }

    public function edit($id)
    {
        $data['title'] = 'Edit Historical Place';
        $data['historicalplace'] = Historicalplace::withTrashed()->where('id', $id)->first();
        return view('resource.historicalplace.edit',$data);

    }

    public function update(Request $request,$id)
    {

        $this->validate($request,[
            'name' =>'required',
            'history' =>'required',
            'location' =>'required',
            'map_url' =>'required',
            'status' =>'required',
        ]);

        $historicalplace = Historicalplace::withTrashed()->where('id', $id)->first();
        $historicalplace->name                  = $request->name;
        function make_slug($string) {
            $slug = preg_replace('/\s+/u', '-', trim($string));
            $slug = mb_strtolower($slug);
            return $slug;
        }
        $historicalplace->slug                  = make_slug($request->name);
        $historicalplace->type                  = $request->type;
        $historicalplace->phone                 = $request->phone;
        $historicalplace->history               = $request->history;
        $historicalplace->architectural_style   = $request->architectural_style;
        $historicalplace->build_time            = $request->build_time;
        $historicalplace->opening_hour          = $request->open_hour;
        $historicalplace->location              = $request->location;
        $historicalplace->map_url               = $request->map_url;
        $historicalplace->status                = $request->status;

        $image = $request->file('image');
        if ($image) {
            if($historicalplace->image !=null)
            {
                @unlink($historicalplace->image);
            }
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/resource/';
            $image_url = $upload_path . $image_full_name;
            $image->move($upload_path, $image_full_name);
            if ($ext=='jpg' || $ext=='png'|| $ext=='jpeg'|| $ext=='JPG' || $ext=='PNG'|| $ext=='JPEG'){
                $historicalplace->image = $image_url;
            }else{

                Session::flash('warning','File is not valid!!');
                return redirect()->back();
            }
        }

        $historicalplace->save();
        Session::flash('message', 'Historical place Update Successfully!!');
        return redirect()->back();

    }

    public function trash($id)
    {
        Historicalplace::findorfail($id)->delete();
        Session::flash('message', 'Historical place successfully Trashed.');
        return redirect()->back();
    }

    public function restore($id)
    {
        Historicalplace::withTrashed()->where('id', $id)->first()->restore();
        Session::flash('message', 'Historical place successfully restored.');
        return redirect()->route('historical.list');

    }
    public function delete($id)
    {
        $historicalplace = Historicalplace::withTrashed()->where('id', $id)->first();
        if ($historicalplace->image) {
            @unlink($historicalplace->image);
        }
        $historicalplace->forceDelete();
        Session::flash('message', 'Historical place delete Successfully!!');
        return redirect()->route('historical.list');
    }
}

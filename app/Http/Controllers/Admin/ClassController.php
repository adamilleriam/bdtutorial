<?php

namespace App\Http\Controllers\Admin;

use App\AcademicClass;
use App\Chapter;
use App\ChapterContent;
use App\ClassSubject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
class ClassController extends Controller
{
    public function index(Request $request){
        $data['title']='Class List';
        $Class = New AcademicClass();
        if ($request->search == 'trashed') {
            $Class = $Class->onlyTrashed();

        } elseif ($request->search == 'inactive') {
            $Class = $Class->where('status', 'inactive');
        }elseif ($request->search == 'active') {
            $Class = $Class->where('status', 'active');
        }elseif ($request->search == 'primary') {
            $Class = $Class->where('type', 'primary');
        }elseif ($request->search == 'high') {
            $Class = $Class->where('type', 'high');
        }elseif ($request->search == 'collage') {
            $Class = $Class->where('type', 'collage');
        }elseif ($request->search == 'university') {
            $Class = $Class->where('type', 'university');
        }

        $Class = $Class->orderBy('id', 'DESC')->paginate(10);
        if (isset($request->search)) {
            $render['search'] = $request->search;
            $Class = $Class->appends($render);
        }
        $data['classes'] = $Class;
        $data['serial'] = managePagination($Class);

        return view('academic.class.index',$data);
    }
    public function create(){
        $data['title']='New Class Create';
        return view('academic.class.create',$data);
    }
    public function store(Request $request){
        $this->validate($request, [
            'type' => 'required',
            'name' => 'required',
            'slug' => 'required',
            'status' => 'required',
        ]);

        $slug=  str_slug($request->slug);
        $class= New AcademicClass();
        $class->name=$request->name;
        $class->slug=$slug;
        $class->type=$request->type;
        $class->sequence=$request->sequence;
        $class->status=$request->status;
        $class->save();

        Session::flash('message', 'Class Create Successfully.');
        return redirect()->route('class.index');
    }
    public function edit($id)
    {
        $data['title'] = 'Edit Class';

        $data['class'] = AcademicClass::withTrashed()->where('id', $id)->first();
        $data['slugExist']=ClassSubject::where('class_id',$data['class']->slug)->count();

        return view('academic.class.edit', $data);
    }
    public function update(Request $request,$id){
        $this->validate($request, [
            'type' => 'required',
            'name' => 'required',
            'status' => 'required',
        ]);
        $class=AcademicClass::withTrashed()->where('id', $id)->first();
        if(isset($request->newslug)&&$request->newslug!=null){
        $slug=  str_slug($request->newslug);
        $class->slug=$slug;
        }
        $class->name=$request->name;
        $class->type=$request->type;
        $class->sequence=$request->sequence;
        $class->status=$request->status;
        $class->save();

        Session::flash('message', 'Class Update Successfully.');
        return redirect()->route('class.index');
    }
    public function trash($id)
    {
     $class=AcademicClass::findorfail($id);
     $slugExist=ClassSubject::where('class_id',$class->slug)->count();
     if($slugExist>0){
         Session::flash('warning', 'This Class Can\'t Trashed.');
         return redirect()->back();
     }else{

     $class->delete();
         Session::flash('message', 'Successfully Trashed.');
         return redirect()->back();
     }

    }
    public function restore($id)
    {
        AcademicClass::withTrashed()->where('id', $id)->first()->restore();
        Session::flash('message', 'Successfully restored.');
        return redirect()->route('class.index');


    }
    public function destroy($id)
    {
        $class=AcademicClass::withTrashed()->where('id', $id)->first();
        ClassSubject::where('class_id',$class->slug)->forceDelete();
        Chapter::where('class_id',$class->slug)->forceDelete();
        ChapterContent::where('class_id',$class->slug)->forceDelete();
        $class->forceDelete();

        Session::flash('message', 'Successfully Deleted.');
        return redirect()->route('class.index');

    }
}

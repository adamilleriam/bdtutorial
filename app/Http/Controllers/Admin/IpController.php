<?php

namespace App\Http\Controllers\Admin;

use App\IpList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class IpController extends Controller
{
    public function index(Request $request)
    {
//        dd($request->segment());
        $data['title'] = 'Blocked IP';
        $ip = new IpList();
//        dd($ip);
        if (isset($request->key)) {
            $ip = $ip->where('ip',$request->key);
        }
        $ip = $ip->where('status','Blocked')->paginate(5);

        $data['ip_lists'] = $ip;
        $data['serial'] = managePagination($ip);
        return view('ip.index', $data);
        
    }
    public function destroy($id)
    {
        IpList::where('id', $id)->first()->delete();
        Session::flash('message', 'IP successfully deleted.');
        return redirect()->back();
    }
}

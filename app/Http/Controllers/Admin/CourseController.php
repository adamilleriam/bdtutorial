<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use App\CourseModules;
use App\ProfessionalTraining;
use App\Teacher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
class CourseController extends Controller
{
    public function index(Request $request){
        $data['title']='Course List';
        $course = New Course();
        if ($request->search == 'trashed') {
            $course = $course->onlyTrashed();

        } elseif ($request->search == 'inactive') {
            $course = $course->where('status', 'inactive');
        }elseif ($request->search == 'active') {
            $course = $course->where('status', 'active');
        }
        $course = $course->orderBy('id', 'DESC')->paginate(10);
        if (isset($request->search)) {
            $render['search'] = $request->search;
            $course = $course->appends($render);
        }
        $data['courses'] = $course;
        $data['serial'] = managePagination($course);

        return view('professional_training.courses.index',$data);
    }
    public function create(){
        $data['title']='New Course Create';
        $data['category']=ProfessionalTraining::where('status','active')->pluck('name','slug');
        $data['teacher']=Teacher::where('status','active')->pluck('name','id');

        return view('professional_training.courses.create',$data);
    }
    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'overview' => 'required',
            'what_will_i_learn' => 'required',
            'requirements' => 'required',
            'topic_slug' => 'required',
            'trainer_id' => 'required',
            'url' => 'required',
            'featured' => 'required',
            'publish' => 'required',
            'files' => 'required',
            'status' => 'required',
        ]);

            $course= New Course();
            $course->name=$request->name;
            $course->slug=$this->_slug($request->name);
            $course->overview=$request->overview;
            $course->what_will_i_learn=$request->what_will_i_learn;
            $course->requirements=$request->requirements;
            $course->topic_slug=$request->topic_slug;
            $course->trainer_id=$request->trainer_id;
            $course->url=$request->url;
            $course->featured=$request->featured;
            $course->publish=$request->publish;
            $course->sequence=$request->sequence;

             if ($request->hasFile('files')) {
                $image = $request->file('files');
                $image_name = str_random(20);
                $ext = strtolower($image->getClientOriginalExtension());
                $image_full_name = $image_name . '.' . $ext;
                $upload_path = 'upload/course/';
                $image_url = $upload_path . $image_full_name;
                if ($ext=='jpg' || $ext=='png'|| $ext=='jpeg'){
                    $image->move($upload_path, $image_full_name);
                    $course->file = $image_url;
                }else{

                    Session::flash('warning','File is not valid!!');
                    return redirect()->back()->withInput();
                }
            }


        $course->status=$request->status;
            $course->save();
            Session::flash('message', 'Course Create Successfully.');
            return redirect()->route('course.index');


    }
    public function edit($id){
        $data['title'] = 'Edit Class Subject';
        $data['category']=ProfessionalTraining::where('status','active')->pluck('name','slug');
        $data['teacher']=Teacher::where('status','active')->pluck('name','id');
        $data['course'] = Course::withTrashed()->where('id', $id)->first();
        return view('professional_training.courses.edit',$data);

    }
    public function update(Request $request,$id){

        $course=Course::withTrashed()->where('id', $id)->first();
        $course->name=$request->name;
        $course->overview=$request->overview;
        $course->what_will_i_learn=$request->what_will_i_learn;
        $course->requirements=$request->requirements;
        $course->topic_slug=$request->topic_slug;
        $course->trainer_id=$request->trainer_id;
        $course->url=$request->url;
        $course->featured=$request->featured;
        $course->publish=$request->publish;
        $course->sequence=$request->sequence;
        $course->status=$request->status;
        if ($request->hasFile('files')) {
            $image = $request->file('files');
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/course/';
            $image_url = $upload_path . $image_full_name;
            if ($ext=='jpg' || $ext=='png'|| $ext=='jpeg'){
                if($course->file!=null){
                    @unlink($course->file);
                }

                $image->move($upload_path, $image_full_name);
                $course->file = $image_url;
            }else{
                Session::flash('warning','File is not valid!!');
                return redirect()->back()->withInput();
            }
        }

        $course->save();

        Session::flash('message', 'Course Update Successfully.');
        return redirect()->route('course.index');
    }
    public function trash($id)
    {
            Course::findorfail($id)->delete();
            Session::flash('message', 'Successfully Trashed.');
            return redirect()->back();
    }
    public function restore($id)
    {
        Course::withTrashed()->where('id', $id)->first()->restore();
        Session::flash('message', 'Successfully restored.');
        return redirect()->route('course.index');


    }
    public function destroy($id)
    {
        $course=Course::withTrashed()->where('id', $id)->first();
        if($course->file!=null){
            @unlink($course->file);
        }

        CourseModules::withTrashed()->where('course_id',$course->id)->forceDelete();

        $course->forceDelete();

        Session::flash('message', 'Successfully Deleted.');
        return redirect()->route('course.index');
    }
    public function _slug($value)
    {
        // make lower case
        $value = strtolower($value);
        // replace space with -
        $slug = str_replace(' ', '-', $value);
//        $slug = str_slug($value);
        if (Course::withTrashed()->where('slug', $slug)->exists()) {
            $slug = $slug . mt_rand(10, 99);
            $this->_slug($slug);
        }
        return $slug;
    }
}

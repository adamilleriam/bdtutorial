<?php

namespace App\Http\Controllers\Admin;
use App\EbookCategory;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EbookCategoryController extends Controller
{
    //
    public function index(Request $request)
    {
        $categories = New EbookCategory();
        $data['title'] = 'Manage E-books';
        if ($request->search == 'trashed') {
            $categories = $categories->onlyTrashed();
        } elseif ($request->search == 'inactive') {
            $categories = $categories->where('status', 'inactive');
        }elseif ($request->search == 'active') {
            $categories = $categories->where('status', 'active');
        }

        $categories = $categories->orderBy('id', 'DESC')->paginate(10);
        if (isset($request->search)) {
            $render['search'] = $request->search;
            $categories = $categories->appends($render);
        }
        $data['categories'] = $categories;
        $data['serial'] = managePagination($categories);
        return view('resource.ebook.ebookcategory.index', $data);
    }

    public function create()
    {
        $data['title'] = 'Add E-book Category';
        return view('resource.ebook.ebookcategory.create',$data);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' =>'required',
        ]);

        $categories = new EbookCategory();
        $categories->category_name                  = $request->name;
        function make_slug($string) {
            $slug = preg_replace('/\s+/u', '-', trim($string));
            $slug = mb_strtolower($slug);
            return $slug;
        }
        $categories->slug                  = make_slug($request->name);
        $categories->status                = $request->status;
        $categories->save();
        Session::flash('message', 'Ebook Category added Successfully!!');
        return redirect()->back();

    }

    public function edit($id)
    {
        $data['title'] = 'Edit Ebook Category';
        $data['categories'] = EbookCategory::withTrashed()->where('id', $id)->first();
        return view('resource.ebook.ebookcategory.edit',$data);

    }

    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'name' =>'required',
        ]);
        $categories = EbookCategory::withTrashed()->where('id', $id)->first();
        $categories->category_name                  = $request->name;
        function make_slug($string) {
            $slug = preg_replace('/\s+/u', '-', trim($string));
            $slug = mb_strtolower($slug);
            return $slug;
        }
        $categories->slug                  = make_slug($request->name);
        $categories->status                = $request->status;
        $categories->save();
        Session::flash('message', 'Ebook Category update Successfully!!');
        return redirect()->back();

    }

    public function trash($id)
    {
        EbookCategory::findorfail($id)->delete();
        Session::flash('message', 'Ebook Category successfully Trashed.');
        return redirect()->back();
    }

    public function restore($id)
    {
        EbookCategory::withTrashed()->where('id', $id)->first()->restore();
        Session::flash('message', 'Ebook Category successfully restored.');
        return redirect()->route('book.category.list');
    }
    public function delete($id)
    {
        $categories = EbookCategory::withTrashed()->where('id', $id)->first();
        $categories->forceDelete();
        Session::flash('message', 'Ebook Category delete Successfully!!');
        return redirect()->route('book.category.list');
    }
}
